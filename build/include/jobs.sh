# shellcheck shell=bash

source "${srcIncludeHelpers}"
source "${srcEnv}"

function job_build_nix_caches {
  local provisioners
  local dockerfile
  local context='.'
  local dockerfile_generic='build/Dockerfile.generic'
  local dockerfile_solutions='build/Dockerfile.solutions'

      helper_use_pristine_workdir \
  &&  provisioners=(./build/provisioners/*) \
  &&  helper_build_nix_caches_parallel \
  &&  for (( i="${lower_limit}";i<="${upper_limit}";i++ ))
      do
            provisioner=$(basename "${provisioners[${i}]}") \
        &&  provisioner="${provisioner%.*}" \
        &&  if echo "${provisioner}" | grep -q 'build_solutions_'
            then
              dockerfile="${dockerfile_solutions}"
            else
              dockerfile="${dockerfile_generic}"
            fi \
        &&  helper_docker_build_and_push \
              "${CI_REGISTRY_IMAGE}/nix:${provisioner}" \
              "${context}" \
              "${dockerfile}" \
              'PROVISIONER' "${provisioner}" \
        ||  return 1
      done
}

function job_build_solutions_gherkin {
  local builder='build/builders/solutions/gherkin'
  local extensions='*.feature'
  local folders=(
    'code'
    'hack'
    'vbd'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_java {
  local builder='build/builders/solutions/java'
  local extensions='*.java'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_shell {
  local builder='build/builders/solutions/shell'
  local extensions='*.sh'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_nix {
  local builder='build/builders/solutions/nix'
  local extensions='*.nix'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_python {
  local builder='build/builders/solutions/python'
  local extensions='*.py'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_csharp {
  local builder='build/builders/solutions/csharp'
  local extensions='*.cs'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_javascript {
  local builder='build/builders/solutions/javascript'
  local extensions='*.js'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_ruby {
  local builder='build/builders/solutions/ruby'
  local extensions='*.rb'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_clojure {
  local builder='build/builders/solutions/clojure'
  local extensions='*.clj'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_cpp {
  local builder='build/builders/solutions/cpp'
  local extensions='*.cpp'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_scala {
  local builder='build/builders/solutions/scala'
  local extensions='*.scala'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_php {
  local builder='build/builders/solutions/php'
  local extensions='*.php'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_rust {
  local builder='build/builders/solutions/rust'
  local extensions='*.rs'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_d {
  local builder='build/builders/solutions/d'
  local extensions='*.d'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_crystal {
  local builder='build/builders/solutions/crystal'
  local extensions='*.cr'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_ocaml {
  local builder='build/builders/solutions/ocaml'
  local extensions='*.ml'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_haskell {
  local builder='build/builders/solutions/haskell'
  local extensions='*.hs'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_go {
  local builder='build/builders/solutions/go'
  local extensions='*.go'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_c {
  local builder='build/builders/solutions/c'
  local extensions='*.c'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_lua {
  local builder='build/builders/solutions/lua'
  local extensions='*.lua'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_r {
  local builder='build/builders/solutions/r'
  local extensions='*.r'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_erlang {
  local builder='build/builders/solutions/erlang'
  local extensions='*.erl'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_dart {
  local builder='build/builders/solutions/dart'
  local extensions='*.dart'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_coffeescript {
  local builder='build/builders/solutions/coffeescript'
  local extensions='*.coffee'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_elixir {
  local builder='build/builders/solutions/elixir'
  local extensions='*.exs'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_fsharp {
  local builder='build/builders/solutions/fsharp'
  local extensions='*.fs'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_haxe {
  local builder='build/builders/solutions/haxe'
  local extensions='*.hx'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_octave {
  local builder='build/builders/solutions/octave'
  local extensions='*.m'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_perl {
  local builder='build/builders/solutions/perl'
  local extensions='*.pl'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_pascal {
  local builder='build/builders/solutions/pascal'
  local extensions='*.pas'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_awk {
  local builder='build/builders/solutions/awk'
  local extensions='*.awk'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_cobol {
  local builder='build/builders/solutions/cobol'
  local extensions='*.cbl'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_fortran {
  local builder='build/builders/solutions/fortran'
  local extensions='*.f90'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_prolog {
  local builder='build/builders/solutions/prolog'
  local extensions='*.pro'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_kotlin {
  local builder='build/builders/solutions/kotlin'
  local extensions='*.kt'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_typescript {
  local builder='build/builders/solutions/typescript'
  local extensions='*.ts'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_lisp {
  local builder='build/builders/solutions/lisp'
  local extensions='*.lsp'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_build_solutions_clojurescript {
  local builder='build/builders/solutions/clojurescript'
  local extensions='*.cljs'
  local folders=(
    'code'
  )

      helper_use_pristine_workdir \
  &&  helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
}

function job_test_schemas {
  local lang_data_supported='code/lang-data-supported.yml'
  local lang_schema_supported='code/lang-schema-supported.yml'
  local lang_data_dropped='code/lang-data-dropped.yml'
  local lang_schema_dropped='code/lang-schema-dropped.yml'
  local policies_data='policies/data.yaml'
  local policies_schema='policies/schema.yaml'

      helper_use_pristine_workdir \
  &&  env_prepare_python_packages \
  &&  helper_test_pykwalify \
        "${lang_data_supported}" \
        "${lang_schema_supported}" \
  &&  helper_test_pykwalify \
        "${lang_data_dropped}" \
        "${lang_schema_dropped}" \
  &&  helper_test_pykwalify \
        "${policies_data}" \
        "${policies_schema}" \
  &&  helper_test_schemas_site_data
}

function job_test_generic {
      helper_use_pristine_workdir \
  &&  env_prepare_python_packages \
  &&  env_prepare_ruby_modules \
  &&  helper_test_generic_dir_depth \
  &&  helper_test_generic_only_png_evidences \
  &&  helper_test_generic_misplaced_evidences \
  &&  helper_test_generic_allowed_mimes \
  &&  helper_test_generic_atfluid_account \
  &&  helper_test_generic_short_filenames \
  &&  helper_test_generic_raw_github_urls \
  &&  helper_test_generic_no_asc_extension \
  &&  helper_test_generic_no_tabs \
  &&  helper_test_generic_only_allowed_characters_in_paths \
  &&  helper_test_generic_80_columns \
  &&  helper_test_generic_pre_commit \
  &&  prospector --profile build/configs/prospector.yaml build
}

function job_test_others {
      helper_use_pristine_workdir \
  &&  helper_test_generic_others_duplicates \
  &&  helper_test_generic_others_code_has_unique_ext \
  &&  helper_test_generic_others_code_raw_only \
  &&  helper_test_generic_others_code_sort_by_ext \
  &&  helper_test_others_urls_status_code 'LINK.lst' \
  &&  helper_test_others_urls_status_code 'OTHERS.lst'
}

function job_test_policy {
  if helper_is_solution_commit
  then
        helper_use_pristine_workdir \
    &&  env_prepare_python_packages \
    &&  python3 build/modules/test-policy/main.py
  else
    return 0
  fi
}

function job_test_commit_msg {
      helper_use_pristine_workdir \
  &&  env_prepare_python_packages \
  &&  helper_test_commit_msg_commitlint \
  &&  pushd build/modules/parse-commit-msg \
  &&  pushd tests \
  &&  echo '[INFO] Testing the parser' \
  &&  ./test-commit-msg-parser.sh \
  &&  popd || return 1 \
  &&  echo '[INFO] Using the parser to check your commit message' \
  &&  ./commit_msg_parser.py \
  &&  popd || return 1
}

function job_pages {
      env_set_utf8_encoding \
  &&  env_prepare_ruby_modules \
  &&  helper_pages_compile
}

function job_pages_local {
      helper_use_pristine_workdir \
  &&  env_set_utf8_encoding \
  &&  env_prepare_ruby_modules \
  &&  helper_pages_compile \
  &&  nanoc view
}

function job_reviews {

  function reviews {
    export __NIX_PATH
    export __NIX_SSL_CERT_FILE

    NIX_PATH="${__NIX_PATH}" \
    NIX_SSL_CERT_FILE="${__NIX_SSL_CERT_FILE}" \
      "${srcProduct}/bin/reviews" "${@}"
  }

      helper_use_pristine_workdir \
  &&  reviews reviews.toml
}
