source "${srcGeneric}"

function compile {
  local solution="${1}"

  clisp \
    -c "${solution}"
}

function build {
      generic_set_utf_8 \
  &&  generic_get_solution \
  &&  compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
