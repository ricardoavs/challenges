#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun delimiterp (c)
  (char= c #\Space)
)

(defun split (string &key (delimiterp #'delimiterp))
  (loop
    :for beg = (position-if-not delimiterp string)
    :then (position-if-not delimiterp string :start (1+ end))
    :for end = (and beg (position-if delimiterp string :start beg))
    :when beg :collect (subseq string beg end)
    :while end
  )
)

(defun find-min (line)
  (let ((number-list (sort (map 'list #'parse-integer (split line)) #'<)))
    (format t "~a " (nth 0 number-list))
  )
)

(defun solve-all (size-input)
  (loop for i from 0 to (- size-input 1)
    do(find-min (read-line))
  )
)

(defvar size-input (read))
(solve-all size-input)

#|
  cat DATA.lst | clisp bridamo98.lsp
  -6428612 -6763437 -8697062 -6773166 1379899 -9853923 -9051368 -9889194
  -3497897 -109995 -6259293 -2687905 2260302 -9227149 -5619399 -5473322
  -4524690 1006052 -7347072 -5923032 -2040122 -5878363 -8413666 -5079171
  6506224 -7513502 -3398672
|#
