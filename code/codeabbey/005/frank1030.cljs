;$ clj-kondo --lint frank1030.cljs
;linting took 82ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core]))

(defn validator [index]
  (let [x (atom index) a (core/read) b (core/read) c (core/read)]
    (if (> @x 0)
      (do (swap! x dec)
        (if (and (< a b) (< a c))
          (println a)
          (if (and (< b a) (< b c))
            (println b)
            (if (and (< c a) (< c b))
              (println c)
              (print 0))))
        (if (not= @x 0)
          (validator @x)
          (print "")))
      (print ""))))

(defn main []
  (let [index (core/read)]
    (validator index)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;-6428612 -6763437 -8697062 -6773166 1379899 -9853923 -9051368 -9889194
;-3497897 -109995 -6259293 -2687905 2260302 -9227149 -5619399 -5473322
;-4524690 1006052 -7347072 -5923032 -2040122 -5878363 -8413666 -5079171
;6506224 -7513502 -3398672
