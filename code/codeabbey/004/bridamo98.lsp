#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun compare (size-input)
  (let ((i 1) (first-number 0) (second-nomber 0))
    (loop
      (setq first-number (read))
      (setq second-number (read))
      (if (< first-number second-number)
        (format t "~a " first-number)
        (format t "~a " second-number)
      )
      (setq i (+ i 1))
      (when (> i size-input)(return NIL))
    )
  )
)

(defvar size-input (read))
(compare size-input)

#|
  cat DATA.lst | clisp bridamo98.lsp
  586254 6209071 -6824287 -7578938 2536687 -6785022 -8660201 1462649
  -1406074 -6803702 -3391748 1668688 -832977 -9849648 -7385857 -7910251
  -9550187 -1052633 -4872152 -8570407 -1474502
|#
