open System

[<EntryPoint>]
let main argv =

    let operations x y = if x>y then y else x
    printfn "ingrese la cantidad de pruebas:"
    let c = stdin.ReadLine() |> int  // '|> int' cast a int
    let mutable data = Array.zeroCreate (c*2)
    let mutable out = Array.zeroCreate c

    let mutable counter = 0
    for i in 0 .. c-1 do
        printfn "(%i) ingrese 2 numeros separados por un espacio:" (i+1)
        let pairArray = stdin.ReadLine().Split ' ' //array de string
        data.[counter] <- int pairArray.[0]
        data.[counter+1] <- int pairArray.[1]
        out.[i] <- operations data.[counter] data.[counter+1]
        counter <- counter + 2
    for i in 0 .. c-1 do
        printf "%i "out.[i]
    Console.ReadKey() |> ignore
    0 // devolver un codigo de salida entero
