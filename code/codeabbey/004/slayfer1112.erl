% $ erl -compile slayfer1112.erl
% $ erl -noshell -s erlint slayfer1112 -s init stop
% ok

-module(slayfer1112).

-import(lists, [nth/2]).

-export([start/0]).

-compile(slayfer1112).

% Read file and convert in binary list
readfile(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  string:tokens(erlang:binary_to_list(Binary), "\r\n").

% Take a binary and convert in integers
integers(Binary) ->
  [element(1, string:to_integer(Integers))
   || Integers <- string:tokens(Binary, " ")].

% For loop
for(N, N, F) -> F(N);
for(I, N, F) -> F(I) ++ for(I + 1, N, F).

% Take the array and split the head (# of inputs) with the tail (the params)
% And return a list with the small number of each comparation
for_in_range(Array) ->
  [Head | Tail] = Array,
  N = string:to_integer(Head),
  {J, _} = N,
  List = for(1, J,
         fun (X) -> [integers(nth(X, Tail))] end),
  Min = for(1, J,
        fun (X) ->
          [compare_two(nth(1, nth(X, List)), nth(2, nth(X, List)))]
        end),
  Min.

% Compare what is the small number
compare_two(A, B) ->
  case A of
    _ when A > B -> B;
    _ -> A
  end.

% Print the result
result(F, List) -> [F(X) || X <- List].

% Function that run all the proccess
start() ->
  Array = readfile("DATA.lst"),
  List_mins = for_in_range(Array),
  Print = fun (X) -> io:fwrite("~w ", [X]) end,
  result(Print, List_mins),
  io:fwrite("~n").

% erl -noshell -s slayfer1112 -s init stop
% 586254 6209071 -6824287 -7578938 2536687 -6785022 -8660201 1462649 -1406074
% -6803702 -3391748 1668688 -832977 -9849648 -7385857 -7910251 -9550187
% -1052633 -4872152 -8570407 -1474502
