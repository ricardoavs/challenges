;;clisp -c paolagiraldo.lsp
;;Compiling file /home/../paolagiraldo.lsp ...
;;Wrote file /home/../paolagiraldo.fas
;;0 errors, 0 warnings
;;Bye.

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
      (loop :for line = (read-line nil nil)
            :while line
            :collect (read-from-string (concatenate 'string "(" line ")")))))
(defvar data)
(defvar legs)
(setq data (cdr (read-data)))
(dolist (item data)
  (setq legs (+ (expt (first item) 2) (expt (second item) 2)))
  (COND ((> legs (expt (third item) 2)) (print 'A))
    ((< legs (expt (third item) 2)) (print 'O))
    (T (print 'R))))

;;cat DATA.lst | clisp paolagiraldo.lsp
;;R  O  R  O  O  O  A  A  O  A  A  A  O  R  O  R  O  R  A  O  R  O  O  R
;;O  R
