clc
clear all
inp=[4 4 4 4 4
3 4 4 4 4
2 3 4 5 6
2 3 4 5 6
2 2 6 4 1
2 3 4 5 6
2 3 4 5 1
4 2 2 2 3
6 5 3 4 3
2 2 2 2 2
6 3 6 6 6
2 2 2 2 2
2 6 5 2 5
2 3 4 5 6
2 3 4 5 6
5 6 6 5 5
2 3 4 5 1
2 3 4 5 1
1 2 1 6 1
6 3 3 2 3
2 3 4 5 6
2 3 4 5 1
2 3 4 5 1
2 3 4 5 1
6 6 6 6 6];
out=[];
for i=1:size(inp,1)
    mats=sort(inp(i,:));
    a = unique(mats);
    frec = [histc(mats,a)];
     if size(frec,2)==5 && sum(mats)==20
       res='big-straight';
     endif
     if size(frec,2)==5 && sum(mats)==15
        res='small-straight';
     endif
     if (numel(find(frec==3))) == 1 && (numel(find(frec==2)))==1
        res='full-house';
     endif
     if (numel(find(frec==2))==2)
        res='two-pairs';
     endif
     if size(frec)==1
        res='yacht';
     endif
     if (numel(find(frec==4))==1)
        res='four';
     endif
     if (numel(find(frec==3))==1) && (numel(find(frec==2))==0)
        res='three';
     endif
     if (numel(find(frec==2))==1) && (numel(find(frec==3))==0)
        res='pair';
     endif
     if size(frec,2)==5 && (sum(mats) <= 19) && (sum(mats)>=16)
        res='none';
     endif
     out=[out, cellstr(res)];
end
disp(out)

