#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defvar *data*)
(defvar *letter*)
(defparameter *counter* 0)

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect line)))

(setq *data* (cdr (read-data)))
(setq *counter* 0)
(dolist (item *data*)
  (loop for i across item
    do(setq *letter* (string i))
    (cond ((or (string= *letter* "a") (string= *letter* "e")
               (string= *letter* "i") (string= *letter* "o")
               (string= *letter* "u") (string= *letter* "y"))
          (setq *counter* (+ *counter* 1)))))

  (format t "~s " *counter*)
  (setq *counter* 0))

#|
cat DATA.lst | clisp ludsrill.lsp
11 14 13 19 5 14 15 9 8 8 6 8 8 7 9
|#
