;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (subvec data 1 (alength (to-array-2d data)))]
    [head
     body]))

(defn matching-brackets [brackets steps i]
  (if (< i steps)
    (let [new-brackets (str/replace brackets #"(\[\])|(\{\})|(\<\>)|(\(\))" "")
          len (count new-brackets)]
      (if (= len 0)
        1
        (matching-brackets new-brackets steps (inc i))))
    0))

(defn solution [data]
  (let [brackets (str/replace data #"[^(\[{<)\]}>]" "")
        steps (count brackets)
        matching (matching-brackets brackets steps 0)]
    (print (str matching " "))))

(defn main []
  (let [[_ body] (get-data)]
    (doseq [x body]
      (solution x))
    (println)))
(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 0 0 1 1 0 1 1 1 1 1 1 0 0 0 0 1 1 1 1 0
