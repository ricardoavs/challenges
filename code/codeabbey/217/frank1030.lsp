#|
$ sblint -v frank1030.lsp
[INFO] Lint file frank1030.lsp

$ clisp -c frank1030.lsp
Compiling file /Users/Frank/Desktop/frank/217/frank1030.lsp ...
Wrote file /Users/Frank/Desktop/frank/217/frank1030.fas
0 errors, 0 warnings
Bye.
|#

(defun get-dat()
  (let ((dat))
    (setq dat(read))
  )
)

(defun split (string)
  (loop for i = 0 then (1+ j)
    as j = (position #\- string :start i)
    collect (subseq string i j)
    while j
  )
)

(defun get-body (body-minds n a)
  (let ((index 0))
    (loop for i from 0 to (- n 1)
      do (if (string-equal a (aref body-minds i 0))
        (setq index i)
      )
    )
    (return-from get-body index)
  )
)

(defun return-minds (body-minds n)
  (let ((x (make-array '(1 2) :initial-element "X"))
    (y (make-array '(1 2) :initial-element "Y")) (y1 0) (y2 ""))
    (setq y2 (aref x 0 1))
    (setf (aref x 0 1) (aref body-minds 0 1))
    (setf (aref body-minds 0 1) y2)
    (format t "~a-~a " (aref x 0 0) (aref body-minds 0 0))
    (loop for i from 1 to (- n 2)
      do (format t "~a-~a " (aref x 0 0) (aref body-minds
        (get-body body-minds n (aref x 0 1)) 0))
      do (setq y1 (get-body body-minds n (aref x 0 1)))
      do (setq y2 (aref body-minds y1 1))
      do (setf (aref body-minds y1 1) (aref x 0 1))
      do (setf (aref x 0 1) y2)
    )
    (setq y1 (get-body body-minds n (aref x 0 1)))
    (setq y2 (aref body-minds y1 1))
    (setf (aref body-minds y1 1) (aref y 0 1))
    (setf (aref y 0 1) y2)
    (format t "~a-~a " (aref x 0 0) (aref body-minds y1 0))
    (setq y1 (get-body body-minds n (aref y 0 1)))
    (setq y2 (aref body-minds y1 1))
    (setf (aref body-minds y1 1) (aref y 0 1))
    (setf (aref y 0 1) y2)
    (format t "~a-~a " (aref y 0 0) (aref body-minds y1 0))
    (setq y1 (get-body body-minds n (aref x 0 1)))
    (setq y2 (aref body-minds y1 1))
    (setf (aref body-minds y1 1) (aref x 0 1))
    (setf (aref x 0 1) y2)
    (format t "~a-~a " (aref x 0 0) (aref body-minds y1 0))
    (setq y2 (aref x 0 1))
    (setf (aref x 0 1) (aref y 0 1))
    (setf (aref y 0 1) y2)
    (format t "~a-~a " (aref x 0 0) (aref y 0 0))
    (return-from return-minds body-minds)
  )
)

(defun changes (body-minds s n)
  (let ((x-y 0) (c 0) (sw 0) (y1 0) (y2 ""))
    (loop for i from 0 to (- s 1)
      do (setq x-y (get-dat))
      do (setq x-y (string x-y))
      do (setq x-y (split x-y))
      do (loop while (= sw 0)
        do (if (string-equal (aref body-minds c 0) (car x-y))
          (progn
            (setf (aref body-minds c 0) (car x-y))
            (setq y2 (aref body-minds c 1))
            (setq y1 (get-body body-minds n (cadr x-y)))
            (setf (aref body-minds c 1) (aref body-minds y1 1))
            (setq sw 1)
          )
        )
        do (setq c (+ c 1))
      )
      do (setq c 0)
      do (loop while (= sw 1)
        do (if (string-equal (aref body-minds c 0) (cadr x-y))
          (progn
            (setf (aref body-minds c 0) (cadr x-y))
            (setf (aref body-minds c 1) y2)
            (setq sw 0)
          )
          (setq c (+ c 1))
        )
      )
      do (setq c 0)
    )
    (setf (aref body-minds 26 0) (car x-y))
    (setf (aref body-minds 26 1) (cadr x-y))
    (return-from changes body-minds)
  )
)

(defun main()
  (let ((n 0) (s 0) (body-minds (make-array '(27 2) :initial-contents
    '(("A" "A") ("B" "B") ("C" "C") ("D" "D") ("E" "E") ("F" "F")
    ("G" "G") ("H" "H") ("I" "I") ("J" "J") ("K" "K") ("L" "L")
    ("M" "M") ("N" "N") ("O" "O") ("P" "P") ("Q" "Q") ("R" "R")
    ("S" "S") ("T" "T") ("U" "U") ("V" "V") ("W" "W") ("X" "X")
    ("Y" "Y") ("Z" "Z") ("0" "0")))))
    (setq n (get-dat))
    (setq s (get-dat))
    (setf body-minds (changes body-minds s n))
    (print body-minds)
    (setf body-minds (return-minds body-minds n))
    (print body-minds)
  )
)

(main)

#|
$ cat DATA.lst | clisp frank1030.lsp
X-A X-L X-E X-J X-I X-H X-B X-M X-G X-C X-D X-K X-F Y-A X-F X-Y
|#
