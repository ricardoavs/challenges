#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect line)))

(defun split-by-one-space (item)
  (loop for i = 0 then (1+ j)
    as j = (position #\Space item :start i)
    collect (subseq item i j)
    while j))

(defun split-by-colon (item)
  (loop for i = 0 then (1+ j)
    as j = (position #\: item :start i)
    collect (subseq item i j)
    while j))

(defun calculation-coordinates ()
  (let ((data)
        (theta-hour)
        (aux-hour)
        (xp-minute)
        (yp-minute)
        (theta-minute)
        (hour) (minute)
        (hour-to-degree)
        (hour-and-minute)
        (minute-to-degree)
        (xp-hour) (yp-hour)
        (x-hour) (x-minute)
        (y-hour) (y-minute))
    (setq data (split-by-one-space (first (cdr (read-data)))))
    (dolist (item data)
      (setq hour-and-minute (split-by-colon item))
      (setq hour (parse-integer (first hour-and-minute)))
      (setq minute (parse-integer (second hour-and-minute)))
      (setq aux-hour (/ (* (/ (* minute 30) 60) pi) 180))
      (setq hour-to-degree (- (* (/ (* (- 30) pi) 180) hour) aux-hour))
      (setq minute-to-degree (* (/ (* (- 6) pi) 180) minute))

      (setq xp-hour 0)
      (setq yp-hour 6)
      (setq theta-hour hour-to-degree)
      (setq x-hour (+ (- (* (cos theta-hour) xp-hour)
                    (* (sin theta-hour) yp-hour)) 10))
      (setq y-hour (+ (+ (* (sin theta-hour) xp-hour)
                    (* (cos theta-hour) yp-hour)) 10))

      (setq xp-minute 0)
      (setq yp-minute 9)
      (setq theta-minute minute-to-degree)
      (setq x-minute (+ (- (* (cos theta-minute) xp-minute)
                      (* (sin theta-minute) yp-minute)) 10))
      (setq y-minute (+ (+ (* (sin theta-minute) xp-minute)
                      (* (cos theta-minute) yp-minute)) 10))

      (format t "~F " x-hour)
      (format t "~F " y-hour)
      (format t "~F " x-minute)
      (format t "~F " y-minute))))

(calculation-coordinates)

#|
cat DATA.lst | clisp ludsrill.lsp
14.9447571317320939705 13.39843742154899699 2.718847050625473185
15.290067270632258165 12.2961005941905386326 15.543277195067720536
1.0 10.000000000000000002 15.857776042719600196
11.298637683628617277 5.4999999999999999974 2.2057713659400521805
14.914912265733950739 13.441458618106276575 2.2057713659400521814
14.5000000000000000035 8.958110933998417907 4.091153481926751644
17.79422863405994782 5.4999999999999999987 7.6556132290643574747
15.523029120714641966 18.950697058314460032 10.940756169408881242
4.0911534819267516426 8.958110933998417913 2.2057713659400521775
5.5000000000000000026
|#
