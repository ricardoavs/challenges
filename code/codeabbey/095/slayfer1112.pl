# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 slayfer1112.pl
# slayfer1112.pl source OK
#
# Compile:
# $ perl -c slayfer1112.pl
# slayfer1112.pl syntax OK

package slayfer1112;

use strict;
use warnings FATAL => 'all';
our ($VERSION) = 1;

use Readonly;
use POSIX;
use List::Util qw(reduce);

my $EMPTY = q{};
my $SPACE = q{ };

sub data_in {
  my @data = ();
  while ( my $line = (<>) ) {
    push @data, $line;
  }
  return @data;
}

sub create_x {
  my @args = @_;
  my @vals = @args;
  return $vals[0][1];
}

sub create_y {
  my @args = @_;
  my @vals = @args;
  return $vals[0][2];
}

sub solution {
  my @args        = @_;
  my @vals        = @args;
  my @x           = map { create_x($_); } @vals;
  my @y           = map { create_y($_); } @vals;
  my $xpro        = ( reduce { $a + $b } @x ) / ( $#x + 1 );
  my $ypro        = ( reduce { $a + $b } @y ) / ( $#y + 1 );
  my @x_xpro      = map { $_ - $xpro } @x;
  my @y_ypro      = map { $_ - $ypro } @y;
  my @xx_yy       = map { $x_xpro[$_] * $y_ypro[$_] } 0 .. $#x;
  my $x_xpro_sqrt = reduce { $a + $b } ( map { $_ * $_ } @x_xpro );
  my $y_ypro_sqrt = reduce { $a + $b } ( map { $_ * $_ } @y_ypro );
  my $sxy         = ( reduce { $a + $b } @xx_yy ) / ( $#x + 1 );
  my $sx          = sqrt( $x_xpro_sqrt / ( $#x + 1 ) );
  my $sy          = sqrt( $y_ypro_sqrt / ( $#y + 1 ) );
  my $p           = $sxy / ( $sx * $sy );
  my $k           = $p * ( $sy / $sx );
  my $b           = $ypro - ( $k * $xpro );
  exit 1 if !print "$k $b";
  return 'To solve this I studied again about linear regresions';
}

sub main {
  my @vals = data_in();
  my @data = ();
  for ( 1 .. $#vals ) {
    my @val = split $SPACE, $vals[$_];
    push @data, [@val];
  }
  solution @data;
  exit 1 if !print "\n";
  return 'Solved in the date 25/07/2020';
}

main();
1;

# $ cat DATA.lst | perl slayfer1112.pl
# 12583026226
