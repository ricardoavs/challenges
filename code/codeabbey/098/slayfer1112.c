/*
linter:
cppcheck \
  --error-exitcode=1 \
  slayfer1112.c \
&&  splint \
  -strict \
  -internalglobs \
  -modfilesys \
  -boundsread \
  slayfer1112.c
Checking slayfer1112.c ...
Splint 3.1.2 --- 05 Sep 2017

Finished checking --- no warnings

compilation:
gcc \
  -Wall \
  -Wextra \
  -Winit-self \
  -Wuninitialized \
  -Wmissing-declarations \
  -Winit-self \
  -ansi \
  -pedantic \
  -Werror \
  slayfer1112.c \
  -o slayfer1112 \
  -lm
*/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#define MAX_SIZE 1024
#define M_PI acos(-1.0)

typedef struct Coords {
  double x;
  double y;
} Coords;

static double azimuth_to_polar (/*@reldef@*/ double azimuth)
  /*@modifies nothing@*/ {
  double degrees;
  /*when azimuth is beetwen 0 and 90 is in the segment +X, +Y*/
  if (azimuth <= 90.0) {
    degrees = (90 - azimuth) * (M_PI / 180);
  }
  /*when azimuth is > 90 you can take 360 and sustract the difference beetwen
    90 and the azimuth, because when azimuth = 90 it becomes the X axis*/
  else {
    degrees = (360 - (azimuth - 90)) * (M_PI / 180);
  }
  return degrees;
}

static Coords polar_to_cartesian (/*@reldef@*/ double r,
  /*@reldef@*/ double degrees)
  /*@modifies nothing@*/ {
  static Coords result;
  result.x = r * cos(degrees);
  result.y = r * sin(degrees);
  return result;
}

/*
Azimuth is a kind of coordinated system, very similar to polar system
The difference is that Azimuth the degrees start in Y axis and polar
start in X axis, for that reason we can tranform azimuth to polar
and convert polar to cartesian and sum the values for X and Y to get
The point to dig
*/
int main(void) {
  char temp[MAX_SIZE];
  double r, azimuth;
  double degrees;
  Coords coords;
  double px = 0.0, py = 0.0;
  /*Ignore the first line*/
  if (fgets(temp, MAX_SIZE, stdin) == 0) { return 0; }
  while (true) {
    if (fgets(temp, MAX_SIZE, stdin) == 0) {
      return 0;
    }
    /*Stops the loop when find the Dig point*/
    if (temp[0] == 'D') {
      break;
    }
    if (sscanf(temp, "go %le feet by azimuth %le", &r, &azimuth) == 0) {
      return 0;
    }
    degrees = azimuth_to_polar(azimuth);
    coords = polar_to_cartesian(r,degrees);
    px += coords.x;
    py += coords.y;
  }
  printf("%d %d", (int) px, (int) py);
  printf("\n");
  return 1;
}

/*
$ cat DATA.lst | ./slayfer1112
603 3286
*/
