#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun april-fool ())
  (let ((data)
        (result))
    (setq data (cdr (read-data)))
    (setq result 0)
    (dolist (item data)
      (loop for i in item
        do(setq result (+ result (expt i 2))))
      (format t "~s " result)
      (setq result 0)))

(april-fool)

#|
cat DATA.lst | clisp ludsrill.lsp
2075 355 761 84 306 45 120 266 189 45 294 89 352 619 1980 567 586 766
|#
