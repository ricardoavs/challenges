/*
$ rustfmt rechavar.rs
$ rustc rechavar.rs
*/
use std::io::{self, Read};

fn next_step(coord: (i32, i32), h_dir: i32, v_dir: i32) -> (i32, i32) {
  let (x0, y0) = coord;
  let x1 = x0 + 1 * h_dir;
  let y1 = y0 + ((x1 - x0) / h_dir) * v_dir;
  (x1, y1)
}

fn need_changes(coord: (i32, i32), whl_array: &Vec<i32>) -> (bool, bool) {
  let (x, y) = coord;
  let mut answer_x = false;
  let mut answer_y = false;
  if 0 >= x || x + whl_array[2] >= whl_array[0] {
    answer_x = true;
  }
  if 0 >= y || y + 1 >= whl_array[1] {
    answer_y = true;
  }
  (answer_x, answer_y)
}

fn make_array(whl_array: Vec<i32>) -> Vec<String> {
  let mut coord = (0, 0);
  let mut dir_x = 1;
  let mut dir_y = 1;
  let mut answer = Vec::new();
  for _i in 0..100 {
    coord = next_step(coord, dir_x, dir_y);
    let (change_x, change_y) = need_changes(coord, &whl_array);
    if change_x {
      dir_x = dir_x * -1;
    }
    if change_y {
      dir_y = dir_y * -1;
    }
    let (x, y) = coord;
    answer.push(x.to_string());
    answer.push(" ".to_string());
    answer.push(y.to_string());
    answer.push(" ".to_string());
  }
  answer
}

fn main() -> io::Result<()> {
  let mut buffer = String::new();
  io::stdin().read_to_string(&mut buffer)?;
  let input_array: Vec<&str> = buffer.split("\n").collect();

  let whl_array: Vec<i32> = input_array[0]
    .split(" ")
    .map(|x| x.parse::<i32>().unwrap())
    .collect();

  whl_array.to_vec();
  let answer = make_array(whl_array);
  println!("0 0 {}", answer.into_iter().collect::<String>());
  Ok(())
}

/*
$ cat DATA.lst | ./rechavar
0 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13 13 14 14 15 13 16
12 17 11 18 10 19 9 20 8 21 7 22 6 23 5 24 4 25 3 26 2 27 1 26 0 25 1 24 2 23 3
22 4 21 5 20 6 19 7 18 8 17 9 16 10 15 11 14 12 13 13 12 14 11 13 10 12 9 11 8
10 7 9 6 8 5 7 4 6 3 5 2 4 1 3 0 2 1 1 2 0 3 1 4 2 5 3 6 4 7 5 8 6 9 7 10 8 11
9 12 10 13 11 14 12 15 13 16 14 17 13 18 12 19 11 20 10 21 9 22 8 23 7 24 6 25
5 26 4 27 3 26 2 25 1 24 0 23 1 22 2 21 3 20 4 19 5 18 6 17 7 16 8 15 9 14 10
13 11 12 12 11 13 10 14 9 13 8 12
  */
