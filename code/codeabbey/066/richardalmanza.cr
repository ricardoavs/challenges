#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.74 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

ALPHABET = ("A".."Z").to_a.join

def decrypt(str, k)
  oput = ""

  str.each_char do |chr|
    if !chr.in?(ALPHABET)
      oput = "#{oput}#{chr}"
      next
    end
    id = ((ALPHABET.index(chr) || 0) - k) % ALPHABET.size
    oput = "#{oput}#{ALPHABET[id]}"
  end

  oput
end

args = ARGV
fluid = args == [] of String

top_50_common_english_words = [
                                "as", "i", "his", "that", "he",
                                "was", "for", "on", "are", "with",
                                "they", "be", "at", "one", "have",
                                "this", "from", "by", "hot", "word",
                                "but", "what", "some", "is", "it",
                                "you", "or", "had", "the", "of", "to",
                                "and", "a", "in", "we", "can", "out",
                                "other", "were", "which", "do", "their",
                                "time", "if", "will", "how", "said", "an",
                                "each", "tell"
                              ]

args = File.read("DATA.lst").split("\n") if fluid
args = args[0].split("\n") if !fluid

args = args[1..]

args.each do |x|
  possibilities = [] of Tuple(Int32, Int32, String) # [key, counter, string]

  (0...ALPHABET.size).each do |key|
    counter = 0
    msg = decrypt(x, key)

    msg.split.each do |word|
      counter += 1 if word.downcase.in?(top_50_common_english_words)
    end

    possibilities << {key, counter, msg}
  end

  possibilities = possibilities.sort { |a, b| b[1] <=> a[1] }

  possibilities[0][2].split[...3].each do |word|
    print "#{word} "
  end

  print "#{possibilities[0][0]} "
end

puts

# $ ./richardalmanza.cr
# POETRY IS WHAT 5 CARTHAGE MUST BE 22 NO SOONER SPOKEN 21 IT IS BLACK 6
# THAT ALL MEN 18 0
