/*
# Lint
$ ktlint --experimental --verbose --disabled_rules=experimental:indent,indent \
> miyyer1946.ky
# Compile
$ kotlinc -Werror miyyer1946.kt -include-runtime -d miyyer1946.jar
*/

import kotlin.math.PI
import kotlin.math.tan

fun main(vararg args: String) {
  val string = readLine()!!.split(" ")
  var cont = 2
  var result: Long
  string.map {
    if (cont <= string.size - 1) {
    var s = tan(((string[cont].toDouble() - 90) * PI) / 180)
    cont += 1
    result = Math.round((s * string[cont - 2].toDouble()))
    cont += 1
    println(result)
    }
}
}

/*
$ cat DATA.lst | java -jar miyyer1946.jar
29 47 64 29 29 58 54 27 48 25 15 51 47
*/
