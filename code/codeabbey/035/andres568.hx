/**
  linting
  $ haxelib run checkstyle -s Andres568.hx
  Running Checkstyle v2.6.1 using 79/79 checks on 1 source file...
  No issues found.
**/

using StringTools;

/**
  class with the main method
**/
class Main {

  // Haxe applications have a static entry point called main
  static function main() {
    var cont:String = sys.io.File.getContent("DATA.lst");
    var lines:Array<String> = cont.split("\n");
    var inputData:Array<String> = [];
    var result:String = "";
    var years:Int = 0;
    var prec:Int = 100;
    var money:Float = 0;
    var interestRate:Float = 0;
    lines.shift();
    lines.pop();

    for (i in 0...lines.length) {
      years = 0;
      inputData = lines[i].split(" ");
      money = Std.parseInt(inputData[0]);
      interestRate = Std.parseInt(inputData[2]) / 100;

      while (true) {
        if (Std.parseInt(inputData[1]) < money) {
          break;
        }

        money = Std.int(money * (interestRate + 1) * prec) / prec;
        years++;
      }

      result += " " + years;
    }

    trace(result);
  }
}
/**
   $ haxe -main Andres568 --interp
   Andres568.hx:46:  9 31 8 8 10 26 7 43 8 66 7 100 37 48 6 17 10 8
**/
