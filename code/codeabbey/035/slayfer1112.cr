#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 8.48 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Float64)
  data.each_line do |x|
    inter = [] of Float64
    (x.split).each do |y|
      y = y.is_a?(String) ? y.try &.to_f : y
      inter << y
    end
    values << inter
  end
  values[1..]
end

def solution(array)
  s = array[0]
  r = array[1]
  p = ( array[2] / 100 ) + 1
  y = 0
  while s<=r
    y += 1
    s = (s * p).round(2)
  end
  print "#{y} "
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# 109 109 273 302 163 137 163 21 92 324 163 163 221 163 7
