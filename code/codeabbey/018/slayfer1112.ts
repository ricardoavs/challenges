/*
$ eslint slayfer1112.ts
$ tsc slayfer1112.ts
*/

function getRootSqrt(
  root: number,
  lim: number,
  val: number,
  counter: number
): number {
  if (counter < lim) {
    const middle = val / root;
    return getRootSqrt((root + middle) / 2, lim, val, counter + 1);
  }
  return root;
}

function solution(entry: string[]): number {
  const entryF: number[] = entry.map(parseFloat);
  const check: number = getRootSqrt(1, entryF[1], entryF[0], 0);
  process.stdout.write(`${check} `);
  return 0;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const dat: string[] = entryArray.slice(1, entryArray.length);
    dat.map((value) => solution(value.split(' ')));
    process.stdout.write('\n');
    return 0;
  });
  return 0;
}

main();
/*
$ cat DATA.lst | node slayfer1112.js
219.2680589727784 1649.559110569358 24.48936170212766 126.31114538360205
8.366600265340756 87.6818422209526 7.0710678118654755 159.2930151371662
6.782329983125268 70.12851739897322 694.4357809978496
*/
