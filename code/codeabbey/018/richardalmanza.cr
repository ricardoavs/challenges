#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.94 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

def sqroot_(pow, iter)
  r = 1.0
  (0...iter).each do |_|
    r = r + pow / r
    r = r / 2
  end
  if r - r.to_i == 0.0
    r.to_i
  else
    r.round 10
  end
end

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map {|x| x.to_i}

(0...args.size).step(2).each  do |x|
  print "#{sqroot_(args[x], args[x + 1])} "
end

puts

# $ ./richardalmanza.cr
# 7.4161984871 6.0827625303 22.0680764907 293.5188235492 5.4772255756
# 81.1541742611 59.6741149913 89.7050723204 2178.9583211891 27.5317997959
# 12.7550781622 9.1104335792 4.8989794856
