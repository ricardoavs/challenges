# $ pylint juanksepul.py
# -------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 9.09/10, +0.91)
# $ mypy juanksepul.py
# Success: no issues found in 1 source file
"""Solution for the codeabbey challenge #6"""

import sys
import math
from typing import List


def input_data() -> List[str]:
    """Reads the input"""
    return sys.stdin.readlines()[1:]


def split_round(line: str) -> None:
    """Splits the str input and prints the rounded result of their division"""
    num_x, num_y = line.split()
    div = int(num_x) / int(num_y)
    print(math.floor(div + 0.5), ' ')


def main() -> None:
    """Executes the tasks"""
    data = input_data()
    list(map(split_round, data))


main()

# $ cat DATA.lst | python juanksepul.py
# 3
# 6
# 3
# 16040
# -2
# 14
# 17320
# 13
# 31766
# 18
# 18
# 3
# 5
# 41470
# 19
# 37201
# 8
# 17622
# 9
# 17
# 10
# -8
# 2
# 18
# 18
# 9
# 8
# 21
# 18
# 5
# 2
