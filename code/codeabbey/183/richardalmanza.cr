#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 42.13 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

require "http"

URL = "http://open-abbey.appspot.com/interactive/connect4"

COLUMNS = 9
ROWS    = 8

enum Pos
  Col
  Row
end

enum Direction
  Up
  Left
  Right
  DiagonalPR
  DiagonalPL
  DiagonalNR
  DiagonalNL
end

def get_positions(matrix, symbol)
  positions = [] of Tuple(Int32, Int32)

  (0...COLUMNS * ROWS).each do |pos|
    positions << to_2d(pos) if matrix[pos] == symbol
  end

  positions
end

def to_1d(pos)
  pos[Pos::Row.value] * COLUMNS + pos[Pos::Col.value]
end

def to_2d(pos)
  {pos % COLUMNS, pos // COLUMNS}
end

def get_x(pos)
  pos[Pos::Col.value]
end

def get_y(pos)
  pos[Pos::Row.value]
end

def case_vector(vector)
  col = 1
  row = 1
  left = false
  down = false

  case vector
  when .up?,
       .diagonal_pr?, .diagonal_pl?,
       .diagonal_nr?, .diagonal_nl?
    row = ROWS
  else
  end

  case vector
  when .diagonal_pr?, .diagonal_pl?,
       .diagonal_nr?, .diagonal_nl?,
       .left?, .right?
    col = COLUMNS
  else
  end

  case vector
  when .left?, .diagonal_nl?,
       .diagonal_pl?
    left = true
  else
  end

  case vector
  when .diagonal_nr?, .diagonal_pl?
    down = true
  else
  end

  {col, row, left, down}
end

def out_range_2d(pos)
  get_x(pos) < 0 || get_x(pos) >= COLUMNS ||
    get_y(pos) >= ROWS || get_y(pos) < 0
end

def is_diagonal(vector)
  !(vector.up? || vector.left? || vector.right?)
end

def to_negative(value, bool)
  return value * -1 if bool
  value
end

def value_matrix(value_matrix, matrix, symbol, free_symbol, pos, g_win = false)
  Direction.each do |vector|
    a = case_vector(vector)
    iter_col = a[0]
    iter_row = a[1]
    left_flag = a[2]
    down_flag = a[3]
    break_flag = false

    counter = 0

    (0...iter_row).each do |y|
      break if break_flag

      (0...iter_col).each do |x|
        next if x != y && is_diagonal(vector)

        xx = to_negative(x, left_flag)

        yy = to_negative(y, down_flag)

        new_pos = {get_x(pos) + xx, get_y(pos) + yy}

        if out_range_2d(new_pos)
          break_flag = true
          break
        end

        case matrix[to_1d(new_pos)]
        when symbol
          counter += 1
          return true if g_win && counter > 3
          next
        when free_symbol
          value_matrix[to_1d(new_pos)] += 1
        else
        end
        break_flag = true
        break
      end
    end
  end
end

def value_move(value_matrix)
  move = [] of Tuple(Int32, Int32, Int32) # {move | col, value, row}

  (0...COLUMNS).each do |x|
    (0...ROWS).each do |y|
      value = value_matrix[to_1d({x, y})]

      if value > 0
        move << {x, value, y}
        break
      end
    end
  end

  move.sort_by { |move_v| move_v[1] }.reverse
end

def connect_four(matrix, symbol_player, symbol_rival, free_place)
  positions_rival = get_positions(matrix, symbol_rival)
  positions_player = get_positions(matrix, symbol_player)
  value_matrix_r = [0] * (COLUMNS * ROWS)
  value_matrix_p = value_matrix_r.clone

  positions_rival.each do |pos_r|
    value_matrix(value_matrix_r, matrix, symbol_rival, free_place, pos_r)
  end

  positions_player.each do |pos_p|
    value_matrix(value_matrix_p, matrix, symbol_player, free_place, pos_p)
  end

  play = value_move(value_matrix_p)
  playp =
    select_wmoves(matrix, {symbol_player, symbol_rival, free_place}, play)
  block = value_move(value_matrix_r)
  blockp =
    select_wmoves(matrix, {symbol_rival, symbol_player, free_place}, block)
  move = play[0][0]
  block = block[0]

  p = prediction(matrix,
    symbol_player, symbol_rival, free_place, block, round: 2)

  if p[0] && p[1] & 1 == 1 || !p[0] && p[1] & 1 == 0
    move = block[0]
  end

  move = blockp[0][0] unless blockp.empty?
  move = playp[0][0] unless playp.empty?

  move
end

def select_wmoves(matrix, symbols, moves)
  moves.select do |mp|
    pp = prediction(matrix, symbols[0], symbols[1], symbols[2], mp)

    pp[0] && pp[1] & 1 == 1
  end
end

def prediction(matrix, s_player, s_rival, free_symbol, move, round = 1)
  m_p = matrix.clone
  block = move.clone

  (1...round*2).each do |iter|
    symbol = s_player
    symbol = s_rival if iter & 1 == 0

    m_p[to_1d({block[0], block[2]})] = symbol
    vm_p = [0] * (COLUMNS * ROWS)

    get_positions(m_p, symbol).each do |pos_r|
      if value_matrix(vm_p, m_p, symbol, free_symbol, pos_r, g_win: true)
        return {true, iter}
      end
    end

    block = value_move(vm_p)
    return {false, iter} if block.empty?
    block = block[0]
  end

  {false, 0}
end

def generate_matrix(game) # game from server
  matrix = [0] * (COLUMNS * ROWS)
  col = 0

  game.each do |columns|
    row = 0

    columns.each_char do |players|
      matrix[to_1d({col, row})] = players.to_i

      row += 1
    end
    col += 1
  end

  matrix
end

def show_m(matrix)
  (0...ROWS).reverse_each do |y|
    (0...COLUMNS).each do |x|
      print "#{matrix[to_1d({x, y})]} "
    end
    puts
  end
end

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

form = "token: #{args[0]}\n"
puts form

response = HTTP::Client.post(URL, body: form)
puts response.body

until "end".in?(response.body)
  argus = response.body.split
  index = argus.index("state:") || 2

  state = argus[index + 1..]
  game_state = generate_matrix(state)

  move = connect_four(game_state, 1, 2, 0)

  text = "move: #{move}"

  puts text

  response = HTTP::Client.post(URL, body: "#{form}#{text}")
  puts response.body
end

puts
show_m(game_state || [0, 0])

# $ ./richardalmanza.cr
# token: vtC2Pzbc0gK0DJAiiy2bdDYx
# state: 1 0 0 0 0 0 2 0 0
# move: 7
# move: 6
# state: 1 0 0 0 0 0 22 1 0
# move: 7
# move: 2
# state: 1 0 2 0 0 0 22 11 0
# move: 6
# move: 5
# state: 1 0 2 0 0 2 221 11 0
# move: 5
# move: 8
# state: 1 0 2 0 0 21 221 11 2
# move: 7
# move: 7
# state: 1 0 2 0 0 21 221 1112 2
# move: 4
# move: 7
# state: 1 0 2 0 1 21 221 11122 2
# move: 7
# move: 0
# state: 12 0 2 0 1 21 221 111221 2
# move: 6
# move: 6
# state: 12 0 2 0 1 21 22112 111221 2
# move: 6
# move: 0
# state: 122 0 2 0 1 21 221121 111221 2
# move: 1
# move: 7
# state: 122 1 2 0 1 21 221121 1112212 2
# move: 1
# move: 3
# state: 122 11 2 2 1 21 221121 1112212 2
# move: 4
# move: 0
# state: 1222 11 2 2 11 21 221121 1112212 2
# move: 0
# move: 4
# state: 12221 11 2 2 112 21 221121 1112212 2
# move: 3
# move: 2
# state: 12221 11 22 21 112 21 221121 1112212 2
# move: 1
# move: 1
# state: 12221 1112 22 21 112 21 221121 1112212 2
# move: 2
# move: 8
# state: 12221 1112 221 21 112 21 221121 1112212 22
# move: 8
# move: 5
# state: 12221 1112 221 21 112 212 221121 1112212 221
# move: 5
# move: 6
# state: 12221 1112 221 21 112 2121 2211212 1112212 221
# move: 4
# move: 1
# state: 12221 11122 221 21 1121 2121 2211212 1112212 221
# move: 4
# end: EzFL0cpp5G/DIvbXS+mLdzg5
# message: You win!
#
# 0 0 0 0 0 0 0 0 0
# 0 0 0 0 0 0 2 2 0
# 0 0 0 0 0 0 1 1 0
# 1 2 0 0 0 0 2 2 0
# 2 2 0 0 1 1 1 2 0
# 2 1 1 0 2 2 1 1 1
# 2 1 2 1 1 1 2 1 2
# 1 1 2 2 1 2 2 1 2
