;; $ clj-kondo --lint bridamo98.cljs
;; linting took 37ms, errors: 0, warnings: 0

(ns bridamo98-027
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn is-taken[k-row k-col q-row q-col]
  (if (or (= k-row q-row) (= k-col q-col)
  (= (Math/abs (- k-row q-row)) (Math/abs (- k-col q-col))))
    (str "Y")
    (str "N")))

(defn solve-all [size-input files]
  (loop [x 0 result '()]
    (if (< x size-input)
      (let [pos (str/split (core/read-line) #" ")
      k-col ((keyword (subs (pos 0) 0 1)) files)
      k-row (edn/read-string (subs (pos 0) 1 2))
      q-col ((keyword (subs (pos 1) 0 1)) files)
      q-row (edn/read-string (subs (pos 1) 1 2))]
        (recur (+ x 1) (concat result (is-taken k-row k-col q-row q-col))))
      result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))
  files (hash-map :a 1, :b 2, :c 3, :d 4, :e 5, :f 6, :g 7, :h 8)]
    (doseq [item (solve-all size-input files)]
      (print item ""))
    (println)))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; Y N Y N N N N N Y N N Y Y Y N N N N N N N
