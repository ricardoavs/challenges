! fortran-linter --syntax-only --linelength = 80 --verbose alb4tr02.f90 #linting
! Checking alb4tr02.f90
! gfortran -Wall -pedantic-errors -Werror alb4tr02.f90 -o alb4tr02 #compilation

PROGRAM prime_numbers

  IMPLICIT NONE
  integer, DIMENSION(200000) :: arr
  integer :: n, k, cnt, np, idx, asc, stat
  logical :: flag, aux, IS_PRIME, f
  character(1) c

  n = 200000
  k = 2
  cnt = 0
  flag = .TRUE.
  DO WHILE(flag)
    aux = IS_PRIME(k)
    IF (aux) then
      arr(cnt) = k
      cnt = cnt + 1
    END IF
    k = k + 1
    IF (cnt >= n) flag = .FALSE.
  END DO
  f = .TRUE.
  np = 0
  DO WHILE(f)
    CALL fget(c, stat)
    IF (stat /= 0) exit
    asc = IACHAR(c)
    IF (asc == 10) then
      f = .FALSE.
    ELSE
      np = np * 10
      np = np + asc - 48
    END IF
  END DO
  DO WHILE(np > 0)
    idx = 0
    f = .TRUE.
    DO WHILE(f)
      CALL fget(c, stat)
      IF (stat /= 0) exit
      asc = IACHAR(c)
      IF (asc == 10 .OR. asc == 32) then
        f = .FALSE.
      ELSE
        idx = idx * 10
        idx = idx + asc - 48
      END IF
    END DO
    write (*, fmt="(1x,a,i0)", advance='no') '', arr(idx - 1)
    np = np - 1
  END DO
  print *, ''
END PROGRAM prime_numbers

logical FUNCTION IS_PRIME(n)

  integer :: n, aux, i
  logical flag

  flag = .TRUE.
  aux = int(SQRT(real(n)))
  DO i = 2, aux, 1
    IF ((MOD(n, i) == 0).AND.(n /= i)) then
      flag = .FALSE.
      exit
    END IF
  END DO
  IS_PRIME = flag
END FUNCTION IS_PRIME

! cat DATA.lst | ./alb4tr02
! 2529089 2018167 2543621 1800619 1955113 2491117 2684933 1422089 2464349 \
! 1370053 2224753 1761883
