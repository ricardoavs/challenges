#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 1.63 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

SUITS = ["Clubs", "Spades", "Diamonds", "Hearts"]
RANKS = ["2", "3", "4", "5", "6", "7", "8", "9", "10",
                          "Jack", "Queen", "King", "Ace"]

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map {|x| x.to_i}

args.each do |card_value|
  print "#{RANKS[card_value % 13]}-of-#{SUITS[card_value // 13]} "
end

puts

# $ ./richardalmanza.cr
# King-of-Hearts King-of-Clubs 4-of-Spades 4-of-Clubs Queen-of-Clubs
# Jack-of-Diamonds Ace-of-Clubs 9-of-Diamonds 7-of-Clubs Queen-of-Hearts
# Queen-of-Diamonds 7-of-Hearts 7-of-Diamonds Queen-of-Spades 6-of-Hearts
# 3-of-Hearts 9-of-Clubs 10-of-Diamonds 6-of-Diamonds 2-of-Clubs King-of-Spades
# 2-of-Hearts Ace-of-Diamonds Jack-of-Hearts 10-of-Clubs 5-of-Diamonds
