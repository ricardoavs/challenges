;$ clj-kondo --lint frank1030.cljs
;linting took 29ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.pprint :refer [cl-format]]))

(defn abs [n]
  (max n (- n)))

(defn herons-form [index]
  (let [x (atom index) x1 (core/read) y1 (core/read)
    x2 (core/read) y2 (core/read) x3 (core/read) y3 (core/read)
    herons (- (* (- x1 x3) (- y2 y1)) (* (- x1 x2) (- y3 y1)))
    area (/ (abs herons) 2)]
    (if (> @x 0)
      (do (swap! x dec)
        (print (cl-format nil "~$ " area))
        (if (not= @x 0)
          (herons-form @x)
          (print "")))
      (print ""))))

(defn main []
  (let [index (core/read)]
    (herons-form index)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;2058825.00 408448.50 1962020.00 8166048.00 4313036.00
;11631530.50 11072049.00 4688396.00 3598616.50 11097630.00
;4977228.50 2206271.50 7792157.50 9652407.00
