#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 57.36 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Float64)
  data.each_line do |x|
    inter = [] of Float64
    (x.split).each do |y|
      y = y.is_a?(String) ? y.try &.to_f : y
      inter << y
    end
    values << inter
  end
  values[1..]
end

def solution(array)
  x1 = array[0]
  y1 = array[1]

  x2 = array[2]
  y2 = array[3]

  x3 = array[4]
  y3 = array[5]

  a = (((x2-x1)**2)+((y2-y1)**2))**0.5
  b = (((x2-x3)**2)+((y2-y3)**2))**0.5
  c = (((x1-x3)**2)+((y1-y3)**2))**0.5

  s = (a+b+c)/2

  area = ((s*(s-a)*(s-b)*(s-c))**0.5).round(7)

  x = area.is_a?(Float64) ? area.try &.to_i : area
  y = area.divmod(x)
  if y[1] == 0
    print "#{x} "
  else
    print "#{area} "
  end
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# 2058825 408448.5 1962020 8166048 4313036 11631530.5 11072049
# 4688396 3598616.5 11097630 4977228.5 2206271.5 7792157.5 9652407
