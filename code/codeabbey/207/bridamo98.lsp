#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defstruct
  (suffix (:conc-name dr-))
  text
  index
)

(defun order-condition(a b)
  (string< (dr-text a) (dr-text b))
)

(defun order-suffix(array-suffix)
  (sort array-suffix 'order-condition)
)

(defun print-solution(size-input array-suffix)
  (loop for x from 0 to (- size-input 1)
    do(format t "~a " (dr-index (aref array-suffix x)))
  )
)

(defun fill-array(array-suffix text)
  (loop for x from 0 to (- (length text) 1)
    do(setf (aref array-suffix x)
      (make-suffix
        :text (substring text x (length text))
        :index x
      )
    )
  )
)

(defvar text (read-line))
(defvar array-suffix (make-array (length text)))

(fill-array array-suffix text)
(order-suffix array-suffix)
(print-solution (length text) array-suffix)

#|
  cat DATA.lst | clisp bridamo98.lsp
  174 115 97 149 22 5 64 143 164 83 47 200 45 188 105 221
  114 4 113 3 112 92 155 184 102 38 93 29 196 156 168 159
  107 73 76 33 52 36 214 141 1 21 82 183 195 58 138 67 151
  14 192 99 95 181 26 60 161 109 71 125 87 175 116 111 51
  98 59 207 185 120 208 166 24 103 7 139 16 39 163 199 187
  75 66 150 94 70 86 23 6 186 65 30 31 43 68 90 56 197 78
  144 127 152 205 11 32 213 0 13 191 15 55 130 157 202 121
  209 218 131 193 169 147 100 179 96 63 44 167 158 20 182 137
  160 108 50 165 74 69 89 77 204 217 19 203 122 27 34 84 9 48
  123 53 61 91 28 35 57 25 119 162 198 85 42 10 201 146 216
  134 210 219 177 135 132 79 46 104 37 81 194 110 129 49 18
  8 118 41 145 215 128 170 153 211 189 171 173 148 142 220 2
  154 101 106 72 140 180 124 206 126 212 12 190 54 178 62 136
  88 133 176 80 17 117 40 172
|#
