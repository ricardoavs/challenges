% erlc -W john2104.erl
% erl -compile john2104.erl
% erlint:lint("john2104.erl").
% {ok,[]}

-module(john2104).
-export([start/0]).
-export([readfile/1]).
-export([domath/4]).
-export([sendmath/2]).
-export([loop/1]).
-export([digest/1]).
-import(lists,[nth/2]).
-export([returnnstring/1]).


loop(A) ->
  lists:foreach(fun(X) -> digest(X) end, A).


digest(A) ->
  Z = returnnstring(A),
  L = length(Z),
  sendmath(Z, L).


sendmath(_, L) when L == 1 ->
  ok;


sendmath(A, L) when L > 1 ->
  domath(A, L, 1, 0).


domath(A, Z, R, K) when R =< Z ->
  W = (((K + nth(R, A)))*113) rem  10000007,
  if R == Z -> io:fwrite("~p ", [W]);
  true -> domath(A, Z, R+1, W)
  end.


returnnstring(T) ->
  [ element(1, string:to_integer(Substr)) ||
  Substr <- string:tokens(T, " ")].


readfile(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  string:tokens(erlang:binary_to_list(Binary), "\r\n").


start() ->
  Arr = readfile("DATA.lst"),
  loop(Arr),
  io:fwrite("~n").


% erl -noshell -s john2104 start -s init stop
% 8921379
