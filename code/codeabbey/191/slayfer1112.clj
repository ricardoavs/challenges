;; $ clj-kondo --lint slayfer1112.clj
;; linting took 16ms, errors: 0, warnings: 0

(ns slayfer1112-038
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data [file]
  (let [dat (slurp file)
        datv (str/split-lines dat)
        head (str/split (datv 0) #" ")
        body (subvec datv 1 (alength (to-array-2d datv)))]
    [head
     body]))

(defn count-times [array eval counter index]
  (let [a-length (alength (to-array-2d array))]
    (if (< index a-length)
      (if (= eval (array index))
        (count-times array eval (inc counter) (inc index))
        (count-times array eval counter (inc index)))
      counter)))

(defn min-val [value val]
  (if-not
   (= -1 (.lastIndexOf value (str/upper-case (Integer/toString val 16))))
    [(str/upper-case (Integer/toString val 16))
     (.lastIndexOf value (str/upper-case (Integer/toString val 16)))]
    (min-val value (inc val))))

(defn max-val [value val]
  (if-not
   (= -1 (.lastIndexOf value (str/upper-case (Integer/toString val 16))))
    [(str/upper-case (Integer/toString val 16))
     (.lastIndexOf value (str/upper-case (Integer/toString val 16)))]
    (max-val value (dec val))))

(defn min-num [value min-n m-index index zero? z-index]
  (if (< index m-index)
    (if (and zero? (> index 0))
      (if
       (<= (. Integer parseInt (value index) 16) (. Integer parseInt "0" 16))
        (min-num value "0" z-index (inc index) zero? z-index)
        (let [swap1 (assoc value index "0")]
          (assoc swap1 z-index (value index))))
      (if
       (<= (. Integer parseInt (value index) 16) (. Integer parseInt min-n 16))
        (min-num value min-n m-index (inc index) zero? z-index)
        (let [swap1 (assoc value index min-n)]
          (assoc swap1 m-index (value index)))))
    value))

(defn max-num [value max-n mx-index index]
  (if (< index mx-index)
    (if
     (>= (. Integer parseInt (value index) 16) (. Integer parseInt max-n 16))
      (max-num value max-n mx-index (inc index))
      (let [swap1 (assoc value index max-n)]
        (assoc swap1 mx-index (value index))))
    value))

(defn solution [_ body]
  (doseq [x body]
    (let [vals (str/split x #"")
          [min-n m-index] (min-val vals 1)
          [mx-n mx-index] (max-val vals 15)
          [max-n max-index] (if (= 0 mx-index)
                              (max-val vals (- (. Integer parseInt mx-n 16) 1))
                              (max-val vals 15))
          [_ z-index] (min-val vals 0)
          zero? (= "0" ((min-val vals 0) 0))]
      (print (str/join "" (min-num vals min-n m-index 0 zero? z-index)) "")
      (print (str (str/join "" (max-num vals max-n max-index 0)) " ")))))

(defn main []
  (let [[head body] (get-data "DATA.lst")]
    (solution head body)))

(main)

;; 2C3C4C73B53EC EC3C4273B53CC 11CDC9472CB4296D92ADEEF F1CDC9412CB4296D92ADEE7
;; 2BC5A8E0ABE75388F4D3 FBC5A8E0ABE7538834D2 1066A4593BCF81900ABB924CB9CF
;; F066A4593BCF81900ABB921CB9C4 2DAE409862CEEBA04B0E EEAE409862C2EBA04B0D
;; 189D44704DFC8D586DF9 F89D44704DF18D586DC9 185C89898F60845DB6195323
;; F85C89898260845DB6195313 1A6930BC584FAE804999E6 FA6930BC584A1E804999E6
;; 18DABFD497B3ABF758 F8DABFD497B3AB5718 19AA9F32829D61B1DAE245C708
;; F9AA9232829D61B1DAE145C708 1AF3B8D58D945BD32FE3 FAF3B8D51D945BD328E3
;; 19EC7B474BEEA2920F4D8833666 F1EC7B474BEEA292094D8833666 193DA2A5EE92A804AA6A
;; E93DA2A1E592A804AA6A 2823C6A2C0764507FB5EC784 F823C6A2C0764507B25EC784
;; 1F33ADE202E7C13D99DEA8 FD33ADE202E7C13D991EA8 2EC965EE2D258F4B06973BB044D3
;; FEC965EE2D258D4B06973BB04423 11DAD7035F531AB1F6B6099C4A09C4
;; F1DAD7035F531AB1B616099C4A09C4 25623D2632B73685597BA05
;; D562382632B73625597BA05 1F7ECB745E74CAAFC660 FF7ECB741E74CAA5C660
;; 1522DEDD3422B1E79387AD34739 E522DEDD3422B1979387AD34731
;; 19A27C54A32E75036FA0337E0B38 F1A27C54A32E750369A0337E0B38
