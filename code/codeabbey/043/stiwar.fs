open System

[<EntryPoint>]
let main argv =
    let operations (n:float) =
        let r = (int (n*6.0)) + 1
        r
    printfn "ingrese la cantidad de pruebas:"
    let c = stdin.ReadLine() |> int
    let mutable out = Array.zeroCreate c
    for i in 0..(c-1) do
        printfn "(%i) ingrese el decimal" (i+1)
        let dec = stdin.ReadLine() |> float //cast entrada usuario a float
        out.[i] <- (operations dec)
    for i in 0..(c-1) do
        printf "%i " out.[i]
    Console.ReadKey() |> ignore
    0 // devolver un codigo de salida entero
