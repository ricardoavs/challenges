/*
# Lint
$ ktlint --experimental --verbose --disabled_rules=experimental:indent,indent \
> miyyer1946.ky
# Compile
$ kotlinc -Werror miyyer1946.kt -include-runtime -d miyyer1946.jar
*/

import kotlin.math.PI
import kotlin.math.tan

fun main(args: Array<String>) {
  var result: String = ""
  var data: Long
  var angle = 180
  var cont = 2
  var input = readLine()!!.split(" ")
  while (cont <= input.size - 1) {
    var tanA = tan((input[cont].toDouble() * PI) / angle)
    cont += 1
    var tanB = tan((input[cont].toDouble() * PI) / angle)
    data = Math.round((tanA * input[cont - 2].toDouble()) / (1 - tanA / tanB))
    cont += 2
    result += data.toString() + " "
    }
    println(result)
}

/*
$ cat DATA.lst | java -jar miyyer1946.jar
1041 1534 1081 928 1622 860 1953 1041 1568 645 772 762 1045 1239 1425
*/
