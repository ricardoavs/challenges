;; $ clj-kondo --lint bridamo98.clj
;; linting took 24ms, errors: 0, warnings: 0

(ns bridamo98-003
  (:gen-class)
  (:require [clojure.core :as core]))

(defn sum-values [a b]
  (print (str (+ a b) " "))
)

(defn sums-in-loop [size-input]
  (loop [x 1]
    (when (< x (+ size-input 1))
      (sum-values (core/read) (core/read))
      (recur (+ x 1))
    )
  )
  (println)
)

(defn main []
  (let [size-input (core/read)]
    (sums-in-loop size-input)
  )
)

(main)

;; $ cat DATA.lst | clj bridamo98.clj
;; 77386 440366 1038645 785728 809727 792139
;; 1001087 708771 676606 1286978 1423888
