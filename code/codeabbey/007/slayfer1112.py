# $ mypy --strict slayfer1112.py
# Success: no issues found in 1 source file

from typing import List

A: List[str] = input().split()
X: slice = slice(1, len(A))
B: int = 0
A = A[X]

for item in A:
    B = round((int(item) - 32) * 5 / 9)
    print(B, end=" ")

print()

# $ cat DATA.lst | python3 slayfer1112.py
# 177 236 32 238 162 51 96 97 49 24 21 27 167 292 160 202 162
# 138 221 289 182 206 178 277 204 31 302 313 223 81 99 84 2
