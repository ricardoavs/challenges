#|
$ sblint -v frank1030.lsp
[INFO] Lint file frank1030.lsp

$ clisp -c frank1030.lsp
Compiling file /Users/Frank/Desktop/frank/128/frank1030.lsp ...
Wrote file /Users/Frank/Desktop/frank/128/frank1030.fas
0 errors, 0 warnings
Bye.
|#

(defun get-index()
  (let ((dat))
    (setq dat(read))
  )
)

(defun fact (ni)
  (if (< ni 2)
    (setq ni 1)
    (setq ni (* ni (fact (- ni 1))))
  )
)

(defun combinations()
  (let ((index 0) (n 0) (k 0) (nk 0) (nf 0) (kf 0) (nkf 0) (c 0))
    (setq index(get-index))
    (loop for i from 0 to (- index 1)
      do (setq n(read))
      do (setq k(read))
      do (setq nk(- n k))
      do (setq nf(fact n))
      do (setq kf(fact k))
      do (setq nkf(fact nk))
      do (setq c(/ nf (* kf nkf)))
      do (print c)
    )
  )
)

(combinations)

#|
$ cat DATA.lst | clip frank1030.lsp
53060358690 11050084695
10639125640 28987537150
10235867928 35607051480
22760723700 65033528560
|#
