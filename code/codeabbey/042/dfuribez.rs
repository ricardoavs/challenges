/*
$ clippy-driver dfuribez.rs -D warnings
$ rustc dfuribez.rs
*/

use std::io::{self, BufRead};

fn count_ace(count: u8, aces: u8) {
  let mut count_vec = Vec::new();

  if aces == 1 {
    count_vec.push(count + 1);
    count_vec.push(count + 11);
  } else if aces == 2 {
    for c1 in [1, 11].iter() {
      for c2 in [1, 11].iter() {
        count_vec.push(count + c1 + c2);
      }
    }
  } else {
    count_vec.push(count + aces);
    count_vec.push(count + (aces - 1) + 11);
  }
  let mut max = 0;
  for value in count_vec {
    if value <= 21 && value > max {
      max = value;
    }
  }

  if max == 0 {
    print!("Bust ");
  } else {
    print!("{} ", max);
  }
}

fn count_cards(cards: Vec<&str>) {
  let non_numeric = vec!["T", "J", "Q", "K", "A"];
  let mut count: u8 = 0;
  let mut aces: u8 = 0;

  for card in cards {
    if non_numeric.contains(&card) {
      if card == "A" {
        aces += 1;
      } else {
        count += 10;
      }
    } else {
      let value: u8 = card.parse().unwrap_or(0);
      count += value;
    }
  }
  if aces > 0 {
    count_ace(count, aces);
  } else if count <= 21 {
    print!("{} ", count);
  } else {
    print!("Bust ");
  }
}

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();

  for (index, line) in lines.enumerate() {
    let l = line.unwrap();
    if index > 0 {
      let params: Vec<&str> = l.split_whitespace().collect();
      count_cards(params);
    }
  }
  println!();
}

/*
$ cat DATA.lst | ./dfuribez
21 19 16 18 18 21 18 17 16 Bust Bust 20 20 21 19 20 17 19 19 19 18 21 Bust 21 19
*/
