#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun add-last(value)
  (setq queue (append queue value))
)

(defun get-first()
  (setq value (car queue))
  (setq queue (cdr queue))
  (return-from get-first value)
)

(defun calc-number(row col tot-cols)
  (+ (* row tot-cols) col 1)
)

(defun calc-row(num tot-cols)
  (floor (- num 1) tot-cols)
)

(defun calc-col(num row tot-cols)
  (- num (* row tot-cols) 1)
)

(defun load-row (i board)
  (let ((line (read-line)))
    (loop for j from 1 to (length line)
      do(setf (aref board i j) (subseq line (- j 1) j))
    )
  )
)

(defun load-board(board rows)
  (loop for i from 1 to (- rows 2)
    do(load-row i board)
  )
)

(defun find-neighbors(current row col cols)
  (if (aref board (- row 1) col)
    (if (and (string= (aref board (- row 1) col) "1")
    (not (gethash (- current cols) seen)))
      (progn
        (setf (gethash (- current cols) seen) "U")
        (add-last (list (- current cols)))
      )
    )
  )
  (if (aref board row (- col 1))
    (if (and (string= (aref board row (- col 1)) "1")
    (not (gethash (- current 1) seen)))
      (progn
        (setf (gethash (- current 1) seen) "L")
        (add-last (list (- current 1)))
      )
    )
  )
  (if (aref board row (+ col 1))
    (if (and (string= (aref board row (+ col 1)) "1")
    (not (gethash (+ current 1) seen)))
      (progn
        (setf (gethash (+ current 1) seen) "R")
        (add-last (list (+ current 1)))
      )
    )
  )
  (if (aref board (+ row 1) col)
    (if (and (string= (aref board (+ row 1) col) "1")
    (not (gethash (+ current cols) seen)))
      (progn
        (setf (gethash (+ current cols) seen) "D")
        (add-last (list (+ current cols)))
      )
    )
  )
)

(defun find-path(board cols)
  (let ((row NIL) (col NIL) (flag 0))
    (loop
      (setq current (get-first))
      (setq row (calc-row current cols))
      (setq col (calc-col current row cols))
      (if (and (= row 1) (= col 1))
        (setq flag 1)
        (find-neighbors current row col cols)
      )
      (when (or (= flag 1) (not queue)) (return NIL))
    )
  )
)

(defun print-solution(cols seen)
  (let ((i-number (calc-number 1 1 cols))
  (i-action "") (cont 1) (lst "") (result ""))
    (loop
      (setq i-action (gethash i-number seen))
      (if (string= lst i-action)
        (setq cont (+ cont 1))
        (progn
          (if (not (= cont 1))
            (setq result (concatenate 'string (write-to-string cont)
            lst result))
            (setq result (concatenate 'string lst result))
          )
          (setq cont 1)
        )
      )
      (if (string= i-action "U")
        (setq i-number (+ i-number cols))
      )
      (if (string= i-action "L")
        (setq i-number (+ i-number 1))
      )
      (if (string= i-action "R")
        (setq i-number (- i-number 1))
      )
      (if (string= i-action "D")
        (setq i-number (- i-number cols))
      )
      (setq lst i-action)
      (when (string= i-action "initial")(return-from print-solution result))
    )
  )
)

(defun find-all-paths()
  (setq cell (calc-number 1 (- cols 2) cols))
  (setf (gethash cell seen) "initial")
  (add-last (list cell))
  (find-path board cols)
  (format t "~a "(print-solution cols seen))
  (setq seen (make-hash-table :test 'equal))
  (setq queue '())
  (setq cell (calc-number (- rows 2) 1 cols))
  (setf (gethash cell seen) "initial")
  (add-last (list cell))
  (find-path board cols)
  (format t "~a "(print-solution cols seen))
  (setq seen (make-hash-table :test 'equal))
  (setq queue '())
  (setq cell (calc-number (- rows 2) (- cols 2) cols))
  (setf (gethash cell seen) "initial")
  (add-last (list cell))
  (find-path board cols)
  (format t "~a "(print-solution cols seen))
)

(defvar cols (+ (read) 2))
(defvar rows (+ (read) 2))
(defparameter dimension-list (list rows cols))
(defparameter board (make-array dimension-list))
(defvar queue '())
(defparameter seen (make-hash-table :test 'equal))
(defvar cell NIL)
(defvar current NIL)

(load-board board rows)
(find-all-paths)

#|
  cat DATA.lst | clisp bridamo98.lsp
  2L6D6L2U2L4D2R2D4L4D2L4D2R2D10L4U8L2U2L8U2R2U4L4U 2U4R4U2L8U2R2U4L4U
  2L2U2R4U4L2U2R2U4L2D2L2U4L4D2L4D2R2D10L4U8L2U2L8U2R2U4L4U
|#
