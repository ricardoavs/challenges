;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        vals (str/split (data 0) #" ")]
    [vals]))

(defn solution [data]
  (let [maximum (reduce max (map read-string data))
        minimum (reduce min (map read-string data))]
    (print (str maximum " " minimum " "))))

(defn main []
  (let [[data] (get-data)]
    (solution data)
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 79968 -79467
