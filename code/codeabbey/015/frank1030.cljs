;$ clj-kondo --lint frank1030.cljs
;linting took 288ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn main []
  (let [index (core/read-line) numbers (str/split index #" ")]
    (print (reduce max (map edn/read-string numbers)))
    (print "" (reduce min (map edn/read-string numbers)))))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;79767 -79840
