;; clj-kondo --lint ludsrill.cljs
;; linting took 29ms, errors: 0, warnings: 0

(ns ludsrill.185
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core])
  (:require [clojure.edn :as edn]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
          (cons (str/split input-string #" ") (get-data))
          nil))))

(defn body-mass-index [data]
  (let [weight (edn/read-string (first data))
        height (edn/read-string (second data))
        bmi (/ weight (* height height))]

    (cond (< bmi 18.5) (print "under ")
          (and (< bmi 25.0) (>= bmi 18.5)) (print "normal ")
          (and (< bmi 30.0) (>= bmi 25.0)) (print "over ")
          (>= bmi 30.0) (print "obese "))))

(defn -main []
  (let [data (rest (get-data))]
    (doseq [item data]
      (body-mass-index item))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; normal normal under obese obese obese normal obese over under normal normal
;; obese normal under over under under under over under under obese obese obese
