;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (subvec data 1 (alength (to-array-2d data)))]
    [head
     body]))

(def starvation-order (atom []))
(def index (atom 0))

(defn visitor-pos [visitors i len]
  (if (< i len)
    (do (if (and (not= nil (visitors i)) (= nil (visitors @index)))
          (reset! index i)
          ())
        (if (and (not= nil (visitors i)) (not= nil (visitors @index)))
          (if (> (visitors i) (visitors @index))
            (reset! index i)
            ())
          ())
        (visitor-pos visitors (inc i) len))
    @index))

(defn tail-discomfort [visitors x0 n]
  (loop [vis visitors
         x x0
         t-n 0
         dis 0]
    (if (> vis 0)
      (if (and (= (mod t-n 2) 0) (> t-n 0))
        ;; if true
        (let [arr @starvation-order
              len (count arr)
              vis-pos (visitor-pos arr 0 len)
              d (+ dis (* (arr vis-pos) (- t-n vis-pos)))]
          (reset! index 0)
          (swap! starvation-order assoc vis-pos nil)
          (if (< t-n n)
            ;; if true
            (let [linear (mod (+ (* 445 x) 700001) 2097152)
                  starvation (inc (mod linear 999))]
              (swap! starvation-order conj starvation)
              (recur (dec vis) linear (inc t-n) d))
            ;; if false
            (recur (dec vis) x (inc t-n) d)))
        ;; if false
        (if (< t-n n)
          ;; if true
          (let [linear (mod (+ (* 445 x) 700001) 2097152)
                starvation (inc (mod linear 999))]
            (swap! starvation-order conj starvation)
            (recur vis linear (inc t-n) dis))
          ;; if false
          (recur vis x (inc t-n) dis)))
      dis)))

(defn total-discomfort [visitors seed]
  (let [x0 seed
        n visitors
        discomfort (tail-discomfort visitors x0 n)]
    discomfort))

(defn solution [data]
  (let [visitors (read-string (data 0))
        seed (read-string (data 1))
        dis (total-discomfort visitors seed)]
    (print dis)))

(defn main []
  (let [[head _] (get-data)]
    (solution head)
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 11854336550
