--[[
$ luacheck mrsossa.lua
Checking mrsossa.lua                              OK
Total: 0 warnings / 0 errors in 1 file
]]

local data = io.lines("DATA.lst")
local a = 0
local i = 1
for line in data do
    local N1 = 0
    local N2 = 0
    local N3 = 0
    if a == 0 then
        a = tonumber(line)
    else
        local c = 1
        for token in string.gmatch(line, "[^%s]+") do
            if c == 1 then
                N1 = tonumber(token)
                c = 2
            elseif c == 2 then
                N2 = tonumber(token)
                c = 3
            elseif c == 3 then
                N3 = tonumber(token)
                c = 1
            end
        end
        if (N1 > N2 and N1 < N3) or (N1 < N2 and N1 > N3) then
            print(N1)
        elseif (N2 > N1 and N2 < N3) or (N2 < N1 and N2 > N3)then
            print(N2)
        elseif (N3 > N1 and N3 < N2) or (N3 < N1 and N3 > N2)then
            print(N3)
        end
    end
    i = i + 1
end

--[[
$ lua mrsossa.lua
4
1087
22
57
503
80
161
811
620
112
952
52
15
847
13
90
38
36
308
14
971
117
250
284
11
929
151
358
]]
