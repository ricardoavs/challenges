#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 4.01 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Int32)
  data.each_line do |x|
    inter = [] of Int32
    (x.split).each do |y|
      y = y.is_a?(String) ? y.try &.to_i : y
      inter << y
    end
    values << inter
  end
  values[1..]
end

def solution(array)
  a = array[0]
  b = array[1]
  c = array[2]

  triangle = 0

  if (a+b)>c && (a+c)>b && (b+c)>a
    triangle = 1
  end

  print "#{triangle} "
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# 0 1 0 0 0 1 0 1 1 0 1 0 0 0 1 0 0 1 0 1 0 0 1 0 0 1 0 1 1 1
