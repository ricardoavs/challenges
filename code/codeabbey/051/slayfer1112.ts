/*
$ eslint slayfer1112.ts
$ tsc slayfer1112.ts
*/

/*
Premise:

Due to probabilistic nature of the problem you may fail at first attempt,
however try a few times - and if your algorithm is correct, your answer
will be accepted.

Attemps until correct:

2
*/
function solution(entry: string): number {
  const diceA = 1.5;
  const diceB = 2.5;
  const diceC = 3.5;
  const diceD = 4.5;
  const diceE = 5.5;
  const diceF = 6.5;
  const elemA: number[] = [diceA, diceB].concat([diceC, diceD]);
  const dices: number[] = elemA.concat([diceE, diceF]);
  const values: number[] = entry.split(' ').map((value) => Number(value));
  const dat: number[] = values.slice(0, values.length - 1);
  const minimum: number = Math.min(...dat);
  const maximum: number = Math.max(...dat);
  const pro: number = dat.reduce((acc, cur) => acc + cur) / dat.length;
  const prob: number[] = dices.map((value) =>
    Math.round(Math.abs(value * minimum - pro))
  );
  const faces: number = (prob.indexOf(Math.min(...prob)) + 1) * 2;
  if (minimum * faces <= maximum) {
    const sol = `${minimum}d${faces}`;
    process.stdout.write(`${sol} `);
  } else {
    const sol = `${minimum}d${faces - 2}`;
    process.stdout.write(`${sol} `);
  }
  return 0;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const dat: string[] = entryArray.slice(0, entryArray.length);
    dat.map((value) => solution(value));
    process.stdout.write('\n');
    return 0;
  });
  return 0;
}

main();
/*
$ cat DATA.lst | node slayfer1112.js
1d12 2d2 1d6
*/
