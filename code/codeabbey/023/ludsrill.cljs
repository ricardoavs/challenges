;; clj-kondo --lint ludsrill.cljs
;; linting took 33ms, errors: 0, warnings: 0

(ns ludsrill.023
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn swap [data x]
  (if (= (count data) 2)
    [(first data) (list x)]
    (if (< (Integer. (str (first data))) (Integer. (str (second data))))
      (cons (first data) (swap (rest data) x))
      (cons (second data) (swap (cons (first data)
        (rest (rest data))) (inc x))))))

(defn checksum [swaper x]
  (if (not= (count swaper) 1)
    (checksum (rest swaper)
              (rem (* (+ x (Integer. (str (first swaper)))) 113) 10000007))
    [x]))

(defn -main []
  (let [data (get-data)
        swaper (swap (first data) 0)
        result (checksum swaper 0)]
    (println (core/format "%s %s" (first (last swaper)) (first result)))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 33 2036273
