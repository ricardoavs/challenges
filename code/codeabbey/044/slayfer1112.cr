#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 4.44 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Float64)
  data.each_line do |x|
    inter = [] of Float64
    (x.split).each do |y|
      y = y.is_a?(String) ? y.try &.to_f : y
      inter << y
    end
    values << inter
  end
  values[1..]
end

def solution(array)
  val1 = array[0]
  val2 = array[1]

  dice1 = Int32.new(val1.divmod(6)[1]) + 1
  dice2 = Int32.new(val2.divmod(6)[1]) + 1

  dicesum = dice1 + dice2

  print "#{dicesum} "
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# 12 6 11 10 5 8 4 9 10 8 4 6 3 5 10 10 10 7 6 7 5 9 10 2 6 4 5 7 9
