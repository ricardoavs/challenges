let moonrad= 1737.1*.1000.0;;
let surfg= 1.622;;
let vex = 2800.0;;
let mcraft= ref 6400.0;;
let mfuel= ref 6900.0;;
let height = ref 180000.0;;
let vdesc= ref 1550.0;;
let dt= 0.1;;
let brates= [15.0;5.0;5.0;0.0;15.0;20.0;0.0;10.0;70.0;70.0;80.0;80.0;80.0];;
let indbrate=ref  0;;
let brateact= ref (List.nth brates 0);;
let dv= ref 0.0;;
let mtotal= ref (!mcraft +. !mfuel);;
let t= ref 0.0;;
let gact= ref 0.0;;
let dm= ref 0.0;;
let i = ref 0;;
while !t< 1500.0 do
        
        if (!mfuel<=0.0 || !indbrate> List.length brates -1 )  then (brateact:= 0.0) else (brateact:=List.nth brates !indbrate); 
        i:= 0;
        while !i < 100 do
          height := !height -. !vdesc *. dt;
          dm := dt*. !brateact ;
            dv:= vex *. (!dm/. !mtotal);
            mfuel := !mfuel -. !dm; 
            mtotal:= !mcraft +. !mfuel;
            gact := (surfg *. moonrad**2.0)/. ((moonrad+. !height)**2.0) ;
            vdesc:=!vdesc +. (!gact*.dt) -. !dv ;
            t:= !t +. 0.1 ;
            if !height <= 0.0 then (i:= 100) else (i:= !i +1 )
         done;
        indbrate:= !indbrate + 1;
        if !height <= 0.0 then (print_float !vdesc;)
done;

    
