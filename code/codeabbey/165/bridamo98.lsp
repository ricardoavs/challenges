#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun delimiterp (c)
  (char= c #\Space)
)

(defun split (string &key (delimiterp #'delimiterp))
  (loop
    :for beg = (position-if-not delimiterp string)
    :then (position-if-not delimiterp string :start (1+ end))
    :for end = (and beg (position-if delimiterp string :start beg))
    :when beg :collect (subseq string beg end)
    :while end
  )
)

(defun find-end-velocity()
  (setq total-mass (+ c-mass f-mass))
  (setq radius 1737100)
  (setq gravity 0.0)
  (setq h0-gravity 1.622)
  (setq v-exhaust 2800)
  (setq delta-t 0.1)
  (setq delta-v 0.0)
  (setq delta-m 0.0)
  (setq i (- 0 1))
  (setq current-rate 0)
  (setq time 0.0)
  (setq flag 1)
  (setq cont 0)
  (loop
    (if (= (mod time 10.0) 0.0)
      (setq i (+ i 1))
    )
    (if (or (> i (- (length rates) 1)) (or (< f-mass 0.0)(= f-mass 0.0)))
      (setq current-rate 0.0)
      (setq current-rate (parse-integer (nth i rates)))
    )
    (setq cont (+ cont 1))
    (setq time (float (/ cont 10)))
    (setq height (- height (* velocity delta-t)))
    (setq delta-m (* delta-t current-rate))
    (setq delta-v (* v-exhaust (/ delta-m total-mass)))
    (setq f-mass (- f-mass delta-m))
    (setq total-mass (+ f-mass c-mass))
    (setq gravity (/ (* h0-gravity (expt radius 2))(expt (+ radius height) 2)))
    (setq velocity (+ velocity (- (* gravity delta-t) delta-v)))
    (if (< height 0.0)
      (progn
        (format t "~a" (round velocity))
        (setq flag 0)
      )
    )
    (when (= flag 0)(return NIL))
  )
)

(defvar c-mass (read))
(defvar f-mass (read))
(defvar height (read))
(defvar velocity (read))
(defparameter rates (split (read-line)))
(find-end-velocity)

#|
  cat DATA.lst | clisp bridamo98.lsp
  273
|#
