#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data ()
  (return-from read-data (read-line)))

(defun join-string (join)
  (return-from join-string (format NIL "~{~a~}" join)))

(defun base32-to-binary (value)
  (let ((base-32))
    (setq base-32 '(("00000" "0") ("00001" "1") ("00010" "2") ("00011" "3")
      ("00100" "4") ("00101" "5") ("00110" "6") ("00111" "7") ("01000" "8")
      ("01001" "9") ("01010" "A") ("01011" "B") ("01100" "C") ("01101" "D")
      ("01110" "E") ("01111" "F") ("10000" "G") ("10001" "H") ("10010" "I")
      ("10011" "J") ("10100" "K") ("10101" "L") ("10110" "M") ("10111" "N")
      ("11000" "O") ("11001" "P") ("11010" "Q") ("11011" "R") ("11100" "S")
      ("11101" "T") ("11110" "U") ("11111" "V")))

    (return-from base-32-to-binary (remove NIL (map 'list (lambda (x)
      (when (equal (second x) value)
        (first (elt base-32 (position x base-32))))) base-32)))))

(defun decoding-table (value)
  (let ((table))
    (setq table '((" " "11") ("e"  "101") ("t" "1001") ("o" "10001")
      ("n" "10000") ("a" "011") ("s" "0101") ("i" "01001") ("r" "01000")
      ("h" "0011") ("d" "00101") ("l" "001001") ("!" "001000") ("u" "00011")
      ("c" "000101") ("f" "000100") ("m" "000011")  ("p" "0000101")
      ("g" "0000100") ("w" "0000011") ("b" "0000010") ("y" "0000001")
      ("v" "00000001") ("j" "000000001") ("k" "0000000001") ("x" "00000000001")
      ("q" "000000000001") ("z" "000000000000")))

    (return-from decoding-table (remove NIL (map 'list (lambda (x)
      (when (equal (second x) value)
        (first (elt table (position x table)))))
          table)))))

(defun decode-to-binary (codified-message)
  (cond ((= (length codified-message) 0) NIL)
         (T (nconc (base32-to-binary (subseq codified-message 0 1))
              (decode-to-binary (subseq codified-message 1))))))

(defun decode-message (binary-message aux-binary-message)
  (cond ((= (length aux-binary-message) 0) NIL)
         (T (if (equal (decoding-table
                (subseq binary-message 0 (- (length binary-message)
                (length aux-binary-message)))) NIL)

              (decode-message binary-message (subseq aux-binary-message 1))

              (progn
                (nconc (decoding-table (subseq binary-message 0
                          (- (length binary-message)
                          (length aux-binary-message))))
                       (decode-message (subseq binary-message
                          (- (length binary-message)
                          (length aux-binary-message)))
                          (subseq aux-binary-message 1))))))))

(defun main ()
  (let ((binary-message))
    (setq binary-message (join-string (decode-to-binary (read-data))))
    (print (join-string (decode-message binary-message binary-message)))))

(main)

#|
cat DATA.lst | clisp ludsrill.lsp
"station to which the !laws of !nature and of !natures !god entitle them by
refusing his !assent to !laws for establishing !judiciary !powers !he has made
consanguinity !we must therefore acquiesce in the necessity which denounces
our"
|#
