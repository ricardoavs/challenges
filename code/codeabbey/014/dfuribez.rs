/*
$ clippy-driver dfuribez.rs -D warnings
$ rustc dfuribez.rs
*/

use std::io::{self, BufRead};

fn add(a: u32, b: u32) -> u32 {
  a + b
}

fn mul(a: u32, b: u32) -> u32 {
  a * b
}

fn md(a: u32, b: u32) -> u32 {
  a % b
}

fn calc(ops: Vec<String>, numbers: Vec<u32>, modulus: u32, mut buffer: u32) {
  let functions: Vec<&dyn Fn(u32, u32) -> u32> = vec![&add, &mul, &md];
  let operations: Vec<String> =
    vec!["+".to_string(), "*".to_string(), "%".to_string()];

  for (index, number) in numbers.iter().enumerate() {
    let operation_index = operations
      .iter()
      .position(|x| x == &ops[index])
      .unwrap_or(0);
    buffer = (functions[operation_index])(buffer, *number) % modulus;
  }

  println!("{}", buffer);
}

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();
  let mut ops: Vec<String> = Vec::new();
  let mut numbers: Vec<u32> = Vec::new();
  let mut modulus: u32 = 0;
  let mut buffer: u32 = 0;

  for (index, line) in lines.enumerate() {
    let l = line.unwrap();
    if index == 0 {
      buffer = l.parse().unwrap_or(0);
    } else {
      let parameters: Vec<&str> = l.split_whitespace().collect();
      let operation: String = parameters[0].to_string();
      let number: u32 = parameters[1].parse().unwrap_or(0);
      let operation = operation.clone();

      ops.push(operation);
      numbers.push(number);

      modulus = number;
    }
  }
  calc(ops, numbers, modulus, buffer);
}

/*
$ cat DATA.lst | ./dfuribez
2178
*/
