package cha0smagick;

use strict;
use warnings;

our $VERSION = 1.0;

sub sum {
  my ( $a, $b ) = @_;

  return $a + $b;
}

1;
