#! /usr/bin/crystal

# $ ameba --all --fail-level Convention slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 8.92 milliseconds
# $ crystal build --error-trace --error-on-warnings \
# --threads 1 --no-codegen slayfer1112.cr

_ = gets #cases
args = gets

def check_digits(value)
  if (value.size < 8)
    check_digits("0#{value}")
  else
    value
  end
end


def neumann(seed, arr, counter)
  seed_sqrt = seed * seed
  value = check_digits("#{seed_sqrt}")
  trunc = value[2..5].to_i
  if (arr.index(trunc) == nil)
    arr << trunc
    counter += 1
    neumann(trunc, arr, counter)
  else
    counter
  end
end

def solution(seed)
  seed = seed.is_a?(String) ? seed.try &.to_i : seed
  val = neumann(seed, [seed], 1)
  print "#{val} "
end

if args
  args = args.split
  args.each do |seed|
    solution(seed)
  end
  puts
end

# $ cat DATA.lst | crystal slayfer1112.cr
# 106 111 97 101 98 98 105 98 110 100 101
