#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect line)))

(defun split-by-one-space (item)
  (loop for i = 0 then (1+ j)
    as j = (position #\Space item :start i)
    collect (subseq item i j)
    while j))

(defun count-digits (aux-power)
  (if (< (length aux-power) 8)
    (progn
      (setq aux-power (concatenate 'string "0" aux-power))
      (count-digits aux-power))
    (return-from count-digits aux-power)))

(defun neumann-random-generator (item randoms)
  (let ((aux-power)
        (seq))
    (setq randoms (append randoms (list item)))
    (setq aux-power (write-to-string (expt (parse-integer item) 2)))
    (if (< (length aux-power) 8)
      (setq aux-power (count-digits aux-power)))

    (setq seq (subseq aux-power 2 6))

    (if (equal (find seq randoms :test #'equal) seq)
      (format t "~s " (length randoms))
      (neumann-random-generator seq randoms))))

(defun organize-data ()
  (let ((data)
        (iter)
        (randoms))
    (setq data (split-by-one-space (first (cdr (read-data)))))
    (setq iter 0)
    (dolist (item data)
      (setq randoms ())
      (neumann-random-generator item randoms))))

(organize-data)

#|
cat DATA.lst | clisp ludsrill.lsp
102 100 105 102 105 110 104 101 100 98 111 102 102 104
|#
