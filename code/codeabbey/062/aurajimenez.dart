// $ dartanalyzer aurajimenez.dart
// Analyzing aurajimenez.dart...
// No issues found!

import 'dart:io';

int quantities(int startN, int finalN){

  int quantity = 0;
  int step = 0;
  if (startN % 2 == 0) {
    step = 1;
  }
  else {
    step = 2;
  }
  for (int i = startN; i <= finalN; i = i + step){
    bool prime = true;
    for (int j = 2; j < i ; j++){
      if (i % j == 0) {
        prime = false;
      }
    }
    if (prime) {
      quantity = quantity + 1;
    }
  }
  return quantity;
}

void main() {

  var line1 = int.parse(stdin.readLineSync());
  var result = 0;
  for (int k = 1; k <= line1; k++){
    var var1 = stdin.readLineSync().split(" ");
    var startN = int.parse(var1[0]);
    var finalN = int.parse(var1[1]);
    result = quantities(startN, finalN);
    print (result);
  }
}

/* $ cat DATA.lst | dart aurajimenez.dart
14666 35668 12210 8331 4987 35092 10371 46198
28460 6092 19100 31738 6359 13173 9406 82052
13504 11894 2885 12246 14584 4702 21450 1635
62783 29676 49501
*/
