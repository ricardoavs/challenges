% $ erlc -Werror andresclm.erl
% andresclm.beam

-module(andresclm).

-export([start/0]).

-compile(andresclm).

start() ->
  Data = read_file("DATA.lst"),
  Matrix = [string:lexemes(Line," ") || Line<-Data],
  Result = [ get_result(PA,PB)|| [PA,PB] <- Matrix],
  [io:format("~B ",[Probability])|| Probability <- Result].

read_file(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  [_|Data] = string:lexemes(erlang:binary_to_list(Binary), "\n"),
  Data.

get_result(A,B) ->
  {PA, _} = string:to_integer(A),
  {PB, _} = string:to_integer(B),
  calculate_probability(PA/100,PB/100).

calculate_probability(PA,PB) ->
  erlang:round((PA / (PA + PB - (PA * PB)))*100).

% $ erl -noshell -s andresclm -s init stop
% 96 34 29 24 96 91 35 36 13 46 54 96 24 95 80 88 85 59 72 55 91
% 76 33 92 81 88 92 69
