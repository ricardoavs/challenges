/*
  $ rustfmt vmelendez.rs
  $ rustc vmelendez.rs
  $
*/

fn getbinary(value: u32) -> i32 {
  let mut ret = 0;

  let mut v = value;
  while v > 0 {
    if v & 1 == 1 {
      ret += 1
    }
    v >>= 1;
  }
  return ret;
}

fn main() -> std::io::Result<()> {
  let mut n = String::new();
  std::io::stdin().read_line(&mut n).unwrap();

  let input: Vec<i32> = {
    let mut a = String::new();
    std::io::stdin().read_line(&mut a).unwrap();
    a.split_whitespace()
      .map(|x| x.trim().parse::<i32>().unwrap())
      .collect()
  };

  for i in 0..input.len() {
    print!("{} ", getbinary(input[i] as u32));
  }
  Ok(())
}

/*
  $ cat .\DATA.lst | .\vmelendez.exe
  3 20 4 19 12 6 18 12 20 29 30 28 20 3 20 16 6 3 5
  10 10 30 5 29 28 31 20 15 6 27 29 13 7 22 10 0 14 17 22 1 27 19 12
*/
