/*
$ clippy-driver dfuribez.rs -D warnings
$ rustc dfuribez.rs
*/

use std::io::{self, BufRead};

fn find_duplicates(wordlist: Vec<&str>, size: usize) {
  let mut duplicated = Vec::new();
  for (index, element) in wordlist.iter().enumerate() {
    for element2 in wordlist.iter().take(size).skip(index + 1) {
      if element == element2 && !duplicated.contains(element) {
        duplicated.push(element);
      }
    }
  }

  duplicated.sort();

  for element in duplicated.iter() {
    print!("{} ", element);
  }
}

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();

  for (index, line) in lines.enumerate() {
    let l = line.unwrap();
    if index == 0 {
      let params: Vec<&str> = l.split_whitespace().collect();
      let size = params.len();
      find_duplicates(params, size);
    }
  }
  println!();
}

/*
$ cat DATA.lst | ./dfuribez
biq bis buq but byc byf byh dat deh diq dis dys gof gyt jec jit joh jup jut jys
les lih lit loc lox mec mef mic mof mop mos mox muc mus myf myh myq nah nak net
nih noq nos nus raf rec rep req ric ris roc rys vas veq vet vik voh vuh vyh zaf
zap zax zeq zup
*/
