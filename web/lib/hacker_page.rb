# frozen_string_literal: true

# rubocop:disable Lint/MissingCopEnableDirective
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength
# rubocop:disable Metrics/CyclomaticComplexity
# rubocop:disable Metrics/PerceivedComplexity
require 'yaml'

def user_page
  items = {}
  itemst1 = user_page_table_1
  itemst2 = user_page_table_2
  itemst3 = user_page_table_3
  itemst1[:general_users].each_key do |name|
    content = File.read('./layouts/user-page.slim')
    attributes = {
      title: name,
      layout: '/default.slim',
      itemst1: {
        code_users: itemst1[:code_users][name],
        hack_users: itemst1[:hack_users][name],
        vbd_users: itemst1[:vbd_users][name],
        general_users: itemst1[:general_users][name]
      },
      itemst2: {
        general_users: itemst2[:general_users][name],
        general_totals: itemst2[:general_totals][name]
      },
      itemst3: {
        general_users: itemst3[:general_users][name],
        general_totals: itemst3[:general_totals][name]
      }
    }
    identifier = "/users/#{name}.slim"
    items[name] = {
      content: content,
      attributes: attributes,
      identifier: identifier
    }
  end
  items
end

def user_page_table_1
  items = {}
  code_users, = user_ranking_code
  hack_users, = user_ranking_hack
  vbd_users, = user_ranking_vbd
  general_users = add_hashes(code_users, hack_users)
  general_users = add_hashes(general_users, vbd_users)
  general_users = add_percentage(general_users)
  general_users.each_key do |name|
    user_page_table_1_no_sltns(code_users, name) if code_users[name].nil?
    user_page_table_1_no_sltns(hack_users, name) if hack_users[name].nil?
    user_page_table_1_no_sltns(vbd_users, name) if vbd_users[name].nil?
    code_users[name][:name] = 'Code'
    hack_users[name][:name] = 'Hack'
    vbd_users[name][:name] = 'VbD'
    general_users[name][:name] = 'Total'
  end
  itemsp1 = {
    code_users: code_users, hack_users: hack_users, vbd_users: vbd_users,
    general_users: general_users
  }
  itemsp2 = git_user_stats_users_statistics
  itemsp1.each_key do |hash|
    items[hash] = add_hashes(itemsp1[hash], itemsp2[hash])
  end
  items
end

def user_page_table_2
  code_users, code_totals = user_page_table_2_code
  hack_users, hack_totals = user_page_table_2_hack
  vbd_users, vbd_totals = user_page_table_2_vbd
  general_users = add_hashes(code_users, hack_users)
  general_users = add_hashes(general_users, vbd_users)
  general_users.each_key do |user|
    general_users[user] = add_percentage(general_users[user])
  end
  general_totals = add_hashes(code_totals, hack_totals)
  general_totals = add_hashes(general_totals, vbd_totals)
  general_totals = add_percentage(general_totals)
  {
    general_users: general_users, general_totals: general_totals
  }
end

def user_page_table_2_code
  users = {}
  users_totals = {}
  folder = @config[:const][:code_dir]
  yml_path = @config[:const][:lang_data]
  lang_info = YAML.safe_load(File.read(yml_path))
  Dir.foreach(folder) do |page|
    next if page == '.' || page == '..' || File.file?(folder + '/' + page)

    page_path = folder + '/' + page + '/'
    Dir.foreach(page_path) do |challenge|
      chg_path = page_path + challenge
      next if challenge == '.' || challenge == '..' || File.file?(chg_path)

      sltns = Dir.glob(chg_path + '/*')
      sltns.each do |sltn|
        user = sltn.split('/')[-1].split('.')[0].strip
        sltn_ext = sltn.split('.')[-1].strip
        sltn_lang = search_lang(sltn_ext, lang_info)
        next if sltn_lang.nil?

        user_page_table_2_add_lang(
          user, sltn_lang, users, users_totals, lang_info
        )
        users[user][sltn_lang]['sltns'] += 1
        users_totals[user]['sltns'] += 1
        if check_sltn_unique_code(sltn)
          users[user][sltn_lang]['unique-sltns'] += 1
          users_totals[user]['unique-sltns'] += 1
        end
      end
    end
  end
  [users, users_totals]
end

def user_page_table_2_hack_vbd(folder)
  users = {}
  users_totals = {}
  sltn_lang = 'Gherkin'
  yml_path = @config[:const][:lang_data]
  lang_info = YAML.safe_load(File.read(yml_path))
  Dir.foreach(folder) do |page|
    next if page == '.' || page == '..' || File.file?(folder + '/' + page)

    page_path = folder + '/' + page + '/'
    Dir.foreach(page_path) do |challenge|
      chg_path = page_path + challenge
      next if challenge == '.' || challenge == '..' || File.file?(chg_path)

      sltns = Dir.glob(page_path + challenge + '/*.feature')
      sltns.each do |sltn|
        user = sltn.split('/')[-1].split('.')[0].strip
        sltn_ext = sltn.split('.')[-1].strip
        next unless sltn_ext == 'feature'

        user_page_table_2_add_lang(
          user, sltn_lang, users, users_totals, lang_info
        )
        users[user][sltn_lang]['sltns'] += 1
        users_totals[user]['sltns'] += 1
        if check_sltn_unique_hack_vbd(sltn)
          users[user][sltn_lang]['unique-sltns'] += 1
          users_totals[user]['unique-sltns'] += 1
        end
      end
    end
  end
  [users, users_totals]
end

def user_page_table_2_hack
  user_page_table_2_hack_vbd(@config[:const][:hack_dir])
end

def user_page_table_2_vbd
  user_page_table_2_hack_vbd(@config[:const][:vbd_dir])
end

def user_page_table_3
  code_users, code_totals =
    user_page_table_3_users(@config[:const][:code_dir])
  hack_users, hack_totals =
    user_page_table_3_users(@config[:const][:hack_dir])
  vbd_users, vbd_totals =
    user_page_table_3_users(@config[:const][:vbd_dir])
  general_users = add_hashes(code_users, hack_users)
  general_users = add_hashes(general_users, vbd_users)
  general_totals = add_hashes(code_totals, hack_totals)
  general_totals = add_hashes(general_totals, vbd_totals)
  general_users.each_value do |user|
    add_percentage(user)
  end
  general_totals = add_percentage(general_totals)
  { general_users: general_users, general_totals: general_totals }
end

def user_page_table_3_users(folder)
  lang_info = YAML.safe_load(File.read(@config[:const][:lang_data]))
  users = {}
  totals = {}
  Dir.foreach(folder) do |site|
    next if site == '.' || site == '..' || File.file?(folder + '/' + site)

    site_info = YAML.safe_load(File.read("#{folder}/#{site}/site-data.yml"))
    sltns = Dir.glob(folder + '/' + site + '/**/*.*') if code_dir?(folder)
    sltns = Dir.glob(folder + '/' + site + '/**/*.feature') if
      hack_dir?(folder) || vbd_dir?(folder)
    sltns.each do |sltn|
      next if code_dir?(folder) && !check_valid_sltn_code(sltn, lang_info)

      user = sltn.split('/')[-1].split('.')[0]
      if users[user].nil?
        users[user] = {}
        totals[user] = { 'sltns' => 0, 'unique-sltns' => 0 }
      end
      if users[user][site].nil?
        users[user][site] = deep_copy(site_info)
        users[user][site]['sltns'] = 0
        users[user][site]['unique-sltns'] = 0
      end
      users[user][site]['sltns'] += 1
      totals[user]['sltns'] += 1
      next unless (code_dir?(folder) && check_sltn_unique_code(sltn)) ||
                  ((hack_dir?(folder) || vbd_dir?(folder)) &&
                  check_sltn_unique_hack_vbd(sltn))

      users[user][site]['unique-sltns'] += 1
      totals[user]['unique-sltns'] += 1
    end
  end
  [users, totals]
end

def user_page_table_1_no_sltns(hash, name)
  hash[name] = {}
  hash[name][:sltns] = 0
  hash[name][:'unique-sltns'] = 0
  hash[name][:'unique-sltns-prcntg'] = 0
  hash[name][:npages] = 0
  hash[name][:pages] = []
end

def user_page_table_2_add_lang(user, lang, users, users_totals, lang_info)
  if users[user].nil?
    users[user] = {}
    users_totals[user] = {
      'sltns' => 0, 'unique-sltns' => 0, 'unique-sltns-prcntg' => 0.0
    }
  end
  return unless users[user][lang].nil?

  users[user][lang] = {
    'sltns' => 0, 'unique-sltns' => 0, 'unique-sltns-prcntg' => 0.0
  }
  if lang != 'Gherkin'
    users[user][lang]['url'] = lang_info[lang]['url']
    users[user][lang]['name'] = lang_info[lang]['name']
  else
    users[user][lang]['url'] = 'https://docs.cucumber.io/gherkin/'
    users[user][lang]['name'] = 'Gherkin'
  end
end
