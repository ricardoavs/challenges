## Version 1.4.1
## language: en

Feature:
  TOE:
    Badstore
  Location:
    http://192.168.56.4/cgi-bin/badstore.cgi
  CWE:
    CWE-0089: Improper Neutralization of Special Elements used in SQL command
  Rule:
    REQ.169: https://fluidattacks.com/web/en/rules/169/
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Detect and exploit a SQL Injection vulnerability
  Recommendation:
    Validate and sanitize input fields

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      | 2019.1a     |
    | Mozilla Firefox | 60.6.2esr   |
  TOE information:
    Given I am accessing the site http://192.168.56.4/cgi-bin/badstore.cgi
    And enter a web site thah allows me to see items for sale
    And I see the server is running Apache 1.3.28
    Then I see a search bar

  Scenario: Normal use case
    Given I access the site http://192.168.56.4/cgi-bin/badstore.cgi
    And I can see a navigation bar at the left including a search bar
    Then I can see the search bar allows to search items for sale

  Scenario: Static detection
    When I look at the code with inspect element
    Then I can see that the vulnerability is being caused by <form>
    And I see the following code
    """
    16 <FORM name=search onsubmit=/cgi-bin/badstore.cgi method=get>
    """
    Then I can conclude the code does not validate the input

  Scenario: Dynamic detection
    Given the vulnerability in the <form> tag
    Then I write a slash and a hyphen in the search field
    And I get the following output
    """
    SELECT itemnum, sdesc, ldesc, price FROM itemdb
    WHERE '/-' IN (itemnum,sdesc,ldesc)
    """
    Then I conclude the input are not validated

  Scenario: Exploitation
    Given the code does not validate the input in the serch bar
    Then I write a new search statement to exploit the SQLi
    And the SQL query is the following
    """
    SELECT itemnum, sdesc, ldesc, price FROM itemdb
    WHERE '1' OR '1'='1' IN (itemnum,sdesc,ldesc)
    """
    Then I see the query return different items [evidence](evidence.png)
    Then I can conclude that the site is vulnerable to SQL Injection

  Scenario: Remediation
    Given the site is using <form> to process the input data
    And this occurs with the search bar
    Then the input fields must be validated or sanitized
    And use the attributes type or pattern to espace special characters
    When the SQL query is processed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6/10 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.0/10 (High) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.9/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2019-05-14
