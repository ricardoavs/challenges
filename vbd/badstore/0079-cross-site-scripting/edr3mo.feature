## Version 1.4.1
## language: en

Feature:
  TOE:
    Badstore
  Location:
    http://192.168.1.94/cgi-bin/badstore.cgi?action=guestbook
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Detect and exploit insecure input fields with XSS
  Recommendation:
    Validate and sanitize the input fields

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Linux Mint      | Tessa 19.1  |
    | Mozilla Firefox | 66.0.2      |
  TOE information:
    Given I am accessing the site http://192.168.1.94/index.html
    And enter a site that allows me to see items for sale
    Then I see a search bar
    And I see a section that allows me to post comments
    Then I see the server is running Apache 1.3.28

  Scenario: Normal use case
    Given I access http://192.168.1.94/index.html
    And I can see a navigation bar at the left including a search bar
    Then I open the What's New section
    And I see different items for sale
    Then I open the Sign Our Guestbook section
    And I see three input fields which are name, email and comment

  Scenario: Static detection
    When I look at the code with inspect element
    Then I can see that the vulnerability is being caused by <form>
    And I see in the index.html web page the code is
    """
    16 <FORM name=search onsubmit=/cgi-bin/badstore.cgi method=get>
    """
    Then I see in the section Sign Our Guestbook the code is
    """
    <form method="POST"
    action="/cgi-bin/badstore.chi?action=doguestbook"
    enctype="application/x-www-form -urlencoded"></form>
    """
    Then I can conclude the code does not validate the input

  Scenario: Dynamic detection
    Given I see that <form> does not validate the input
    Then I execute the following code in the search bar input
    """
    <script>alert("XSS Test");</script>
    """
    And I get the output XSS Test inside an alert box
    Then I can conclude that the input is vulnerable to XSS

  Scenario: Exploitation
    Given the code does not validate the input
    And I can add a comment in the section Guest Our Guestbook
    Then I can add the following code as a comment
    """
    <script>alert("Hey I discover this XSS vulnerability");</script>
    """
    Then I get the output [evidence](evidence.png)
    And I see the code executes always when users access the guestbook
    Then I can conclude the site is vulnerable to XSS

  Scenario: Remediation
    Given the site is using <form> to process the input data
    And this occurs with the search bar and Guestbook comments
    Then the input fields must be validated or sanitized
    And use the attributes type or pattern to escape special characters

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.6/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2019-04-07
