## Version 1.4.1
## language: en

Feature:
  TOE:
    diwa
  Location:
    http://172.17.0.2/index.php - page (field)
  CWE:
    CWE-98: Improper Control of Filename for Include/Require Statement
    in PHP Program ('PHP Remote File Inclusion')
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Include unintended PHP files
  Recommendation:
    Implement a white list

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.07.01  |
    | Brave           | 1.10.97     |
    | Docker          | 19.03.12-ce |
  TOE information:
    Given I am accessing the site at http://172.17.0.2/
    And it runs on a docker container

  Scenario: Normal use case
    Given I access the site
    And I see I can browse beetewn several sections [evidence](sec.png)

  Scenario: Static detection
    When I look at the code at "index.php"
    Then I can see no validation it's done on parameter "page"
    """
    54  if(isset($_GET['page'])) {
    55    $content = $_GET['page'];
    56  }
    57    else {
    58    $content = 'home';
    59  }
    60
    61  $contentFile = CONTENT_PATH . '/' . $content . '.php';
    """
    Then I can conclude "index.php" it's vulnerable to LFI

  Scenario: Dynamic detection
    Given that now I know the parameter vulnerable
    Then I try the following
    """
    http://172.17.0.2/index.php?page=%00
    """
    And I can see the warning:
    """
    Warning: file_exists() expects parameter 1 to be a valid path, \
    string given in /var/www/html/index.php on line 63
    """
    And I realize that I can not use a null byte attack
    And I can conclude that the site is vulnerable to LFI

  Scenario: Exploitation
    Given that now I know "index.php" is vulnerable to LFI
    When  I try to include an unintended php file
    """
    http://172.17.0.2/?page=upload
    """
    Then I can see it's content gets displayed [evidence](upload.php)
    When I try to include a php file outside the intended folder
    """
    http://172.17.0.2/?page=../index
    """
    Then I see nothig gets displayed neither a 404 error nor "index" content
    And that's because of the "require_once" "index" has already being included
    Then I can conclude that I can exploit the LFI
    But only with php files already on the server

  Scenario: Remediation
    Given I have patched the code by implementing a white list lines 61, 62
    """
    54  if(isset($_GET['page'])) {
    55    $content = $_GET['page'];
    56  }
    57  else {
    58    $content = 'home';
    59  }
    60
    61  $white_list = array("documentation", "downloads", "contact", "home");
    62  if (in_array(trim($content), $white_list)) {
    63    $contentFile = CONTENT_PATH . '/' . $content . '.php';
    64    require_once $contentFile;
    65    } else {
    66      require_once CONTENT_PATH . '/404.php';
    67  }
    """
    Then If I go back to
    """
    http://172.17.0.2/?page=upload
    """
    Then I get a 404 error message [evidence](404.png)
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.2 (Medium) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.7 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date {2020-07-15}
