## Version 1.4.1
## language: en

Feature:
  TOE:
    diwa
  Location:
    http://172.17.0.2/?page=thread - id (field)
  CWE:
    CWE-284: Improper Access Control
  Rule:
    REQ.097: Define control access model
  Goal:
    Get access to private post
  Recommendation:
    Check user's privileges before showing content

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.07.01  |
    | Brave           | 1.11.101    |
    | Docker          | 19.03.12-ce |
  TOE information:
    Given I am accessing the site at http://172.17.0.2/
    And it runs on a docker container

  Scenario: Normal use case
    Given I am logged as an user
    When I am in the "Board" section
    Then I can see a list of posts both public and private [evidence](board.png)

  Scenario: Static detection
    When I look at the code at "thread.php"
    """
    3 if(!isset($_SESSION['user_id'])) {
    4   echo getForbiddenMessage();
    5   return;
    6 }
    """
    Then I can see that the code only checks if the user is logged in
    But no if the user has the privileges to see the content of the post
    Then I can conclude any logged user can see admin's posts

  Scenario: Dynamic detection
    Given I am on the "Board" section
    And I can see three posts two public and one private
    And the "id" of both public posts are 1 and 23
    Then I try accessing
    """
    http://172.17.0.2/?page=thread&id=3
    """
    And I can see the content of the post [evidence](unautorized)
    Then I can conclude that the system is vulnerable

  Scenario: Exploitation
    Given that now I know the system is vulnerable
    Then it can be exploited either by knowing the id or bruteforce it
    And to access the content of the post
    """
    http://172.17.0.2/?page=thread&id=<id_private_post>
    """
    Then I can conclude that the system can be exploited

  Scenario: Remediation
    Given I have patched the code by adding the lines 54 to 58
    """
    53  $adminsOnly = (1 == $thread['admins_only']);
    54  $isAdmin = (isset($_SESSION['user']['is_admin']) && 1 ==\
          $_SESSION['user']['is_admin']);
    55
    56  if ($adminsOnly && !$isAdmin) {
    57    die("Restricted content");
    58  }
    """
    And if the user is not an admin
    When he tries to access the post
    Then the message "Restricted content" will be shown [evidence](fixed.png)
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.0 (Medium) - AV:N/AC:L/PR:L/UI:N/S:C/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.8 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.8 (Medium) - MAV:N/MAC:L/MPR:L/MUI:N/MS:C/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date {yyyy-mm-dd}
