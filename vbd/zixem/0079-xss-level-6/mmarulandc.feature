## Version 1.4.1
## language: en

Feature:
  TOE:
    zixem
  Category:
    Cross Site Scripting
  Location:
    http://zixem.altervista.org/XSS/6.php - name (field)
  CWE:
    CWE-79: Improper Neutralization of Input During
    Web Page Generation ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Execute an alert with message 1337
  Recommendation:
    Remove user intervention

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 19.10       |
    | Firefox         | 72.0.2      |
  TOE information:
    Given I am accessing the site
    And The page is made in PHP
    And The page displays a message

  Scenario: Normal use case
    Given The page just displays a message
    When I check out all the page
    Then I realize displaying a message is its only functionality

  Scenario: Static detection
    Given There is not access to the source code
    When I try a static detection
    Then I realize this is not possible

  Scenario: Dynamic detection
  Detecting DOM based XSS
    When The data is passed by parameter in the URL
    Then The value is displayed as a message
    When I input HTML tags in the URL
    Then The page says "XSS detected" [evidence](evidence.png)
    And I think I could inject HTML with unicode
    When I input the next string in the URL:
    """
    https://www.zixem.altervista.org/XSS/6.php?name=\u003Ch1\u003Ethisisvulnerable\u003C/h1\u003E
    """
    Then I can conclude the site is vulnerable to Cross-site scripting [evidence](evidence2.png)

  Scenario: Exploitation
    When I enter the following string
    """
    http://zixem.altervista.org/XSS/4.php?img=test'onerror='alert(1337)
    """
    Then the site executes the JavaScript code [evidence](evidence3.png)

  Scenario: Remediation
    Given The parameter is being found through a query string
    When The user changes the value of the query string
    Then The name changes
    And putting a paragraph tag of HTML would fix the vulnerability with no user intervention
    """
    <p>zxm</p>
    """
    And It would be enough

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.3/10 (Medium) - E:H/RL:U/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.3/10 (Medium) - E:P/RL:U/RC:U

  Scenario: Correlations
    No correlations have been found to this date 2020-02-04
