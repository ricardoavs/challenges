## Version 1.4.1
## language: en

Feature:
  TOE:
    zixem
  Category:
    SQL injection
  Location:
    http://zixem.altervista.org/SQLi/lvl8.php?id=1 - user (field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used
    in an SQL Command ('SQL Injection')
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Verify if the site is vulnerable to SQL Injection
  Recommendation:
    Use prepared SQL statements and sanitize inputs

  Background:
  Hacker's software:
    | <Software name>     | <Version>       |
    | Windows 10          | 1809            |
    | Google Chrome       | 75.0.3770.100   |
  TOE Information:
    Given I access the main page
    And the page is made with PHP
    And MySQL is the Database Management System

  Scenario: Normal use case
    Given the page only shows one query
    When I read it
    Then I can conclude that it is only its functionality

  Scenario: Static detection
    Given No access to the source code
    Then it is not possible to perform a static detection

  Scenario: Dynamic detection
    Given The search bar
    When I put a query
    Then the page returns the user information with "id" 1
    And I can conclude that the id parameter is a possible attack vector

  Scenario: Exploitation
    Given the vulnerability
    When I use this value for the parameter "id"
    """
    1%09AND%091=2%09UNION%09SELSELECTECT%09user(),version(),3--
    """
    Then I get the passwd file [evidence](evidence.png)

  Scenario: Remediation
    Given an input filter, we can know when a query is entered in the URL
    Then the filtered query is passed only as text
    When entering a value, you must use prepared statements with variable link
    Then the database can distinguish between code and data
    And you must use white list entry validation to avoid user input in the query

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      8.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.2/10 (High) - E:H/RL:O/RC:C/CR:H/IR:X/AR:X
    Environmental: Unique and relevant attributes to a specific user environment
      9.1/10 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2020-01-30
