## Version 1.4.1
## language: en

Feature:
   TOE:
    Web scanner test site
  Category:
    Improper Neutralization of Special Elements in Output Used by a
    Downstream Component ('Injection')
  Location:
    http://www.webscantest.com/crosstraining/aboutyou2.php
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page
    Generation ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject Cross site scripting
  Recommendation:
    protect each text box

  Background:
  Hacker's software:
    | <Software name> | <Version>         |
    | Ubuntu (Bionic) | 18.04.4 LTS       |
    | Google Chrome   | 80.0.3987.132     |
  TOE information:
    Given I am accessing the site
    """
    http://www.webscantest.com/crosstraining/aboutyou2.php
    """
    Then I can see three text boxes
    And allows me fill them

  Scenario: Normal use case:
    Given I access .../aboutyou2.php
    And I filled the boxes
    """
    fname: Richard
    nick: anaeru
    lname: Vega
    """
    Then I clicked on submit button
    And it returned me
    """
    Welcome Richard "anaeru" Vega.
    """

  Scenario: Static detection
    Given there isn't a source code
    Then there isn't a static detection scenario either

  Scenario: Dynamic detection
    Given I access .../aboutyou2.php
    When I filled each field with "<script>alert(1)</script>"
    And submitted it
    Then Alert 1 triggered twice
    But the third alert didn't show up
    And the site returned me
    """
    Welcome "" <script>alert(1)</script>.
    """
    And in [page source code](s1.png)
    And it looks like third field is protected

  Scenario: Exploitation
    Given information I got from dynamic detection
    And before 'welcome' there is a table open tag
    And next to "last name" is ".<br/>"
    Then I tried to join the fields to bypass the protection in third field
    """
    fname: <script>alert(1); var a =
    nick: 3";alert(2)</script></table><table foo="0
    lname: onmouseover=alert(a) fou=
    """
    And alerts [1](a1.png) and [2](a2.png) showed up
    When I moved the mouse over page, the alert [3](a3.png) showed up
    Then I looked the [page](fp.png) and the [source](fs.png)
    And I concluded this page is completly exploitable

  Scenario: Remediation
    The shorter and easiest way is to use htmlspecialchars() function to
    protect all three fields

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.2/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.1/10 (Medium)     - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-17
