# VbD by [Fluid Attacks](https://fluidattacks.com/)

In this repo you will find an extensive collection
of documented vulnerabilities and hacks
made by [Fluid Attacks](https://fluidattacks.com/)'
hackers and trainees to several vulnerable applications,
especially designed to train people on
[Pen-Testing](https://en.wikipedia.org/wiki/Penetration_test).

## Getting Started

### Trainees

Hey there! We hope you have fun while hacking some
[ToE](https://en.wikipedia.org/wiki/Common_Criteria)'s! 😄

Also, please make sure you understand all the
[rules](https://fluidattacks.com/web/en/careers/technical-challenges/)
before you start posting new solutions to the repository.

Let the hacking begin! 😎

### Guests

Feel free to roam around the repository and ask any questions. 😄

Make sure to read our [License](#license) section
before using any data from this project.

## ToE's List

The vulnerable applications currently included are:

* [Bodgeit](https://github.com/psiinon/bodgeit)
* [OWASP Bricks](https://www.owasp.org/index.php/OWASP_Bricks)
* [bWAPP](http://www.itsecgames.com/)
* [Damn Vulnerable NodeJS Application (DVNA)](https://github.com/appsecco/dvna)
* [Damn Vulnerable Web Application (DVWA)](http://www.dvwa.co.uk/)
* [Firing Range](https://github.com/google/firing-range)
* [Gruyere](https://google-gruyere.appspot.com/)
* [OWASP Hackademic Challenges Project](https://www.owasp.org/index.php/OWASP_Hackademic_Challenges_Project)
* [Hackazon](https://github.com/rapid7/hackazon/wiki)
* [OWASP Juice Shop Project](https://www.owasp.org/index.php/OWASP_Juice_Shop_Project)
* [LAMPSecurity Training](https://sourceforge.net/projects/lampsecurity/)
* [Metasploitable 2](https://metasploit.help.rapid7.com/docs/metasploitable-2)
* [Mutillidae](http://www.irongeek.com/i.php?page=security/mutillidae-deliberately-vulnerable-php-owasp-top-10)
* [WackoPicko Vulnerable Website](https://github.com/adamdoupe/WackoPicko)
* [OWASP WebGoat Project](https://www.owasp.org/index.php/Category:OWASP_WebGoat_Project)
* [XSS Game](https://xss-game.appspot.com/)
* [Xtreme Vulnerable Web Application (XVWA)](https://github.com/s4n7h0/xvwa)
