## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Unvalidated Redirects & Forwards
  Location:
    unvalidated_redir_fwd_2.php?ReturnUrl=portal.php - ReturnUrl (header)
  CWE:
    CWE-601: Changing the the return site
  Rule:
    REQ.008: https://fluidattacks.com/web/es/rules/008/
  Goal:
    Alter the return url
  Recommendation:
    Change the method of redirect of url

  Background:
  Hacker's software:
    | <Software name> | <Version>               |
    | Windows         | 10.0.17134.407 (64-bit) |
    | Firefox         | 64.0          (64-bit)  |
    | XAMPP           | 3.2.2                   |
    | Burp Suite      | 1.7.36                  |
  TOE information:
    Given I'm running bWAPP-latest with XAMPP on my localhost
    And I'm accessing the bWAPP pages through my browser
    And I create a user in the directory

  Scenario: Normal use case
  Normal page with normal text
    When I access
    """
    http://localhost/bWAPP/unvalidated_redir_fwd_2.php
    """
    And everything works normally

  Scenario: Static detection
  The site not validated the method of return of the url
    Given I use the view source code
    Then I found the following line
    """
    <a href="unvalidated_redir_fwd_2.php?ReturnUrl=portal.php">
    here</a> to go back to the portal.
    """
    And I saw that is possible alter the redirection

  Scenario: Exploitation
  The line of code is vulnerable to manipulation
    Given I have the burp suite on my system
    Then I proceed to intecept the conexion
    And I alter the url of return
    And I get redirect the site successfully [evidence](intercept.png)

  Scenario Outline: Remediation
  Simply avoid using redirects and forwards.
    """
    If used, do not allow the url as user input for the destination.
    This can usually be done. In this case, you should have
    a method to validate URL.
    If user input can’t be avoided, ensure that the supplied value is valid,
    appropriate for the application, and is authorized for the user.
    It is recommended that any such destination input be mapped to a value,
    rather than the actual URL or portion of the URL,
    and that server side code translate this value to the target URL.
    Sanitize input by creating
    a list of trusted URL's (lists of hosts or a regex).
    Force all redirects to first go through a page notifying
    users that they are going off of your site,
    and have them click a link to confirm.
    """
  Scenario: Correlations
    No correlations have been found to this date 2018-12-14