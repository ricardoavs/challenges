## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Cross Site Scripting
  Page name:
    bWAPP/xss_ajax_1-1.php
  CWE:
    CWE-80: Improper Neutralization of Script-Related HTML Tags in a Web Page
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Enter a proper malicious input to access sensitive information
  Recommendation:
    Reinforce validation of special characters to avoid code injection

  Background:
  Hacker's software:
  | <Software name>    |    <Version>         |
  | Ubuntu             | 16.04 LTS (x64)      |
  | Mozilla Firefox    | 63.0                 |
  | Visual Studio Code | 1.29.1               |
  TOE information:
    Given I'm running bee-box on VMWare on Ubuntu
    And I'm accessing the bWAPP pages through my Firefox browser
    And enter a php site that allows me to upload files of any type

  Scenario: Normal use case
  The site allows the user to search for a film by its name
    Given The access to the page bWAPP/xss_ajax_1-1.php
    Then I center a film name e.g. "x-men"
    And I get the response "Yes! We have that movie.."
    And I enter another film name e.g. "spiderman"
    And I get the response "spiderman??? Sorry, we don't have that movie :("

  Scenario: Dynamic detection
    Given the url http://localhost/bWAPP/xss_ajax_1-1.php
    Then I open the browser developer tools
    And I see a continuous request to the url
    """
    http://localhost/bWAPP/xss_ajax_1-2.php?title=
    """
    Then I open the url and input some test characters in the title parameter
    And I notice i get an XML parsing error for "<" character
    And for other characters the response is plain XML with message
    """
    <response>value??? Sorry, we don't have that movie :(</response>
    """
    Then I input "&lt &gt" instead of the symbols and get no error
    And I realize I can input html tag with "&lt &gt"

  Scenario: Exploitation
    Given the url "http://localhost/bWAPP/xss_ajax_1-1.php"
    Then I try entering some html tag that might generate a javascript alert
    And get sensitive information of the cookies
    And I try entering a HTML link tag to a non-existing css file
    And For an error producing an alert with cookie information
    """
    &lt;link rel="stylesheet" href="ne.css" onerror="alert(document.cookie)"&gt;
    """
    And I get the output
    """
    security_level=0; PHPSESSID=432q6f72ifcf69voova94v67h6
    """
