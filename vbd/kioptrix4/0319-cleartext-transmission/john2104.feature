## Version 1.4.1
## language: en

Feature:
  TOE:
    Kioptrix4
  Category:
   Sniffing attack
  Location:
    http://192.168.60.130/checklogin.php
  CWE:
    CWE-319: Cleartext Transmission of Sensitive Information
  Rule:
    REQ.181 Transmit data using secure protocols
  Goal:
    Capture credentials on shared network
  Recommendation:
    Use secure services like HTTPS

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | NixOS                | 74.0 (64-bit)  |
    | Mozilla firefox      | 74.0 (64-bit)  |
    | Wireshark            | 3.0.3          |
  TOE information:
    Given I am accessing the site http://192.168.60.130/
    And Entered to site ../checklogin.php
    Then I can see there is a login page
    And allows me to connect with given credentials

  Scenario: Normal use case
    Given I access http://192.168.60.130/checklogin.php
    And I write an username in the login input
    And A password in the password input
    Then I push the Sign in button
    And I get access to the user pages

  Scenario: Static detection
    Given I do not have access to the source code
    Then I can not make static detection

  Scenario: Dynamic detection
    Given I access to the site http://192.168.60.130/
    Then I can see that they doesn't use HTTPS to log in

  Scenario: Exploitation
    Given I access to a shared network (like WiFi)
    Then I can intercept requests to ../checklogin.php using wireshark
    And see clear text credentials
    Then I can access to the user interface

  Scenario: Remediation
    Use secure services like HTTPS

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.6/10 (Medium) - AV:A/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    4.4 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.1 (Medium) - CR:H/IR:H/AR:H/MAV:N/MAC:L/MPR:N/MUI:R/MS:U/MC:L/MI:L/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-03-30
