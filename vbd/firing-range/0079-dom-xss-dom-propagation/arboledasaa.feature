# language: en

Feature: DOM XSS - DOM Propagation
  From Firing Range System
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/

  Background:
    Given I'm running Windows 7 Pro
    And also using Google Chrome version 69.0.3497.100
    Given the following
    """
    Message: DOM Propagation
    Details:
      - XSS payload gets stored in DOM and later retrieved in JavaScript
    """

    Scenario: DOM XSS - DOM Propagation
    Given the initial site
    Given I inspect the page source code
    """
    <script>
      // Writing to DOM attribute document.title and
      // reading back propagates the XSS payload.

      var payload = location.hash.substr(1);
      window.status = payload;
      var retrieved_payload = window.status;
      eval(retrieved_payload);

    </script>
    """
    When I see the URL i noticed that it reads the hash argument
    And  I append into the URL:
    """
    #alert(1)
    """
    And request the page
    Then an alert is executed with the browser
    Then the vulnerability is verified