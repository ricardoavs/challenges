## Version 1.4.2
## language: en

Feature:
  TOE:
    Firing Range
  Category:
    Injection Flaws
  Location:
    firing-Range/angular/angular_body_raw_post/1.6.0 - q (parameter)
  CWE:
    CWE-829: Inclusion of Functionality from Untrusted Control Sphere
  Rule:
    REQ.155: https://fluidattacks.com/web/es/rules/155/
  Goal:
    Execute arbitrary JS
  Recommendation:
    Double check inclusion addresses, set CSP

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am accessing Firing Range at
    """
    https://public-firing-range.appspot.com
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to https://firing-range/remoteinclude/parameter/script
    Then I see a blank page

  Scenario: Static detection
  Typo in remote include JS
    When I look at  the page source
    """
    01  <html>
    02    <head><title>Remote include - URL seeded from query parameter</title>
    </head>
    03    <body>
    04      <script src="http://g00gle.com/typosquatting_domain.js"></script>
    05    </body>
    06  </html>
    """
    Then I see it's sourcing "typosquatting_domain.js" from "g00gle.com"

  Scenario: Dynamic detection
  Angular fuzzing
    Given I host a file called "typosquatting_domain.js" that triggers an alert
    And I change my hosts file to point "g00gle.com" to localhost
    And simulate registering the typoed domain
    Then I reload the page and get an alert

  Scenario: Exploitation
  There is no way to meaningfully exploit vulnerabilities in this ToE

  Scenario: Remediation
  Fix Typo
    Given I patch the code like this
    """
    01  <html>
    02    <head><title>Remote include - URL seeded from query parameter</title>
    </head>
    03    <body>
    04      <script src="http://google.com/typosquatting_domain.js"></script>
    05    </body>
    06  </html>
    """
    Then it gets the script from the source it's supposed to

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    3.7/10 (Low) - AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.5/10 (Low) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.5/10 (Low) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-21
