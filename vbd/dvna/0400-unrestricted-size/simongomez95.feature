## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Other Bugs
  Location:
    /app/bulkproducts - File
  CWE:
    CWE-0770: Allocation of Resources Without Limits or Throttling -base-
      https://cwe.mitre.org/data/definitions/770.html
    CWE-0400: Uncontrolled Resource Consumption -class-
      https://cwe.mitre.org/data/definitions/770.html
    CWE-0985: SFP Secondary Cluster: Unrestricted Consumption -category-
      https://cwe.mitre.org/data/definitions/985.html
  CAPEC:
    CAPEC-0231: XML Oversized Payloads -standard-
      http://capec.mitre.org/data/definitions/231.html
    CAPEC-0130: Excessive Allocation -meta-
      http://capec.mitre.org/data/definitions/130.html
  Rule:
    REQ.039: https://fluidattacks.com/web/es/rules/039/
  Goal:
    Upload a huge file
  Recommendation:
    Limit file upload size

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No size control
    Given I see the code at "dvna/core/appHandler.js"
    """
    ...
    233 module.exports.bulkProducts =  function(req, res) {
    234   if (req.files.products && req.files.products.mimetype=='text/xml'){
    235     var products = libxmljs.parseXmlString(req.files.products.data.toStr
    ing('utf8'), {noent:true,noblanks:true})
    236     products.root().childNodes().forEach( product => {
    237       var newProduct = new db.Product()
    238       newProduct.name = product.childNodes()[0].text()
    239       newProduct.code = product.childNodes()[1].text()
    240       newProduct.tags = product.childNodes()[2].text()
    241       newProduct.description = product.childNodes()[3].text()
    242       newProduct.save()
    243     })
    244     res.redirect('/app/products')
    245   }else{
    246     res.render('app/bulkproducts',{messages:{danger:'Invalid file'},lega
    cy:false})
    247   }
    258 }
    ...
    """
    Then I see it processes files without even checking their size

  Scenario: Dynamic detection
  Uploading big file
    Given I go to "http://localhost:8000/app/bulkproducts"
    Then I upload a 600mb binary file
    Then it stays uploading and processing for a long while
    Then it shows an invalid file error

  Scenario: Exploitation
  Resource abuse
    Given I upload a really big file
    And optionally do it concurrently from multiple clients (i.e. with a botnet)
    Then I can consume a high ammount of disk space and CPU time in the server
    And eventually run it out of resources or skyrocket the cloud bill

  Scenario: Remediation
  Access control
    Given I add this middleware to the "app.js" router
    """
    app.use(express.limit('5M'));
    """
    Then the server won't accept files larger than 5mb

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C/
  En4.9onmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-28
