## Version 1.4.1
## language: en

Feature:
  TOE:
    MCIR: Magical Code Injection Rainbow
  Category:
    Shell Command Injection
  Location:
    phpwn/eval.php - Injection String (field)
  CWE:
    CWE-94: Code Injection
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Retrieve the /etc/passwd file
  Recommendation:
    Avoid dinamic code generation

  Background:
  Hacker's software:
    |     <Software name>      |    <Version>    |
    |          OWASP           |       1.6       |
    |   VMWare Workstation 15  |     15.0.2      |
    |     Firefox Quantum      |     65.0.2      |
  TOE information:
    Given I'm running OWASP on VMWare
    And I'm accessing the MCIR pages through my Firefox browser
    And I enter a php page that takes a string
    And passes it to the eval function

  Scenario: Normal use case
  There is no normal use case since the field is intended for code injection

  Scenario: Static Detection
  There is no validation on the input before it is passed to the eval function
    When I look at the source code
    And I check the code that processes the input
    """
    43  $variable = '$var2';
    44  $value = "foobar";
    45  $eval_string = $variable . " = '" . $value . "';";
    ...
    53  switch($_REQUEST['location']){
    ...
    58    case 'value':
    59        $eval_string = str_replace($variable, '$' .
                     $_REQUEST['inject_string'], $eval_string);
    60        break;
    ...
    77  $output = eval($eval_string);
    """
    Then I notice there is no validation on the input string

  Scenario: Dynamic detection
  The input of the Injection string field can take any php code
    Given I enter the following payload into the injection field
    """
    1  ';phpinfo();'
    """
    And I click the Inject! button
    Then I can see the php configuration of the target server
    And it can be seen in [evidence](phpinfo.png)
    And I conclude the application is vulnerable to code injection

  Scenario: Exploitation
  Disclose the contents of /etc/passwd
    Given I enter the following payload into the injection field
    """
    1  ';system("cat /etc/passwd");'
    """
    And I click the Inject! button
    Then the content of /etc/passwd is shown
    And it can be seen in [evidence](etc-passwd.png)

  Scenario: Remediation
  Avoid dynamic code generation. Validate the input if it is not possible
    Given I have patched the code by adding the following lines
    """
    58    case 'value':
    59        $eval_string = str_replace($variable, '$' .
                     $_REQUEST['inject_string'], $eval_string);
    60        $eval_string = preg_replace('/(system|exec|passtru|shell_exec)/i
                             ', '', $eval_string);
    61        break;
    """
    Then I can prevent the injection of the code shown above
    Given I re-run my exploit
    Then I get an empty response
    And it can be seen in [evidence](empty-response.png)

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.4/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.0/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.4/10 (Critical) - CR:L/IR:H

  Scenario: Correlations
    No correlations have been found to this date 2019-03-27
