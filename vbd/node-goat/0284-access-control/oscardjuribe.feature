## Version 2.0
## language: en

Feature: Improper Access Control
  TOE:
    Node-goat
  Category:
    Improper Access Control
  Location:
    http://localhost:4000/benefits
  CWE:
    CWE-284: Improper Access Control
  Rule:
    REQ.097 https://fluidattacks.com/web/rules/097/
    REQ.096 https://fluidattacks.com/web/rules/096/
  Goal:
    Access to restricted functions
  Recommendation:
    Check the privileges and access for each user

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      | 4.19.0      |
    | Firefox         | 60.6.2      |
    | Dirb            | v2.22       |
    | Ubuntu          | 18.04       |
  TOE information:
    Given The site "http://localhost:4000/"
    When I enter into it
    Then The server is running on NodeJs "v8.10.0"

  Scenario: Normal use case
    Given The route "/"
    When I enter to it
    Then I can make actions that are allowed only for that user

  Scenario: Static detection
    Given The source code
    When I go to "app/routes/index.js"
    Then I can find where the "/benefits" route is called
    When I understand the code
    Then I see that there isn't a middleware to validate the admin rights
    """
    app.get("/benefits", isLoggedIn, benefitsHandler.displayBenefits);
    app.post("/benefits", isLoggedIn, benefitsHandler.updateBenefits);
    """
    And Whatever user can access to the "/benefits" page without admin rights

  Scenario: Dynamic detection
    Given The site "http://localhost:4000/"
    When I enter to "http://localhost:4000/benefits"
    Then I access into a page where I should need admin rights
    And The page doesn't have an appropiate access control

  Scenario: Exploitation
    Given The site "http://localhost:4000/"
    When I use the tool "dirb" in the site
    """
    ---- Scanning URL: http://localhost:4000/ ----
    + http://localhost:4000/benefits (CODE:302|SIZE:28)
    + http://localhost:4000/dashboard (CODE:302|SIZE:28)
    """
    Then I get the route "/benefits" that there isn't in my menu as normal user
    When I enter into it in Firefox
    Then I have a site where i can change values pretending to be the admin
    And The site has an inappropriate access control [evidence](image1.png)

  Scenario: Remediation
    Given The source code
    When I go to "app/routes/index.js"
    Then I can add a middleware to validate admin rights
    """
    var isAdmin = sessionHandler.isAdminUserMiddleware;
    """
    When I use the middleware in the get and post request
    """
    app.get("/benefits", isLoggedIn, isAdmin, benefitsHandler.displayBenefits);
    app.post("/benefits", isLoggedIn, isAdmin, benefitsHandler.updateBenefits);
    """
    Then Only admin users can access the site
    When I enter to "http://localhost:4000/benefits"
    Then I am redirected to the login page
    And The vulnerability is patched [evidence](image2.png)

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.3/10 (High) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:L/A:L
    Temporal: Attributes that measure the exploit's popularity and fixability
      6.3/10 (Medium) - E:P/RL:O/RC:U/CR:M/IR:L/AR:L
    Environmental: Unique and relevant attributes to a specific user environment
      5.9/10 (Medium) - MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:H/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2019-09-18
