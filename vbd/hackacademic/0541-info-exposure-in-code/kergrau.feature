## Version 1.4.1
## language: en

Feature:
  TOE:
    hackademic
  Location:
    http://127.0.0.1/hackademic/challenges/ch001/index.php
  CWE:
    CWE-0540: Information Exposure Through Source Code -variant-
      https://cwe.mitre.org/data/definitions/540.html
    CWE-0538: File and Directory Information Exposure -base-
      https://cwe.mitre.org/data/definitions/538.html
    CWE-0200: Information Exposure -class-
      https://cwe.mitre.org/data/definitions/200.html
    CWE-0963: SFP Secondary Cluster: Exposed Data -category-
  CAPEC:
    CAPEC-639: Probe System Files -detailed-
      http://capec.mitre.org/data/definitions/639.html
    CAPEC-545: Pull Data from System Resources -standard-
      http://capec.mitre.org/data/definitions/545.html
    CAPEC-116: Excavation -meta-
      http://capec.mitre.org/data/definitions/116.html
  Rule:
    REQ.156: https://fluidattacks.com/web/en/rules/156/
  Goal:
    Find list of emails of this site.
  Recommendation:
    Remove file which contains sensible information.

  Background:
  Hacker's Software:
    | <Software name> |     <Version>     |
    | Ubuntu          | 16.04.1 (64-bits) |
    | XAMPP           | 7.2.0             |
    | Mozilla Firefox | 63.0.3 (64-bits)  |
    | Hackademic      | 0.9               |
  TOE information:
    Given I'm running Hackademic on XAMPP
    And I access to challenge page through firefox
    Then I access to challenge 2 to find password

  Scenario: Normal Case
  The site allows enter a password and validate it.
    Given I go to http://127.0.0.1/hackademic/challenges/ch001/index.php
    And I see the page ask to me a code and password.

  Scenario: Static Detection
  The source code include sensible information.
    When I open code I could find one file with the bellow client's list email
    """
    Everyone is here... xexexe!
    ----------------------------------------
    Crazy Alice Alice@InWonderland.com
    Nebu Chadnezzar NebuChadnezzar@OldKing.edu
    Jo Raimontilinekergrobelar ShortName@badmail.com
    Web Killer WebMurder@killer.ever.com
    Don Quixote windmill@mail.spain
    Crazy priest Exorcist@hotmail.com
    Jasson Killer Friday13@JasonLives.com
    Everything All AllweSaid@mail.com
    Thiseas Sparrow Pirates@mail.gr
    Black Dreamer SupaHacka@mail.com
    Bond James MyNameIsBond@JamesBond.com
    Poor Boy Millionaire@fmail.com
    Blind Lynxeyed Linxblind@siou.com
    Earl Dracula CarpathianServers@Blood.com
    Tea Coffee sugar@dring.com
    Whisky Vodka drink@drunk.com
    """
    Then I conclude that in this file there is confidential information
    And that should not be in the source code.

  Scenario: Dynamic Detection
  I can see the route to go to sensible information in the source code.
    Given the challenge page I opened the code inspector
    Then I opened all links
    Then I analyzed the code
    And I saw a the source of image "./secret_area_/mails.gif"
    And so, if you change the extension to .txt you can access to list emails
    Then I conclude the problem is I can see a route to access a email list
    When I inspect source code.

  Scenario: Exploitation
  Complete link and change its extention
    Given the source file shown in the static detection section
    Then I proceed to compose a link with the source file
    Then the composed link is
    """
    127.0.0.1/hackademic/challenges/ch001/main/secret_area_/mails.txt
    """
    Then if I put the before link in the browser I've client's emails list
    Then I conclude I complete link then I got client's emails list

  Scenario: Remediation
  Remove file which contains emails list
    Given I've patched the code
    When I removed file which contains client's email list
    Then the list is not accesible now
    Then you can see a 404 error.[evidence](404-error.png)

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    2.5/10 (Low) - AV:L/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.8/10 (Medium) - E:F/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.8/10 (Medium) - CR:X/IR:X/AR:X

  Scenario: Correlations
    No correlations have been found to this date 2018-12-13