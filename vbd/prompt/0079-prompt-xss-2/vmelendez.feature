## Version 1.4.1
## language: en

Feature:
  TOE:
    Prompt
  Category:
    Cross-Site Scripting
  Location:
    http://prompt.ml/2
  CWE:
    CWE-079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Execute javascript code performing an XSS attack
  Recommendation:
    Encode correctly user input

  Background:
  Hacker Software:
    | Firefox            |   79.0     |
    | Windows            |   10.0     |

  TOE information:
    Given I access the site by its URL
    Then I see a sandbox environment to interact with the site
    And two sections to see the site's responses [evidences](site.png)

  Scenario: Normal use case
    Given I access the site
    Then I can enter something in the input box
    And it will be reflected in the sandbox web viewer [evidences](normal.png)

  Scenario: Static detection
    Given the site provides an area called "Text Viewer"
    And which is basically the source code of the relevant function
    Then I could analyze this function
    """
    function escape(input) {
        //                      v-- frowny face
        input = input.replace(/[=(]/g, '');

        // ok seriously, disallows equal signs and open parenthesis
        return input;
    }
    """
    And it's simple takes an input as an argument
    Then as the comment indicates it will replace the equal sign
    And the open parenthesis with nothing ('')
    Then it is possible to execute javascript code
    When these characters are not taken into account

  Scenario: Dynamic detection
    Given the site operates within a sandbox
    Then I can't do dynamic detection

  Scenario: Exploitation
    Given the idea is to execute a "prompt(1);"
    Then I should find a way to do it without using either open parenthesis
    And nor the equal sign, so the following payloads doesn't work:
    """
    <script>prompt(1)</script>
    <img src=x onerror=prompt(1)/>
    """
    Then what I do is avoid the equal sign and encode the open parenthesis
    """
    character = (
    character in hexadecimal = 28
    """
    And Javascript interpret hexadecimal by prepending "\x"
    Then I already know how to place a "(" (indirectly)
    And now for the equals sign I use the "setInterval" method with back quotes
    """
    back quotes = `
    """
    Then my payload would be as follows:
    """
    <script>setInterval`prompt\x281)`</script>
    """
    And I manage to overcome the level [evidences](won.png)

  Scenario: Remediation
    Given The idea is not to interpret user input as code
    Then I could implement a function that correctly encodes the HTML code tags
    """
    function htmlEntities(input) {
        return String(input).replace(/&/g, '&amp;').
               replace(/</g, '&lt;').replace(/>/g, '&gt;').
               replace(/"/g, '&quot;');
    }
    """
    And this allows taking the input as raw text

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      4.3/10 (Medium) - /AV:A/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      3.8/10 (Low) - E:P/RL:U/RC:U
    Environmental:Unique and relevant attributes to a specific user environment
      3.2/10 (Low) - IR:L/MAV:A/MAC:L

  Scenario: Correlations
    No correlations have been found to this date 2020-09-25
