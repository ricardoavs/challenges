## Version 1.4.1
## language: en

Feature:
  TOE:
    acuart
  Category:
    Information exposure
  Location:
    http://testphp.vulnweb.com/CVS/
  CWE:
    CWE-527: Exposure of CVS Repository to an Unauthorized Control Sphere
  Rule:
    REQ.006: https://fluidattacks.com/web/rules/006/
    REQ.241: https://fluidattacks.com/web/rules/241/
  Goal:
    Get unprotected information in the server protected area
  Recommendation:
    Force NGINX server not to display folders if there is no default index

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ubuntu          | 18.04.2     |
    | firefox         | 67.0(x64)   |
    | google search   | n/a         |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com
    And the server is running NGINX version 1.4.1
    And PHP version 5.3.10

  Scenario: Normal use case
    When I open http://testphp.vulnweb.com/
    Then I can see the page

  Scenario: Static detection
    When I do not have access to the source code
    Then I am not able to do static detection

  Scenario: Dynamic detection
    When I use google search to detect unprotected directories
    Then I get the result URL
    Then I get the list of all files in the CVS folder
    Then I can see file[evidence](cvs-folder.png)
    Then I can conclude that NGINX server is not configured properly
    And I can conclude that CVS information is being disclosed

  Scenario: Exploitation
    When I open the URL
    Then I can see files in the folder
    Then I can see the repo name acupart
    Then I can conclude that the NGINX is displaying CVS information

  Scenario: Remediation
    When the autoindex directive is set to off
    Then files are no longer displayed.
    """
    location /somedirectory/ {
    autoindex off;
    }
    """
    Then If I re-open the URL:
    """
    http://testphp.vulnweb.com/CVS/
    """
    Then I get blank page without listing the CVS files.
    Then I can conclude that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N/RL:O/RC:R/MAV:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.0/10 (Medium) - E:X/RL:O/RC:R
  Environmental: Unique and relevant attributes to a specific user environment
    4.0/10 (Medium) - MAV:N

  Scenario: Correlations
    No correlations have been found to this date 2019-08-20
