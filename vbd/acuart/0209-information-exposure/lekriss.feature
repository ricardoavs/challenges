## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Location:
    http://testphp.vulnweb.com - cookie (mycookie:3)
  CWE:
    CWE-0209: Infomation exposure trough an error message
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Generate an error in a SQL query to obtain the folders structure
    trough the error log
  Recommendation:
    Sanitize user input to avoid the generation of compromising error
    logs

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  TOE information:
    Given I am accessing the site testphp.vulnweb.com
    And Entered a php site which uses SQL requests

  Scenario: Normal use case:
    Given I access http://testphp.vulnweb.com/search.php
    And I write "somethin to search" in the search bar
    Then I can see this http://testphp.vulnweb.com/search.php?test=query
    And [evidence](evidence.png)

    Scenario: Static detection:
    Given I access http://testphp.vulnweb.com/search.php
    And I open the debugger
    Then I can see the source code of the page
    And particularly the search bar code
    """
    55 <form action="search.php?test=query" method="post">
    """
    And I can see no sanitization of the method used on the search
    And I conclude that I can inject incorrect SQL queries
    And generate an stack trace of the error

  Scenario: Dynamic detection:
    Given I access http://testphp.vulnweb.com/search.php?test=query
    Then I can write a "'''''" at the end of the url
    """
    http://testphp.vulnweb.com/search.php?test=query'''''
    """
    And I get the following error message
    """
    Warning: mysql_fetch_array() expects parameter 1 to be resource,
    boolean given in /hj/var/www/search.php on line 61
    """
    Then I can see a path which is not normally navigable
    Then I conclude that the path belongs to the server structure
    And I conclude that information is leaking trough an error message

  Scenario: Exploitation:
    Given I access http://testphp.vulnweb.com/search.php?test=query
    And I made the previous process
    Then I can see that the www is the third level in the directory
    And it is the place where the page source files are contained
    Then I can conclude that it is possible to use the path as an attack vector

  Scenario: Remediation:
    When the page is programmed it should sanitizate the input
    And that way, avoid any compromising output leak out
    Then the error log sould not even be generated

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.2/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    6.9 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.5 (Medium) - /CR:L/IR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:L/MI:L/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-05-06
