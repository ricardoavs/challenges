## Version 1.4.1
## language: en

Feature:
  TOE:
    acuart
  Category:
    Cross-Site Scripting
  Location:
    http://testphp.vulnweb.com/userinfo.php
  CWE:
    CWE-079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Detect and exploit vulnerability in userinfo - uname section through XSS
  Recommendation:
    Sanitize input previously

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Ubuntu          | 18.04.5 LTS |
    | Mozilla Firefox | 80.0.1      |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com/userinfo.php
    Then the page redirects to http://testphp.vulnweb.com/login.php

  Scenario: Normal use case
    Given I access http://testphp.vulnweb.com/login.php
    Then I can login to test account
    And I can add, edit or delete data from user "test"

  Scenario: Static detection
    Given I access the web site
    Then I notice that the source code is inaccesible

  Scenario: Dynamic detection
    Given I logged in with the credentials username/password "test/test"
    Then I enter to "Your Profile" option
    And I see the "Name" input field that updates the name of the user
    Then I enter the following script in it
    """
    <script>alert("Ive been pwnd")</script>
    """
    Then I get the output shown in the [evidence] (pwnd.png)
    And I conclude that the "Name" input field is vulnerable to XSS attack

  Scenario: Exploitation
    Given I logged in with the credentials username/password "test/test"
    Then I enter to "Your Profile" option
    And I see the "Name" input field that updates the name of the user
    Then I enter the following script in it
    """
    <script>alert(document.cookie)</script>
    """
    Then I get the cookies session as is shown on [evidence] (cookie.png)
    And I conclude that I can steal the cookies to get user credentials

  Scenario: Remediation
    Given the method called by
    """
    59  <form name="form1" method="post" action="">
    """
    Then the "name" input must be sanitized by using PHP filters as it follows
    """
    $name_sanitized = filter_var($_GET['name'], FILTER_SANITIZE_STRING,
                        FILTER_FLAG_STRIP_HIGH, FILTER_FLAG_STRIP_LOW);
    """
    And the patch applied sanitizes the "name" input

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.8/10 (Medium) - /AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.6/10 (Medium) - /E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.6/10 (Medium) - /MAV:N/MAC:L/MPR:L/MUI:R/MS:U/MC:H/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2020-09-22
