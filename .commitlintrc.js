const HEADERLENGTH = 50;
const LINELENGTH = 72;

module.exports = {
  parserPreset: './.commitlint-parser-preset',
  extends: [ '@commitlint/config-conventional' ],
  rules: {
    // header max chars 50
    'header-max-length': [ 2, 'always', HEADERLENGTH ],
    // always scope
    'scope-empty': [ 2, 'never' ],
    // lower-case subject
    'subject-case': [ 2, 'always', 'lower-case' ],
    // blank line between header and body
    'body-leading-blank': [ 2, 'always' ],
    // body lines max chars 72
    'body-max-line-length': [ 2, 'always', LINELENGTH ],
    // blank line between body and footer
    'footer-leading-blank': [ 2, 'always' ],
    // footer lines max chars 72
    'footer-max-line-length': [ 2, 'always', LINELENGTH ],
    'type-enum': [
      2,
      'always',
      [
        // New feature
        'feat',
        // Improves performance
        'perf',
        // Bug fix
        'fix',
        // Revert to a previous commit in history
        'rever',
        // Do not affect the meaning of the code (formatting, etc)
        'style',
        // Neither fixes a bug or adds a feature
        'refac',
        // Adding missing tests or correcting existing tests
        'test',
        // Programming or Hacking solution
        'sol',
      ],
    ],
    'scope-enum': [
      2,
      'always',
      [
        // Front-End change
        'front',
        // Back-End change
        'back',
        // Infrastructure change
        'infra',
        // Configuration files change
        'conf',
        // System component change (ci, scons, webpack...)
        'build',
        // asynchronous or schedule tasks (backups, maintainance...)
        'job',
        // Mix of two or more scopes
        'cross',
        // Documentation only changes
        'doc',
        // Code solution
        'code',
        // Hack solution
        'hack',
        // Vbd solution
        'vbd',
      ],
    ],
  },
};
