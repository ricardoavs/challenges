## Version 2.0
## language: en

Feature: Simple Programming - Programming - ctflearn
  Site:
    https://ctflearn.com/challenge/174
  User:
    dantivar
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> |   <Version>  |
    | Debian          |   stretch    |
    | Chrome          | 80.0.3987.122|
    | Python          |   3.5.3      |
  Machine information:
    Given A file data.dat

  Scenario: Success: Python Script
    Given The file I opened it
    Then I start to read every line
    And count the number of zeros
    And the number of ones
    Then I perform a simple module operation
    Then I sum the number of lines the number of zeros multiple of three
    And the lines with the number of ones multiple of 2
    And That is the flag
