## Version 2.0
## language: en

Feature: Pho Is Tasty! - Forensics - ctflearn
  Site:
    https://ctflearn.com/challenge/971
  User:
    dantivar
  Goal:
    Get flag

  Background:
  Hacker's software:
    | <Software name> |   <Version>  |
    | Debian          |   stretch    |
    | Chrome          | 80.0.3987.122|
    | xxd             |    1.10      |
  Machine information:
    Given A file "Pho.jpg"

  Scenario: Fail: Photo viewer
    Given The file I opened it a photo viewer
    And It looks normal
    Then there is no information to extract

  Scenario: Success: xxd Forensics
    Given The file I check the hexdump of the file with "xxd"
    And there is the flag
