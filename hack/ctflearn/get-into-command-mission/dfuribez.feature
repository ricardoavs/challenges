## Version 2.0
## language: en

Feature:
  Site:
    ctflearn
  Category:
    Miscellaneous
  User:
    mr_once
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Arch Linux      | 2020.08.01    |
    | Firefox         | 79.0          |
    | Binary Ninja    | 2.1.2263 demo |
  Machine information:
    Given I am downloading a ".exe" file from
    """
    https://mega.nz/#!KXYEQaIJ!ima4afmEP59Z1kKTm0H-3vO2x2UPdvNYKhUDdb3Vbr0
    """
    And the description of the challenge is
    """
    Back into the mission. Since we struck one fugitive successfully, we found
    an ID Card named ALDI and a flash disk which contains a program
    (https://mega.nz/#!KXYEQaIJ!ima4afmEP59Z1kKTm0H-3vO2x2UPdvNYKhUDdb3Vbr0).
    Unfortunately, it was locked. Note: You do NOT need a specific operating
    system to solve this question.
    """

  Scenario: Fail:Wrong flag
    Given I have a ".exe" file
    And since I use "archlinux" I can't execute it
    Then I use "Binary Ninja demo" to decompile it
    And I can see that it checks if the username is "ALDI" [evidence](comp.png)
    And it compares if "var_48" is equal to "data_4887c"
    And the value of "data_4887c" is "384" [evidence](variables.png)
    When I try "384" as the challenge flag
    Then I can see it does not work

  Scenario: Success:Right flag
    Given that the software credentials did not work to validate the challenge
    When I follow the software's flow
    Then I can see a base64 image gets shown after the user log-in
    When I copy that base64 [evidence](base64.png)
    And I use "html" to decode and show the image
    """
    <html>
      <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CA..." />
    </html>
    """
    Then I can see the flag [evidence](flag.png)
    And I solve the challenge
