## Version 2.0
## language: en

Feature: wow...-so-meta - forensics - ctflearn
  Code:
    348
  Site:
    ctflearn
  Category:
    forensics
  User:
    Calderonsin
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>    |
    | Ubuntu          | 16.01        |
    | Firefox         | 71.0         |
    | ExifTool        | 11.84        |
  Machine information:
    Given I am accessing the challenge site via browser

  Scenario: Failed:Trying to search something in the img
    When I check the challenge
    Then I download the challenges's img
    And  I open the img
    And  I can't find nothing interesting

  Scenario: Success:Look into the metadata of the img
    When I check the challenge's title
    Then I realize it was so meta
    Then I search for a program to read the metadata
    Then I find Exiftool
    """
    https://exiftool.org/index.html
    """
    Then I install it
    Then I use it on the img
    And  I find the flag in the camera serial number
    And  I Submit it and solve the challenge
