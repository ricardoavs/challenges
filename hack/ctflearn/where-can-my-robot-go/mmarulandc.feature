## Version 2.0
## language: en

Feature: Where Can My Robot Go? - Micellaneous - ctflearn
  Site:
    https://www.ctflearn.com/
  Category:
    Micellaneous
  User:
    mmarulandc
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 19.10       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given The page displays a question and a hint
    And The question is "Where do robots find what pages are on a website?"
    And The hint: "What does disallow tell a robot"
    And That is the only information in the challenge

  Scenario: Success: Analysing the page
    Given The question and the hint
    When I try thinking a possible solution with those hints
    Then I realize the question reffers to a route in the page
    When I access to the next page:
    """
    https://ctflearn.com/robots.txt
    """

    Then A page with a message is displayed [evidence](evidence.png)
    When I see a message
    Then I realize that message is route inside the site
    When I access to
    """
    https://ctflearn.com//70r3hnanldfspufdsoifnlds.html
    """
    Then The flag is displayed [evidence](evidence2.png)
    When I enter the flag in the format
    """
    CTFLearn{<FLAG>}
    """
    Then the challenge is solved
