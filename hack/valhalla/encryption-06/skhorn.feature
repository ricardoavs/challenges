# language: en

Feature: Solve Encryption challenge 6
  From site Valhalla
  From Encryption Category
  With my username Skhorn

  Background:
    Given the fact i have an account on Valhalla site
    And i have Debian 9 as Operating System
    And i have internet access
    And some background on old encryption techniques

  Scenario: Succesful Solution
    Given the link to the challenge
    And a statement about a very old encryption
    And a set of numbers arranged in pair ranging from 1 to 5
    And another statement that answer should be in caps with proper spacing
    And i search for the polybius square
    And i check the grid
    And i use the column row number identification to locate the coordinate
    And i translate the numbers into plain text
    And i see it is an english word
    Then i use this word as an answer
    And i solve the challenge
