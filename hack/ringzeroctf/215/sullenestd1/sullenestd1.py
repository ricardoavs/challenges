msg = ""
# let's open the file with the correct encoding
with open("LoveLetter.txt","r",encoding="iso-8859-1") as f:
    for character in f.read():
        if character == " ":
            msg = msg + "0"
        if ord(character) == 160: #non breaking space unicode
            msg = msg + "1"
print(msg)
