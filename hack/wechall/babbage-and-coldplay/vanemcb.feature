## Version 2.0
## language: en

Feature: Babbage and Coldplay - Steganography - WeChall
  Site:
    WeChall
  Category:
    Steganography
  User:
    vanem_cb
  Goal:
    Find a hidden message on the audio file

  Background:
  Hacker's software
    | <Software name>  |       <Version>      |
    | Windows          | 10.0.17134.407 (x64) |
    | Chrome           | 70.0.3538.77         |
    | Pro Tools HD     | 10.3.9               |
    | Sonic Visualiser | 3.1.1                |
  Machine information:
    Given the challenge URL
    """
    https://www.wechall.net/challenge/ludde/babbage_and_coldplay/index.php
    """
    Then I open the URL with Google Chrome
    And I see the challenge statement
    """
    We got a recording of the newcomer band, "Babbage and Coldplay".
    Can you find out if it contains any hidden message, so we can
    safely ship it to the customers?
    """
    And I download an audio file
    """
    babbage_and_coldplay.mp3
    """

  Scenario: Fail: Listening the audio
    Given the audio file
    Then I play the audio file
    And I listen a song
    But I don't listen anything weird
    And I don't write nothing in the input field to submit the answer
    And I don't solve the challenge

  Scenario: Fail: Using an analysis tool
    Given the audio file
    Then I open an analysis audio tool
    """
    Sonic Visualiser 3.1.1
    """
    And I import the audio file
    Then I choose the option to see the signal's spectrogram
    But I don't see anything in the spectrum [evidence](soundspect.png)
    Then I don't write nothing in the input field to submit the answer
    And I don't solve the challenge

  Scenario: Succes: Editing the audio
    Given the audio file
    Then I open an audio edit software
    """
    Pro tools 10 HD
    """
    And I import the audio file
    Then I separate the stereo channel into two monophonic channels
    And I use a plug-in to inverse the phase of the one monophonic channel
    And I play the two monophonic channels [evidense](soundedit.png)
    And I listen a woman saying something
    """
    The solution is sound inversion
    """
    Then I write "sound inversion" in the input field to submit the answer
    And I solve the challenge
