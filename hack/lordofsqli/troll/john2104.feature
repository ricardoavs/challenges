## Version 2.0
## language: en

Feature: troll-sql-injection-lordofsqli
  Site:
    https://los.rubiya.kr/gate.php
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://los.rubiya.kr/chall/troll_05b5eb65d94daf81c42dd44136cb0063.php
    """
    When I open the url with Chrome
    Then I see the PHP code that validates the user id
    And It shows the query made on the screen
    """
    select id from prob_troll where id=' '
    """
  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
    TROLL Clear!
    """
    When I try with the well known payload:
    """
    id='admin
    """
    Then I get "HeHe"
    But I don't solve it

   Scenario: Success:Sqli-uppercase-technique
    Given I inspect the code
    And I see that they validate the admin string
    And the char single quote
    """
    if(preg_match('/\'/i', $_GET[id])) exit("No Hack ~_~");
    if(preg_match("/admin/", $_GET[id])) exit("HeHe");
    """
    When I use an uppercase letter in the admin name
    """
    ?id=aDmin
    """
    Then I solve the challenge
