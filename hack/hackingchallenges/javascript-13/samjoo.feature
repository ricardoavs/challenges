## Version 2.0
## language: en

Feature: javascript-13 - javascript - hacking-challenges
  Site:
    hacking-challenges.de
  Category:
    javascript
  User:
    samjoo
  Goal:
    Find the flag from inspecting javascript code

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows10       | 2004        |
    | Chromium        | 85.0        |
  Machine information:
    Given I am accessing the challenge through a web browser
    And I see a text input with the following text
    """
    Viele Passwörter
    """
    And I know the challenge is related to javascript code
    Then I start the challenge

  Scenario: Fail:Find the flag analyzing the pwd() function
    Given I'm in the challenge page
    And I know the start point is the following function
    """
    pwd() {
    ....
    }
    """
    Then I open the chrome dev tools
    And I search for it in the source tab
    And I found it
    Then I inspect the function
    And I can see the functions compares the input with a predefined string
    """
    ...
      choice=document.hackit.eingabe.value;

      if (choice==upperstring)
      {
      alert ("Das Passwort ist richtig!");
      }
      else
      {
      alert("Falsches Passwort!");
      }
    } // End of pwd()
    """
    Then I search for the "upperstring" variable
    And I check how It's being built
    """
    pw="c-tecx";
    pw1="hacking";
    pw2="challenges";
    pw3="javascript";
    pw4="viele";
    pw5="passwoerter";
    pw6="sind";
    pw7="ziemlich";
    pw8="schwer";
    pw9="c-tecx";
    POW1=pw1.substring(1, 3);
    PEW2=pw2.substring(3, 4);
    PRW11=pw5.substring(0, 1);
    PMW3=pw3.substring(2, 4);
    PW4=pw4.substring(1, 3);
    PxW7=pw5.substring(4, 6);
    PW8=pw6.substring(2, 3);
    PW6=pw8.substring(0, 4);
    PW7=pw4.substring(1, 2);
    PW9=pw7.substring(3, 5);
    PW10=pw8.substring(4, 5);
    upperstring=(POW1+PEW2+PMW3+PW4+PW8+PW6+PxW7+PW8+POW1+PW10
    +PRW11+PxW7+PW8+PW10+PW9);
    """
    But I need to run the code to find the value of "upperstring"

  Scenario: Sucess:Use breakpoints to inspect the code at execution time
    Given I'm in the chrome dev tools
    Then I set a breakpoint in source tab at 234
    """
    179:
    180: if (choice==upperstring)
    181: {
    """
    Then I execute "pwd()" from the dev tools console
    And The functions triggers the breakpoint
    Then I call the variable upperstring from the console
    """
    > upperstring
    """
    And I can see the expected string
    """
    "aclvaienschwwonacepwoneml"
    """
    Then I put the string in the text box to check the flag
    And I got a valid response
    """
    Das Passwort ist Richtig!
    """
