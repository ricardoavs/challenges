## Version 2.0
## language: en

Feature: javascript-01 - javascript - hacking-challenges
  Site:
    hacking-challenges
  Category:
    javascript
  User:
    fgomezoso
  Goal:
    Enter the correct password

   Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 7            |
     | Chrome          | 78.0.3904.70 |
  Machine information:
    Given I am accessing the challenge site via browser
    """
    http://www.hacking-challenges.de/index.php?page=
    hackits&kategorie=javascript&id=1
    """

  Scenario: Success:Source Code Inspection
    Given the challenge page is displayed
    When I check the source code
    Then I find a script with a password function
    """
    <script language="JavaScript">
    function pwd() {
    passwort="javascript";
    if (passwort== document.hackit.eingabe.value) {
      alert ("Super das Passwort ist richtig! Jetzt
      bitte bei Lösung eintragen.");
      }
      else
      {
      alert("Das Passwort ist Falsch!");
      }
    }
    """
    Then a password variable is already set as 'javascript'
    And the user input must have the same value
    When I type 'javascript' as the password
    Then I solved the challenge
