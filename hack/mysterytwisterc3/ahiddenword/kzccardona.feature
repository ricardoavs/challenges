## Version 1.0
## language: en

Feature:  hidden codeword-mysterytwisterc3
  Site:
    https://www.mysterytwisterc3.org
  Category:
     hidden codeword
  User:
    kevinc
  Goal:
    Try to find a hidden codeword tah is inside a pdf file

  Background:
  Hacker's software:
    |<version>             | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | google chrome        | 75.0  (64 bit) |
  Machine information:
    Given I opened the following url
    """
    https://www.mysterytwisterc3.org/en/challenges/level-1/a-hidden-word
    """

  Scenario: fail: Web search
    Given I read the problem
    And I think maybe the hidden word is in metadata
    Then I open a new tab in chrome
    And I try to search a tool to analize the metadata
    And I can't find any relevant data
    Then I think to try other posibilities

  Scenario: Success: Using pdf tools to analyze the text
    Given I read the problem
    Then I take the file and read it
    And I saw something weird in the letters
    Then I start to analize the letters with more zoom
    And I find a little letters inside
    Then I put every little letter in a piece of paper
    Then I got the codeword "SITCHBAR" that is a german word
    And I put this sentence in page to verify the solution
    And I solve the challenge
