## Version 2.0
## language: en

Feature: Defuse Bomb - Bonus - CTFSme
  Site:
    CTFSme
  Category:
    Bonus
  User:
    vanem_cb
  Goal:
    Find the password to defuse the bomb

  Background:
  Hacker's software
    | <Software name> |       <Version>      |
    | Windows         | 10.0.17134.407 (x64) |
    | Chrome          | 70.0.3538.77         |
  Machine information:
    Given the challenge URL
    """
    https://ctfs.me/challenges/CTFs.me#defuse-bomb
    """
    Then I open the URL with Google Chrome
    And I see the challenge statement
    """
    The password to defuse bomb is the flag!
    """
    And I download an image file
    """
    bomb.jpg
    """

  Scenario: Fail: Edit image
    Given the image file
    Then I open the file in a online tool editor [evidence](imageedit.png)
    """
    https://www.befunky.com/es/opciones/editor-de-fotos/
    """
    And I zoom in the image to find something
    And I apply different filters
    And I modify some image's parameters
    But I can't find anything that could be the password
    Then I don't solve the challenge

  Scenario: Succes: Look for information about image
    Given the image file
    Then I do a reverse image search on google
    And I realize that image belongs to a videogame
    """
    Counter Strike
    """
    Then I look for how defuse a bomb on Counter strike
    But I don'n find something useful
    Then I make a new search
    """
    counter strike code bomb
    """
    And I find an interesting text
    """
    Counter Strike's bomb code is "7355608", meaning: Every code you edit
    will repeat itself on the third letter. You can't have more than 7 letter
    slots.
    """
    And I guess that "7355608" is the flag
    Then I write the flag in the input field to submit the answer
    And I solve the challenge
