# $ pylint fgomezoso.py
#
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
This program returns the flag with the format "PICOCTF{}" based on
a picture with numbers.
"""
from __future__ import print_function
import string


def main():

    """
    Associates the input list with an alphabetic list in uppercase.
    """

    letter_list = (list(string.ascii_uppercase))

    data = open("DATA.lst", "r")

    num_input = data.readline().rstrip('\n')
    edited_list = num_input.split()

    for item in edited_list:

        if item in ('{', '}'):
            edited_list.remove(item)

    num_list = list(map(int, edited_list))

    flag = []

    for item in num_list:

        flag.append(letter_list[item-1])

    flag.insert(len("PICOCTF"), "{")
    flag.append("}")

    print(''.join(flag))

    data.close()


main()

# $ python fgomezoso.py
# PICOCTF{<FLAG>}
