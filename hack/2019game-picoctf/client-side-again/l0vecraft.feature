## Version 2.0
## language: en

Feature: where-are-the-robots - web - picoCTF
  Code:
    where-are-the-robots
  Site:
    2019game.picoctf.com
  Category:
    web
  User:
    lovecraft
  Goal:
    capture a flag

  Background:
  Hacker's software:
    | <Software name> | <Version>       |
    | Ubuntu          | 18.04           |
    | FireFox         | 72.0.1(64 bits) |
  Machine information:
    Given I access to the challenge via browser
    When I enter to the page I see a welcom text
    And below other text with the question "where are the robots?"
    Then I can assume the challenge is about the "robots.txt" file

  Scenario: Success: Searching the robots.txt
    When I add at the end of the url "/robots.txt"
    Then I see a route called "/713d3.html" in the screen
    When I enter that route I see the message "Guess you found the robots"
    Then I can capture the flag
    And now I can solve the challenge
