#pylint: disable=superfluous-parens
#pylint: disable=invalid-name
#pylint: disable=bad-indentation
# flake8: noqa

"""
Takes a file and prints it shift-decyphered with keys 1-127
"""

encfile = open('input.txt', 'r')

ENCSTRING = encfile.read()
ENCARR = list(ENCSTRING)

desencarr = []
decrypted = ''

for i in range(0, 127):
  print '+'+str(i)+': \n'
  for letter in ENCARR:
    ascii = ord(letter)
    newascii = (ascii - i) % 127
    if newascii < 0:
      newascii = 127+newascii
    desencarr.append(chr(newascii))
  decrypted = ''.join(desencarr)
  print decrypted
  desencarr = []
  print('_____________________________________________________________')


encfile.close()
