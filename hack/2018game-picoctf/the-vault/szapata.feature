## Version 2.0
## language: en

Feature: the-vault  ctf  2018game-picoctf
  Site:
    2018game.picoctf.com
  Category:
    CTF
  User:
    chalimbu
  Goal:
    log in the site

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1     |
    | Google Chrome   | 76.0.3809   |

  Machine information:
    Given I am accessing the web-site through the browser
    When I access the challenge page

  Scenario: success : sql injection
    Given I am logged in the "2018.picoctf site"
    And I access the challenge for Web Exploitation
    When I access the url on the browser
    """
    http://2018shell.picoctf.com:8420
    """
    Then I see the source code of the "login.php"
    Given there is some interesting php code:
    """
    $query = "SELECT 1 FROM users WHERE
    name='$username' AND password='$password'";
    """
    Given I know the user is admin
    And this code does not make sanitation
    Then I make a injection in the field password
    """
    hola' OR 'SI'=='SI
    """
    And it always return the admin
    And I get the flag
