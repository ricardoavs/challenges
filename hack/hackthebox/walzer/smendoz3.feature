## Version 2.0
## language: en

Feature: Walzer - Crypto - Hack the Box
  Site:
    https://www.hackthebox.eu
  Category:
    Crypto
  User:
    arguoK
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 16.04       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given A text file with a ciphered text:
    """
    mvj03Z/F98Sf25nLw8L/5t3MyNvaxMn72prZ6N7YnMKezPz33tTByd3Uyejd1PzLns/
    B+9qa4MvDwv/72uvjycOb3cme2P/ow+GZ9A==
    """
    And A flag format of
    """
    HTB{$FLAG}
    """
    And Author clue "Easy cryptography made harder in Walzer style"

  Scenario: Fail: Missing information
    Given The clue and the ciphered text
    When I google the concept of "Walzer"
    Then I find is a dance that consist of repeated sequence of three steps
    """
    Basic Walzer is a repeated sequence of three steps. The teachers
    calls them "step", "side", "close".
    """
    And I asume are 3 types of ciphers or encodes repeated in same order
    When I search for easiest cryptography methods that fit with this names
    Then I found simple substitution as ROT13 cipher and XOR
    When I look the initial text
    Then I know is a base64 thanks to the padding at the end of the string
    When I bruteforce this three method in CyberChef
    Then I can't find something useful

  Scenario: Success: Author name as a XOR Key
    Given The ciphered text
    And The knowledge gained on the subjects from the Fail Scenario
    When I search for author profile
    Then I noticed he have a "0x..." prefix
    And maybe that can be a hex number clue
    When I assign base64 to "step", ROT13 TO "side" and XOR to "close"
    Then I bruteforce ROT13 and XOR parameters with author name "0xEA31"
    And I see in the output of XOR using "AE" hex key the pattern "HTB{...}"
    And I caught the flag [evidence](evidence.jpg)
