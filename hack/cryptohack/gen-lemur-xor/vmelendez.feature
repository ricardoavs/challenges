## Version 2.0
## language: en

Feature: general-lemur-xor
  Site:
    Cryptohack
  Category:
    General
  User:
    Mbaku
  Goal:
    Decrypt the image flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Python          | 3.8.5     |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its URL
    When I visit the link
    Then I see a description with the following sentence
    """
    I've hidden two cool images by XOR with the same secret key so you
    can't see them!
    """
    And it is possible to download the two images

  Scenario: Sucess:Decrypt the image
    Given that we have 2 images encrypted with a key
    Then I don't know any of the plaintexts
    And that is, I only have the ciphertexts
    Then I will assume "a" as the plaintext of the first image
    And "b" as the plaintext of the second image
    Then "k" as the xor key
    And "c" and "d" as the ciphertexts, the only thing I know
    Then the operation performed with the images would look like the following
    """
    first image:  a ^ k = c
    second image: b ^ k = d
    """
    And following through the math
    """
    c ^ d = (a ^ k) ^ (b ^ k)
    c ^ d = (a ^ k) ^ (k ^ b)
    c ^ d = a ^ (k ^ k) ^ b
    c ^ d = a ^ 0 ^ b
    c ^ d = a ^ b
    """
    Then the key disappears in this particular equality
    And this means that I can recover the plaintext XORing their ciphertexts
    Then since it is about images, we are talking about pixels
    And the idea is XOR their pixels
    Then I wrote a python script to solve it
    """
    from PIL import Image

    im = Image.open(r"flag.png")
    im2 = Image.open(r"lemur.png")
    pixels = im.load()
    width = im.width
    height = im.height

    for i in range(width):
        for j in range(height):
            cordinate = x, y, = i, j
            r, g, b = im.getpixel(cordinate)
            r1, g1, b1 = im2.getpixel(cordinate)
            rf, gf, bf = (r ^ r1), (g ^ g1), (b ^ b1)
            pixels[i, j] = rf, gf, bf

    im.save("gg.png")
    """
    And I got the flag [evidences](gg.png)
