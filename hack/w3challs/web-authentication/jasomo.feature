## Version 2.0
## language: en

Feature: web-authentication-w3challs
  Site:
    w3challs
  Category:
    Web
  User:
    jasomo
  Goal:
    Obtain the flag by gaining access to the admin web of a site

  Background:
  Machine information:
    | <Software name>   |  <Version> |
    | VMWare ESX        |     6.5    |
    | Windows           |      10    |
    | Debian VM         |   10.4.6   |
    | Kali VM           |   2020.2   |
  Hacker Software:
    | BurpSuite         |    1.9.0   |

  Machine information:
    Given the challenge URL
    """
    https://w3challs.com/challenges/web/authentication
    """
    And the link to the site to hack
    And the field to submit the flag

  Scenario: Success:Burpsuite
    Given I access to the site to hack
    Then a web with 3 internal links are showed
    And 2 of them are under construction
    And one that needs admin permissions
    Then I run Burpsuite proxy to interception requests
    And I configure the browser to use Burpsuite proxy
    Then I try to access again to those internal links
    And I realize the following cookie is being sent
    """
    Cookie: authz=b14361404c078ffd549c03db443c3fede2f3e534d73f78f77301ed97d4a4
    36a9fd9db05ee8b325c0ad36438b43fec8510c204fc1c1edb21d0941c00e9e2c1ce2
    """
    Given the size of the Cookie hash I realize it could be a SHA512
    Then I access this site to decrypt the hash
    """
    https://md5decrypt.net/en/Sha512/
    """
    And the obtained clear text is
    """
    user
    """
    Then I use the same site to hash the word
    """
    admin
    """
    And the obtained hash is
    """
    c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac
    71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec
    """
    Then I try to access the admin site while being intercepted by Burp proxy
    And replace the intercepted request Cookie by this new one
    """
    Cookie: authz=c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b9
    31dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec
    """
    Then I receive the following responde
    """
    Well done !!
    You successfully exploited your "cookies".
    Here is the flag to solve this challenge: W3C{iaobjej4g}
    """
    And the challenge is solved
