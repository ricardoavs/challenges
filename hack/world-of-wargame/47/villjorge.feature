## Version 2.0
## language: en

Feature: 47 - reconimg - world of wargame
  Code:
    47
  Site:
    https://wow.sinfocol.org/
  Category:
    reconimg
  User:
    idleryan
  Goal:
    Find the company logos names

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.5 LTS |
    | Mozilla Firefox | 80.0.1      |
  Machine information:
    Given I am accessing the challenge site through my browser
    And i use my credentials to start the challenge

  Scenario: Fail:Crop and search the logos
    Given three logos about OS distributions
    Then I take a cropped screenshot of each logo
    Then I upload the logos in Google Images
    And get wrong names and urls

  Scenario: Success:Deep search about the logos
    Given three logos about OS distributions
    Then I search on Google about their companies
    Then I found that the first image belongs to Windows
    Then I found that the third image belongs to Apple
    Then I found that the third image belongs to BackTrack
    And solved the challenge
    And I caught the flag
