Feature: Solve Mascotas challenge
  from site World of Wargame
  logged as EdPrado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS

Scenario: Challenge solved
  Given A mosaic image of diferent linux pets
  When I download the image
  And I separate each image from the mosaic
  And I google each image by adding "mascota" as aditional search criteria
  Then I find each pet name
  And I solve the challenge

