## Version 2.0
## language: en

Feature: 63 - steganos - world of wargame
  Code:
    63
  Site:
    https://wow.sinfocol.org/
  Category:
    steganos
  User:
    idleryan
  Goal:
    Find the message behind the ASCII art code

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Ubuntu          | 18.04.5 LTS   |
    | Chromium        | 85.0.4183.121 |
    | WinRAR          | 5.91          |
  Machine information:
    Given I am accessing the challenge site through my browser
    And i use my credentials to start the challenge

  Scenario: Fail:ASCII art visual interpretation
    Given an "stegano_ascii_art.htm" file on "stegano_ascii_art.rar" file
    When I access .rar in order to reach the .htm file
    Then I open the .htm file
    And I see an apparent man in flames in ascii art [evidence] (stegano.png)
    And I found a possible flag
    But the challenge remains unsolved

  Scenario: Success:Decode ASCII art
    Given an "stegano_ascii_art.htm" file on "stegano_ascii_art.rar" file
    When I access .rar in order to reach the .htm file
    Then I open the .htm file
    And I copy all the ASCII values shown in [evidence] (stegano.png)
    And I paste it in the decoder of the folowing webpage
    """
    https://pictureworthsthousandwords.appspot.com/
    """
    When I get the following image [evidence] (decoded.png)
    Then I see an spanish text in the image [evidence] (decoded.png)
    And I found a possible flag
    And the challenge is solved
