Feature: Solve Audio stegano
  From the Root Me site
  Category Steganography
  With my username vanem_cb

Background:
  Given I am running Windows 10 (64 bits)
  And I am using Google Chrome Versión 70.0.3538.77 (Build oficial) (64 bits)

Scenario: Normal use case
  Given the challenge URL
  """
  https://www.root-me.org/en/Challenges/Steganography/Audio-stegano
  """
  Then I opened the URL with Google Chrome
  And I see the challenge statement
  """
  Audio stegano
  Interesting mix.
  """
  And I see the input field to submit the answer
  And I see a button
  """
  Start the challenge
  """
  When I click the button
  Then a audio file is downloaded
  """
  "ch7.wav"
  """
  Then I play the audio file
  And I listen something that I don't understand
  And I don't write nothing in the input field to submit the answer
  And I don't solve the challenge

Scenario: Failed Attempt number 1
  Given the audio file by click the button
  Then I open a audio edit software
  """
  Pro tools 10 HD
  """
  And I import the audio file
  And I see the sampling rate of the audio file
  And I realize that the sampling rate is lower than the standard
  Then I convert the audio file with the correct sampling rate
  And I play the audio
  But I listen something that I don't understand
  And I don't write nothing in the input field to submit the answer
  And I don't solve the challenge

Scenario: Failed Attempt number 2
  Given the audio file by click the button
  Then I open a audio edit software
  """
  Pro tools 10 HD
  """
  And I import the audio file
  And I look at the waveform
  And I realize that the waveform seems modulated
  And I think that the audio file has been modulated in amplitude
  Then I open Google Chrome to look for a way to demodulate the signal
  But I realize that I need the frequency of the signal carrier
  And I don't know the frequency of the signal carrier
  Then I don't write nothing in the input field to submit the answer
  And I don't solve the challenge

Scenario: Successful solution
  Given the audio file by click the button
  Then I download a audio analysis software
  """
  Sonic Visualizer 3.1.1
  """
  And I open the software
  And I import the audio file
  Then I choose the option to see the signal's spectrogram
  And I realize that in a range frequencies the signal has more energy
  Then I modify the zoom
  And I see letters in the frequency spectrum
  """
  "PASSWORD-SECRET"
  """
  Then I write this words in the input field to submit the answer
  And I solve the challenge
