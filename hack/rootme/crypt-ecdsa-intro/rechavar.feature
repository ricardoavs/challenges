# Version 2.0
## language: en

Feature: ECDSA - Introduction - Cryptanalysis - Root Me

  Site:
    https://www.root-me.org
  Category:
    cryptanalysis
  User:
    rechavar
  Goal:
    Get the flag by faking a signature.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          |    20.04    |
    | Google-chrome   |84.0.4147.125|
    | Python          |    3.8.5    |
    | Jupyter         |    1.0.0    |
    | textdistance    |    4.2.0    |
    | ecdsa           |    0.16.0   |

  Machine information:
    Given The challenge URL
    """
    https://www.root-me.org/en/Challenges/Cryptanalysis/ECDSA-Introduction
    """
    And The challenge statement
    """
    You are provided with 30 messages to break our ECDSA cipher.
    Each message has been signed with our private key.
    Get the flag by faking a signature.
    """
    And The challenge connection information:
    """
    Host  challenge01.root-me.org
    Protocol  TCP
    Port  51044
    SSH access  ssh -p 2221 cryptanalyse-ch44@challenge01.root-me.org
    Username  cryptanalyse-ch44
    Password  cryptanalyse-ch44
    """
    And 3 related resource:
    """
    Elliptic_curve_digital_signature_algorithm (fr.wikipedia.org)
    Elliptic_Curve_Digital_Signature_Algorithm (en.wikipedia.org)
    NIST.FIPS.186-4 Digital Signature Standard (Cryptographie)
    """
  Scenario: Fail: base64 to int
    Given I started the challenge
    Then I open my terminal and connect to the server using SSH
    When I type in the terminal
    """
    ssh -p 2221 cryptanalyse-ch44@challenge01.root-me.org
    """
    And I can see[evidence](image1.png)
    Then I inspect which files are there
    When I can see [evidence](image2.png)
    """
    $ ls
    cha44.zip  public.pem  verify.py  xinetd
    """
    Then I try to download verify.py and xinetd
    And they aren't available for download
    When I try to download ch44.zip I can see[evidence](image3.png)
    """
    $ scp -P 2221 cryptanalysis-ch44@chanllenge01.root-me.org:
    /challenge/cryptanalyse/ch44/ch44.zip ~/Desktop
    """
    Then I extract it
    When I open it I find 30 message
    And 1 public key in .pem format [evidence](image4.png)
    Then I open some random files
    When I can see part of a message
    And an encrypt message
    """
    2. Let z be the Ln leftmost bits of e, where Ln is the bit length
    of the group order n. (Note that z can be greater than n but not longer.)

    MIGIAkIBBsPNcNaY27+T0LJcefVhR596kqbO5qHInE8srynIYfeD3Gwu/h+VL6RLB5mW0P1dsTm
    vTYC/I/9oFtUx5ZghYFECQgEyy6hR7VtdQPI/47cz6RsBoY1RbQV+sjzV/tz+jnmA6W+mg8ik2D
    NUQNutZEuUEqKq3nI4m17yq6pxpkYYdmaKFQ==
    """
    When I read m28 I find a hint
    """
    Will you be able to find a flaw in these messages ? (By the way, we use
    SHA-256 as the hash function.)
    """
    Then I read the statement
    And I conclude that is the message and it signature
    When I start to read the Wikipedia related resource
    And I learn about the math behind ECDSA
    Then I go to the security section to read
    """
    In December 2010, a group calling itself fail0verflow announced recovery of
    the ECDSA private key used by Sony to sign software for the PlayStation 3
    game console. However, this attack only worked because Sony did not
    properly implement the algorithm, because {\displaystyle k} was static
    instead of random.
    """
    Then I conclude that in those messages two of them uses the same k value
    And I can recover the private key
    When I search on google text similarity I find "Levenshtein distance"
    """
    the Levenshtein distance between two words is the minimum number of
    single-character edits (insertions, deletions or substitutions) required
    to change one word into the other.
    """
    Then I search on google for a python library that implements this metric
    When I find textdistance library which implements different metrics
    And I search in documentation
    When I find that I can get similarity and distance between 2 sequences
    Then I create a Jupyter notebook
    And I import the necessary libraries
    """
    import socket
    import base64
    import os
    import textdistance
    from hashlib import sha256
    """
    Then I create some constants
    """
    filePath = 'ch44/messages'
    TCP = '212.129.38.224'
    IP = 51044
    """
    And I use os library to get of files names and store them in a list
    """
    files = [x for x in os.listdir(filePath) if os.path
                                    .isfile(os.path.join(filePath, x))]
    """
    Then I store files information in tuples
    """
    info = []
    for file in files:
        text = open(os.path.join(filePath,file), 'r').read()
        text = text.split('\n')
        info.append((file,text[0],text[1]))
    """
    When I connect to the TCP port I can see [evidence](image5.png)
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_addres = (TCP, IP)
    sock.connect(server_addres)
    """
    Then I use textdistance.levenshtein class
    And I create a dictionary with similarity and distants
    """
    dist_dic = {}
    for i in range(len(info)-1):
        for j in range(i, len(info) -1):
            ditance = textdistance.levenshtein.distance(info[i][2], info[j][2])
            similarity = textdistance.levenshtein
                                         .similarity(info[i][2], info[j][2])
            dist_dic['{} - {}'.format(info[i][0], info[j][0])] = {
                                'distance': ditance,'similarity':similarity}
    """
    When I inspect it I can see[evidence](image6.png)
    | <pair>   |  <distance>  |  <similarity>  |
    |  m23-m8  |      84      |      100       |
    | m20-m20  |      0       |      184       |
    | m20-m18  |      176     |       14       |

    Then I conclude that maybe m23 and m8 are signed with the same k value
    And I try to get r and s from the signature
    When I decode the base64 and parser it to int I can see
    """
    encoded_sig_1 = info[15][2]
    encoded_sig_2 = info[28][2]
    msg_1 = info[15][1]
    msg_2 = info[28][1]
    bynari_sig_1 = base64.b64decode(encoded_sig_1)
    bynari_sig_2 = base64.b64decode(encoded_sig_2)
    int_sig_1 = int.from_bytes(bynari_sig_1,'big')

    in_sig_ = 16085342279755414361003011449848274756834936386817394216477107890
    301790032789639702114920265110246355549679605358874979450094144660313933331
    593939948615843234217321378652348494029992220088011379885500523104160541660
    110659650368420694071491131153546806725125843359331729674869439398049213091
    9199108636297955131011520330319155294856
    """
    Then I conclude that I need to read more documentation

  Scenario: Fail: n from NIST192p.order
    Given The pair m23-m8
    Then I search on google a way to get r and s from encoded signature
    And I find a post from https://crypto.stackexchange.com/
    """
    ASN.1 is a notation for structured data, and DER is a set of rules for
    transforming a data structure (described in ASN.1) into a sequence
    of bytes, and back.
    """
    Then I search on google more information about ASN.1 and DER
    And I find the RFC6979
    """
    Deterministic Usage of the Digital Signature Algorithm (DSA) and
    Elliptic Curve Digital Signature Algorithm (ECDSA)
    """
    When I read the document I find
    """
    How a signature is to be
    encoded is not covered by the DSA and ECDSA standards themselves;
    a common way is to use a DER-encoded ASN.1 structure (a SEQUENCE
    of two INTEGERs, for r and s, in that order).
    """
    Then I search on google for a python library for ECDSA
    And I find the python-ecdsa library
    When I search in source code I find in util.py
    """
    def sigdecode_der(sig_der, order):
    """
    And in it info says
    """
    Decoder for DER format of ECDSA signatures.

      DER format of signature is one that uses the :term:`ASN.1` :term:`DER`
      rules to encode it as a sequence of two integers::

      Ecdsa-Sig-Value ::= SEQUENCE {
      r INTEGER,
      s INTEGER
      }
    """
    And it returns r and s as a tuple of ints
    Then I install the library
    And I import the necessary function to decode the signature
    """
    from ecdsa.util import sigdecode_der
    """
    When I read the function params I see that I need the order of the curve
    Then I read the source code and in curves.py I find
    """
    curves = [
      NIST192p,
      NIST224p,
      NIST256p,
      NIST384p,
      NIST521p,
      SECP256k1,
      BRAINPOOLP160r1,
      BRAINPOOLP192r1,
      BRAINPOOLP224r1,
      BRAINPOOLP256r1,
      BRAINPOOLP320r1,
      BRAINPOOLP384r1,
      BRAINPOOLP512r1,
      ]
    """
    And I can get the order of the curve with curve.order
    Then I decide to use the NIST192p curve
    When I user 'sigdecode_der' function I get
    """
    n = NIST192p.order
    r1,s1 = sigdecode_der(bynari_sig_1,n)
    r2,s2 = sigdecode_der(bynari_sig_2,n)
    r1 = 1265510312328484895247331707261448991605824935384310991807416782963835
    564597784363254239659240507333231141438825842769582608823774801766609236081
    671262740399
    r2 = 1265510312328484895247331707261448991605824935384310991807416782963835
    564597784363254239659240507333231141438825842769582608823774801766609236081
    671262740399
    s1 = 6753041640979688387979779876915891115452896113133086598574769062772319
    255453071001131912128923442129221353371809829899254300570032852562071138546
    77366207112
    s2 = 1710071458455891626718043916964887353854285373467262784067671765933876
    302454512508765722501427631426182892482077520690027201664120082947182808008
    546096121898
    """
    Then I hash both message as it says in documentatio to get 'z1' and 'z2'
    And I parses it to int
    """
    z1 = int(sha256(msg_1.encode()).hexdigest(),16)
    z2 = int(sha256(msg_2.encode()).hexdigest(),16)
    """
    When I use modular arithmetic tI get 'k'
    """
    k = (((z1 - z2) % n) * inverse_mod((s1 - s2), n)) % n
    k = 4252019414200616734970993368318005267086154557848483354849
    """
    And I use K to get the secret key dA
    """
    dA = (((s1*k) % n) - z1)*inverse_mod(r1,n) %n
    dA = 2333472634898832191338931325157268056934322789299456824943
    """
    Then I use 'k' and 'dA' to fake a knowing signature
    """
    s_fake = ((((r2*dA)%n)+z2)*inverse_mod(k,n))%n
    s_fake = 1553524736788043604916328887193411033237186836055104161303
    """
    And I conclude that the faking signature process fail

  Scenario: Success: n from curve lists
    Given the python-ecdsa library
    Then I conclude that maybe I don't use the correct curve
    And I create an order curve list
    """
    n_list = [NIST192p.order, NIST224p.order ,NIST256p.order,
              NIST384p.order, NIST521p.order, SECP256k1.order]
    """
    When I create a for loop to iterate across 'n_list'
    And apply the same process to get s_fake
    Then I comparate it with s1
    And I see which n is correct
    """
    for n in n_list:
        r1,s1 = sigdecode_der(bynari_sig_1,n)
        r2,s2 = sigdecode_der(bynari_sig_2,n)
        k = (((z1 - z2) % n) * inverse_mod((s1 - s2), n)) % n
        dA = (((s1*k) % n) - z1)*inverse_mod(r1,n) %n
        s_fake = ((((r1*dA)%n)+z1)*inverse_mod(k,n))%n
        if s_fake == s1:
            print("The order is ", n)
    """
    When I run it I get
    """
    The order is 68647976601306097149819007990813932172694353001433054093944634
    591855431833976553942450577463332171975329639963713633211138647686124403803
    40372808892707005449
    """
    And it belongs to the NIST521p curve
    Then I hash the message
    And I sign it
    """
    target_msg = "I broke your crypto, give me points!"
    z_target = int(sha256(target_msg.encode()).hexdigest(),16)
    s_target = ((((r2*dA)%n)+z_target)*inverse_mod(k,n))%n
    """
    When I search on ecdsa source code i find
    """
    def sigencode_der(r, s, order):
    """
    And it says
    """
    Encode the signature into the ECDSA-Sig-Value structure using :term:`DER`.

      Encodes the signature to the following :term:`ASN.1` structure::

      Ecdsa-Sig-Value ::= SEQUENCE {
      r INTEGER,
      s INTEGER
      }
    """
    Then I import it and use it to get a DER-encoded message
    """
    from ecdsa.util import sigencode_der
    encoded_ms = sigencode_der(r1,s_target,n)
    b64encoded = base64.b64encode(encoded_ms)
    b64encoded = MIGHAkFeYtWN7tbePVn9X8zMe8iWmZ6TkGb72wWdppzD8rahU5fHsZDFHyt1Cl
    ycERWjdYUSQ5ICxbaXYLwmM07WwjyfrwJCAM86rtvEbjHwxiLPnjUgjsTClwLIP9I94SzK1iyLj
    IlN+ShHwptmivbL7qSBtf/MaIwEZ6dOI2Ccw1oZgEPsDa8G
    """
    When I try to send the signature using the socket python library
    And it has problems receiving the response message
    Then I decide to use Netcat to send the encoded message
    """
    $ netcat 212.129.38.224 51044
    """
    And I can see [evidence](image7.png)
    """
    Congratz, you can validate this challenge with the flag: ****
    """
    Then I summit the flag
    And I pass the challenge
