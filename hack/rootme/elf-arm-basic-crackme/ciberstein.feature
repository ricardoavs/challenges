## Version 2.0
## language: en

Feature: Cracking-Root Me
  Site:
    www.root-me.org
  Category:
    Cracking
  User:
    ciberstein
  Goal:
    Get the validation password.

  Background:
  Hacker's software:
    | <Software name> |    <Version>     |
    | Windows         | 10.0.17134 (x64) |
    | Chrome          | 70.0.3538.77     |
    | IDA             | 5.0              |
  Machine information:
    Given I am accessing the challenge site from my browser
    And the statement is displayed
    """
    ELF ARM - Basic Crackme

    Get the validation password.
    Start the challenge
    """
    Then I selected the challenge button to start
    And I download the .BIN file

  Scenario: Fail:Surface exploration
    Given I should open the file
    Then I use the PowerISO version 7.2 (32 bits)
    And I saw many files encrypted
    Then I conclude that I can't use PowerISO for my purpose
    And I could not capture the flag

  Scenario: Success:Disassembling the file
    Given I have IDA version 5.0
    Then I proceed to disassemble the file
    And get the following code
    """
    print 'Please input password'
    s = raw_input()
    print "Checking %s for password...\n" % s

    if len(s) != 6:
      print 'Loser...'
      # Error Code = len(s)
      exit()

    var_10 = 6
    var_10 = var_10 - strlen(s)

    if s[0] != s[5]:
      ++var_10

    if s[0]+1 != s[1]:
      ++var_10

    if s[3]+1 != s[0]:
      ++var_10

    if s[2]+4 != s[5]:
      ++var_10

    if s[4]+2 != s[2]:
      ++var_10

    var_10 += (s[3] ^ 0x72) & 0xff
    var_10 += s[6]

    if var_10 == 0:
      print 'Success, you rocks!'
      # Error Code = 0
      exit()
    else:
      print 'Loser...'
      # Exit Code = var_10
      exit()
    """
    Then I analyze the algorithm
    And I saw that 's' is a string
    Then I saw that 's[6]'= 0 because 'len(s)'' must be 6
    And I need that 'var_10' to be 0.
    Then 's[3]' = 0x72
    Then I get the following
    """
    - s[0] == s[5]
    - s[1] == s[0] + 1
    - s[0] == s[3] + 1 = 0x73 => s[1] = 0x74, s[5] = 0x73
    - s[5] = s[2] + 4 => s[2] = 0x6f
    - s[4] = s[2] - 2 => s[4] = 0x6d
    """
    And I conclude that 's' is 'storms'
    Then I put as answer storms and the answer is correct.
    Then I solved the challenge.
