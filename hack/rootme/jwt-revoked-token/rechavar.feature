# Version 2.0
## language: en

Feature: JWT - Revoked token - Web-server - Root me

  Site:
    https://www.root-me.org
  Category:
    web-server
  User:
    rechavar
  Goal:
    Get the password

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          |    20.04    |
    | Google-chrome   |84.0.4147.125|
    | Python          |    3.8.5    |
    | Jupyter         |    1.0.0    |
    | requests        |    2.22.0   |

  Machine information:
    Given The challenge URL:
    """
    https://www.root-me.org/en/Challenges/Web-Server/JWT-Revoked-token
    """
    And The challenge statement:
    """
    Two endpoints are available:
    POST : /web-serveur/ch63/login
    GET : /web-serveur/ch63/admin
    Get an access to the admin endpoint.
    """
    And 2 related resource:
    """
    Attacking JWT authentication - Sjoerd Langkemper (Exploitation - Web)
    Hacking JSON Web Token (JWT) - Rudra Pratap (Exploitation - Web)
    """
    And the source code write in Python.

  Scenario: Fail: alg None
    Given I start the challenge
    Then I see a new page with information:
    """
    POST : /web-serveur/ch63/login
    GET : /web-serveur/ch63/admin
    """
    And I open the related resources and I read both of them
    When I realize that I should use python to make requests to those URLs
    Then I create a Jupyter notebook
    And I import the library
    """
    import requests
    """
    Then I create the variables as follow:
    """
    URL_post = 'http://challenge01.root-me.org/web-serveur/ch63/login'
    URL_get = 'http://challenge01.root-me.org/web-serveur/ch63/admin'
    """
    Then I read the source code
    And I understand that I should make a post request to get a JWT
    When I make the post requests with the correct user and password
    """
    r = requests.post(URL_post, json = { 'username': 'admin',
                                          'password':'admin'})
    """
    Then I get a JWT[evidence] (image1.PNG)
    And I try to make a get request using it as an authorization header
    """
    headers = {"Authorization": json["access_token"]}

    r2 = requests.get(URL_get, headers = headers)
    """
    When I get the following msg
    """
    b'{"msg":"Bad Authorization header. Expected value \'Bearer <JWT>\'"}\n'
    """
    Then I change the header
    And I add "Bearer" to the JWT:
    """
    headers = {"Authorization": 'Bearer ' + json["access_token"]}
    """
    And I get
    """
    b'{"msg":"Token is revoked"}\n'
    """
    Then I read the source code
    When I see that all tokens are placed in a blacklist [evidence](Image2.PNG)
    And I understand that I should change JWT to get the flag
    Then I copy the JWT
    And I paste it on jwt.io to decode it [evidence](image3.PNG)
    When I realized that:
    """
    In some cases, if the jwt is not well-validated, I can change alg to none
    because one special “algorithm” that all JWT libraries should support is
    none, for no signature at all. If we specify none as an algorithm in the
    header and leave out the signature, some implementations may accept our
    JWT as correctly signed.
    """
    Then I change the alg to none a generate a new JWT [evidence](image4.PNG)
    When I send it in the get request
    And I receive the following message
    """
    b'{"msg":"The specified alg value is not allowed"}\n'
    """

  Scenario: Success: base64 padding
    Given The challenge forum
    Then someone post that the hint is in RFC4648
    And I read it
    """
    This document describes the commonly used base 64, base 32, and base
      16 encoding schemes.  It also discusses the use of line-feeds in
      encoded data, use of padding in encoded data, use of non-alphabet
      characters in encoded data, use of different encoding alphabets, and
      canonical encodings.
    """
    When I find on page 5 the following:
    """
    The padding step in base 64 and base 32 encoding can, if improperly
      implemented, lead to non-significant alterations of the encoded data.
    """
    Then I search on google for a bad padding usage in base64
    And I find that I can add it at the end and nothing change
    When I try to send it
    Then I get the following message:
    """
    b'{"Congratzzzz!!!_flag:":"Do_n0****"}\n'
    """
    And I submit the message
    Then I pass the challenge
