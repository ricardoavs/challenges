## Version 2.0
## language: en

Feature:
  Site:
    root-me.org
  Category:
    Network
  User:
    mr_once
  Goal:
    Get the password

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
    | Wireshark       | 3.2.6       |
  Machine information:
    Given I download a pcap file from
    """
    http://challenge01.root-me.org/reseau/ch16/ch16.pcap
    """
    And the challenge description is
    """
    Get the password that was used to authenticate the SNMP exchange in
    this network capture.
    """

  Scenario: Success:Getting to know the protocol
    Given I have a pcap file
    When I open it with "Wireshark"
    Then I can see six frames [evidence](frames.png)
    And the protocol is "SNMP"
    When looking for how that protocol works I find the page
    """
    https://vad3rblog.wordpress.com/2017/09/11/snmp/
    """
    And there it is explained how to crack the protocol
    Then now I can try to get the password

  Scenario: Success:Cracking the password
    Given that now I know how to crack the password
    And I find this script
    """
    https://github.com/SalimHaddou/snmp0wn-md5
    """
    When I change the variables in the script to match those in the pcap file
    """
    msgAuthoritativeEngineID="80001f8880e9bd0c1d12667a5100000000"
    msgAuthenticationParameters="70d7880b4f31cc4d19a07dcd"
    msgWhole="3081810201033011020420dd06a9020300ffe30401010201030431302f041180
    001f8880e9bd0c1d12667a5100000000020105020120040475736572040c00000000000000
    000000000004003036041180001f8880e9bd0c1d12667a51000000000400a21f02046b4c5a
    c40201000201003011300f060a2b06010201041e010501020101"
    """
    And I run the script
    Then after a few seconds I can see the password
    """
    Testing password: !)&!@!^^^%
    Testing password: !@!!!(()
    Testing password: 4dm1n
    Winner Winner, Chicken Dinner!
    The password is: 4dm1n
    """
    And I solve the challenge

  Scenario: Fail:None
    Given the first article I read explained how to crack the protocol
    And the password was in the wordlist
    Then there is no fail scenario
