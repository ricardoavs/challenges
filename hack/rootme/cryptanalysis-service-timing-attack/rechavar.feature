# Version 2.0
## language: en

Feature: Service - Timing attack - Cryptanalysis - Root Me

  Site:
    https://www.root-me.org
  Category:
    cryptanalysis
  User:
    rechavar
  Goal:
    Find the key used to authenticate on the service a using timing attack

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          |    20.04    |
    | Google-chrome   |84.0.4147.125|
    | Python          |    3.8.5    |
    | Jupyter         |    1.0.0    |
    | socket          |    3.8      |
    | time            |    3.8      |

  Machine information:
    Given The challenge URL
    """
    www.root-me.org/en/Challenges/Cryptanalysis/Service-Timing-attack?lang=en
    """
    And The challenge statement
    """
    Find back the 12 chars long key used to authenticate on the service.
    """
    And The challenge connection information:
    """
    Host challenge01.root-me.org
    Protocol TCP
    Port https://or10nlabs.tech/root-me-command-control/51015
    """
    And 1 related resource:
    """
    Timing_attack (en.wikipedia.org)
    """
  Scenario: Fail: Using only alphanumeric options
    Given I start the challenge
    Then I notice that a webpage try to open but is closed immediately
    When I inspect the start "Start the challenge" button
    And I see it try to open the following URL:
    """
    tcp://challenge01.root-me.org:51015
    """
    Then I open the related resources
    And I learn about timing attack
    """
    In cryptography, a timing attack is a side-channel attack in which the
    attacker attempts to compromise a cryptosystem by analyzing the time
    taken to execute cryptographic algorithms.
    """
    Then I realize that:
    """"
    The server will compare the sending password with the real password
    char by char and I need to comparate the timing response with different
    options and wich it takes most is the correct value for that char
    """
    Then I search on google how to send data by TCP with python
    And I find that I need to use sucket library
    Then I see that in most cases sucket library connect with an IP address
    And I search on Google for rootme IP address
    When I find
    """
    212.129.38.224
    """
    Then I open a Jupyter notebook
    And I start to create a script to apply the timing attack
    Then I import the necessary libraries
    """
    import socket
    import time
    """
    Then I create the constants an variables that will help me
    And I create a socket object that helps with the connection
    """
    TCP = '212.129.38.224'
    IP = 51015
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    answer = ''
    """
    Then I create a Tuple with IP and port
    And I use it to connect the socket object
    """
    server_addres = (TCP, IP)
    sock.connect(server_addres)
    """
    Then I try to receive the first message without sending something
    And I decide to use a buffer size of 1024
    """
    data = sock.recv(1024)
    """
    And the output is
    """
    b'Enter the 12 chars long key : \n'
    """
    Then I send a simple message to see what I will receive:
    """
    sock.sendall(str.encode('aaaaaaaaaaaa'))
    data = sock.recv(1024)
    """
    And the output is:
    """
    b'Wrong key, try again\n'
    """
    Then I create constant 'options' that has all alphanumeric chars
    """
    options = 123456789abcdefghijklmnopqrstuvwxyz
    """
    And I create a script to perform the timing attack:
    """
    for j in range(1,13):
        dic = {}
        for i in options:
            password_ta = answer + i + 'a'*(12-j)
            time1 = time.perf_counter()
            sock.sendall(str.encode(password_ta))
            data = sock.recv(1024)
            time2 = time.perf_counter()
            dic[i] = time2-time1

        answer = answer + max(dic, key = dic.get)
        print('answer: {}, time: {}'.format(answer,
                              dic[max(dic, key = dic.get)]))
    """
    And the output is:
    """
    answer = 30467vk638eb
    """
    Then I summit my answer
    And I recieve a message
    """
    Just try again!
    """

  Scenario: Sucess: Using Script.printable
    Given I realize that I shouldn't limit to alphanumeric chars
    Then I import the string library
    """
    import socket
    import time
    import string
    """
    And I use string.printable to use all possible chars
    Then I run the same script
    And a got a new answer[evidence](image1.PNG)
    Then I test the new answer sending it to the server
    When I got a message
    """
    b'Good key, use it to validate the challenge\n'
    """
    Then a summit the new flag
    And I pass the challenge!
