import sys

user = sys.argv[1]

serial = 0
j = 2
for i in range(len(user)):
    serial += ord(user[i]) ** j
    j += 1

print ("user: ", user)
print ("serial: ", serial)
