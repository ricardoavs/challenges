#!/usr/bin/env python
'''
$ pylint badwolf10.py #linting
No config file found, using default configuration
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ python badwolf10.py #compilation
'''
import string

KEY = 'I know, you love decrypting Byte Code !'
SOLUCE = [57, 73, 79, 16, 18, 26, 74, 50, 13, 38, 13, 79, 86, 86, 87]
DIC = string.printable
PASSWORD = ""
II = 5

for ENC_CHAR in SOLUCE:
    for X in DIC:
        if ((ord(X) + II ^ ord(KEY[II])) % 255) == ENC_CHAR:
            PASSWORD += X
            break

    II = (II + 1) % len(KEY)
print PASSWORD

# pylint: disable=pointless-string-statement
'''
$ python badwolf10.py
I_hate_RUBY_!!!
'''
