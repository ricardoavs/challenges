## Version 2.0
## language: en

Feature: 32-H4CK1NG-enigmes-a-thematiques
  Code:
    32
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    andres568
  Goal:
    Find the password

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
    | Ghidra          | 9.1.2         |
  Machine information:
    Given I am accessing the executable file
    And The file has a bunch of weird characters

  Scenario: Fail:decompile-file
    Given I am on the file
    And I can see the content of the file has the header "ELF"
    When I use a reverse engineering tool called Ghidra
    Then I can see the information related to the file
    When I look out for the password
    Then The tool does not show readable text inside the functions
    And I can not capture the flag

  Scenario: Success:search-key-text
    Given I am on the file
    When I use the code editor to remove all the weird characters
    Then I can see a readable text
    When I use a tool to search for "passe"
    Then I can see a possible "<FLAG>"
    When I enter the message into the challenge form
    And I press the "Valider" button
    Then I can see the message "Bravo"
    And I capture the flag
