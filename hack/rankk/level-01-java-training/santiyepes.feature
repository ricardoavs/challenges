# language: en

Feature: Java training
  From site rankk.org
  From Level 1 Category
  With my user santiyepes

Background:
  Given I am running Linux 4.13.0-41-generic x86_64
  And Google Chrome 66.0.3359.181 (Build oficial) (64 bits)
  Given Intellij IDEA Community Edition
  And a Python site with a login form
  """
  URL: https://www.rankk.org/challenges/java-training.py
  Message: May the source be with you
  Objetive: Find the answer of the challenge
  Evidence: Any
  """

Scenario: Java class
The page shows a message that insists the solution in the source code
  Then I look at the source code
  And I see an Applet with a URL to download a Java class
  """
  <applet codebase="java/" code="Training.class" width="230" height="80">
  </applet>
  """
  Then I try to enter the URL with the Applet information
  """
  https://www.rankk.org/challenges/java/Training.class
  """
  And I'm successful in getting the Java class

Scenario: Analyze Java class
Try to find the solution in a compiled Java class
  Then I use the Intellij IDEA program to decompile
  And I open the class Training.class to see its code
  Then I see a method that contains a URL divided into variables
  """
  public void actionPerformed(ActionEvent var1) {
  String var2 = this.tf.getText();
  String var3 = "mythology";
  if (var2.equals(var3)) {
    this.fb.setText("Correct!");
    String var4 = this.getDocumentBase().toString();

    try {
      var4 = var4 + "?solution=" + var3;
      this.getAppletContext().showDocument(new URL(var4));
    } catch (Exception var6) {
    ;
    }
  } else {
    this.fb.setText("Wrong!");
    this.tf.setText("");
  }

  }
  """
  Then I'm going to organize the URL that contains the possible solution
  """
  https://www.rankk.org/challenges/java-training.py/?solution=mythology
  """
  Then I write it in the navigation bar of the browser
  And I solve the challenge
