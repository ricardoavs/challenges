## Version 2.0
## language: en

Feature: level01-rankK
  Site:
    https://www.rankk.org
  Category:
    level01
  User:
    Seruex
  Goal:
    I Acess to page gold and platinum page for view anime images

  Background:
  Hacker's software:hackergateway
    | <Software name> |  <Version>     |
    | Parrot Os       |  3.9 (64 bits) |
    | Firefox         | 56.4 (64 bits) |
  Machine information:
    Given I the challenge url
    #https://www.rankk.org/challenges/anime-underground.py?
    #user=melissa&acctype=silver&timestamp=1548444232.7
    And I see that the page has images of anime to download and the accounts has
    #limit of downloads

  Scenario: Fail: change the "silver (word)"
    Given I access to #https://www.rankk.org/challenges/anime-underground.py?
    #user=melissa&acctype=silver&timestamp=1548444232.7
    And I see one image of (bleach) anime
    And I left click on "View page source"
    And I see the code
    But I don't see anything reference to complet the challenge
    Then I see the url and change "silver (word)" to "bronze(word), and
    #bronce (word)"
    But I did not find the solution

  Scenario: Success : finding the (words) "URL"
    Given I access to #https://www.rankk.org/challenges/anime-underground.py?
    #user=melissa&acctype=silver&timestamp=1548444232.7
    Then I change "silver (word)" in the URL to "gold (word)"
    And I see new anime images (Naruto)
    Then I change "silver (word)" in the URL to "platinum (word)"
    And I see new anime images (Ultimate Girl DVD)
    And I click on the image
    Then I solved the challenge
