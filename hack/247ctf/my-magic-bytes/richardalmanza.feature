## Version 2.0
## language: en

Feature:  Cryptography-247ctf
  Site:
    247ctf
  Category:
    Cryptography
  User:
    richardalmanza
  Goal:
    Get the flag from encrypted file

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Ubuntu (Bionic) | 18.04.4 LTS   |
    | Google Chrome   | 80.0.3987.132 |
  Machine information:
    Given introduction to the challenge
    """
    Can you recover the secret XOR key we used to encrypt the flag?
    """
    And I downloaded the file

  Scenario: Fail: make a program to decrypt it
    Given challenge file
    And the hint from its name "my-magic-bytes.jpg.enc"
    Then I tried to open it first
    But It didn't work, of course
    When I tried to get more information in the internet about jpg
    And I found about its format, its first and last bytes
    And I checked the file using the following commands
    """
    $ hexdump -C my-magic-bytes.jpg.enc | head -1
    $ hexdump -C my-magic-bytes.jpg.enc | tail -2
    """
    And the terminal returned me this
    """
    00000000  b9 14 06 45 71 e0 b5 f7  37 07 cb 85 47 cc f9 a4
    |...Eq...7...G...|
    00013f00  79 f0 ff db 8a 34 e9 93  e5 14 66 5a a8           y....4....fZ.|
    00013f0d
    """
    Then I saw the file didn't match with the information i got about jpg
    And I decided to write a program in Crystal to find the key by force
    And I read in Crystal doc about how to work with file using byte mode
    But when I realized it was taking me too much time I dropped the idea

  Scenario: Fail: use an online tool to decrypt the file
    Given the ideas and information I got from last method
    Then I looked for a online tool
    And I found this [one](https://wiremask.eu/tools/xor-cracker/)
    When I downloaded all possible [options](options.png)
    And I checked the first bytes of all [files](files.png)
    But no even one file had the SOI(ffd8) bytes nor EOF(ffd9)
    Then I decided this option didn't work either

  Scenario: Success: discover the key 'manually'
    Given all the experience from past tries
    Then I applied XOR operator between jpg SOI, EOF
    And first and last bytes of challenge file
    And I got 46CC for SOI
    And A571 for EOF, the key should be 46CC*A571*, * is a wildcard
    But without more hints, I compared it with the first part of another jpg
    And It got [this](key.png)
    When I wrote the program below
    """
    #! /usr/bin/crystal

    img = File.new(ARGV[0], "r")

    slice = Bytes.new(img.size)
    key = ARGV[1].hexbytes

    hex_img = IO::Memory.new(img.gets_to_end)

    hex_img.read(slice)

    (0...img.size).each  do |x|
      slice[x] = slice[x] ^ key[x % key.size]
    end

    File.open("oput.jpg","wb") do |f|
      f.write slice
    end
    """
    And I used it
    """
    ./xor-decrypter.cr my-magic-bytes.jpg.enc 46ccf9a571f0ffb17e41cb84
    """
    Then I checked the new file
    And Bingo! I got the flag
    And [I passed the challenge](flag.png)
