## Version 2.0
## language: en

Feature: WEB-247ctf
  Site:
    247ctf
  Category:
    WEB
  User:
    richardalmanza
  Goal:
    Get the flag from client-side code

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Ubuntu (Bionic) | 18.04.4 LTS   |
    | Google Chrome   | 80.0.3987.132 |
  Machine information:
    Given introduction to the challenge
    """
    Developers don't always have time to setup a backend service
    when prototyping code. Storing credentials on the client side
    should be fine as long as it's obfuscated right?
    """
    And I accessed to challenge [page](page.png)
    And I saw the source [code](source.png)

  Scenario: Success:deobfuscate-js-code
    Given the obfuscated js code from source code
    When I saw the [, ], (, ), !, and + are the only characters
    And I identified it as JSFUCK obfuscation
    Then I looked for a deobfucator for JSFUCK
    When I used the [jsfuck deobfucator of enkhee-osiris](jsfk-deob.png)
    And I got
    """
    if (this.username.value == 'the_flag_is'
    && this.password.value == __FLAG__)
    { alert('Valid username and password!'); }
    else { alert('Invalid username and password!'); }
    """
    Then I tried with that flag
    And I passed the challenge
